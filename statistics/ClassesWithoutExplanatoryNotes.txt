<?xml version="1.0" encoding="UTF-8"?>AccessLocation
Address
AgentAssociation
AgentId
AgentIndicator
AgentRelation
Annotation
AnnotationDate
AreaCoverage
AudioSegment
BasedOnObjectInformation
BibliographicName
BusinessProcessCondition
BusinessProcessIndicator
CategoryIndicator
CategoryRelation
CategoryStatistic
CharacterOffset
ClassificationIndexEntryIndicator
ClassificationItemIndicator
ClassificationItemRelation
ClassificationSeriesIndicator
ClassificationSeriesRelation
CodeIndicator
CodeRelation
Command
CommandCode
CommandFile
ConceptIndicator
ConceptRelation
ConditionalText
ContactInformation
ContentDateOffset
CorrespondenceType
CustomItemIndicator
CustomItemRelation
CustomValueIndicator
DataPointIndicator
DataPointRelation
DataStoreIndicator
DataStoreRelation
DateRange
DoubleNumberRangeValue
DynamicText
DynamicTextContent
ElectronicMessageSystem
ElseIfAction
Email
ExternalControlledVocabularyEntry
FixedText
Form
GeographicUnitIndicator
GeographicUnitTypeIndicator
Image
ImageArea
IndexEntryRelation
IndividualName
InternationalString
InternationalStructuredString
LabelForDisplay
LanguageSpecificStringType
LanguageSpecificStructuredStringType
LineParameter
LiteralText
LocalIdFormat
LocationName
LogicalRecordIndicator
LogicalRecordRelation
NonIsoDateType
NumberRange
NumberRangeValue
OrganizationName
PairedExternalControlledVocabularyEntry
PhysicalRecordSegmentIndicator
PhysicalRecordSegmentRelation
Polygon
PrivateImage
RangeValue
RationaleDefinition
Segment
SpatialCoordinate
SpatialLine
SpatialPoint
SpatialRelationship
Statistic
StatisticalClassificationIndicator
StatisticalClassificationRelation
StudyIndicator
StudyRelation
SummaryStatistic
TargetSample
Telephone
TextualSegment
TypedDescriptiveText
ValueMappingIndicator
ValueMappingRelation
ValueString
VariableIndicator
VariableRelation
VideoSegment
VocabularyEntryIndicator
VocabularyEntryRelation
WebLink
WorkflowStepOrder
CategorySet
SentinelConceptualDomain
Algorithm
Design
Methodology
Process
ProcessControlStep
ProcessStep
Service
GeographicUnit
GeographicUnitClassification
GeographicUnitRelationStructure
GeographicUnitTypeClassification
GeographicUnitTypeRelationStructure
DataPointRelationStructure
PhysicalLayoutRelationStructure
PhysicalRecordSegment
SegmentByText
ControlledVocabulary
CustomInstance
CustomItemRelationStructure
CustomStructure
CustomValue
VocabularyEntry
VocabularyRelationStructure
SampleFrame
SamplePopulationResult
SamplingAlgorithm
SamplingDesign
SamplingGoal
SamplingProcess
Budget
ComplianceStatement
Embargo
ExPostEvaluation
QualityStatement
Standard
StudyControl
StudyRelationStructure
BusinessAlgorithm
DataPipeline
FundingInformation
ComputationAction
ConditionalControlStep
RepeatUntil
RepeatWhile
WorkflowControlStep
WorkflowProcess
WorkflowService
AppliedUse
BusinessFunction
Goal
Guide
Result
Access
TopicalCoverage
CodeListResponseDomain
ExternalAid
ImplementedInstrument
InstanceMeasurement
InstanceQuestion
Instruction
InstrumentCode
NumericResponseDomain
RankingResponseDomain
ScaleResponseDomain
TextResponseDomain
Comparison
ComparisonMap
MemberIndicator
SimpleCollection
AnnotatedIdentifiable
Identifiable
DataStore
DataStoreRelationStructure
LogicalRecordRelationStructure
ClassificationIndexEntry
ClassificationSeries
ClassificationSeriesRelationStructure
CodeList
EnumerationDomain
IndexEntryRelationStructure
