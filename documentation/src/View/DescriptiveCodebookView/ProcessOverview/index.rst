.. _ProcessOverview:


ProcessOverview
***************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Process is an implementation of an algorithm. It is the series of steps taken as a whole. It is decomposable into ProcessSteps, but this decomposition is not necessary.


Synonyms
========



Explanatory notes
=================
In a descriptive document, Process Overview provides a means of presenting the reader with a clear view of an overall process without going into the details of the process steps. Process Overview should be used when a description of the process in general is all that is desired.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst