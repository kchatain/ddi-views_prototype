.. _PhysicalLayoutRelationStructure:


PhysicalLayoutRelationStructure
*******************************


Extends
=======
:ref:`Identifiable`


Definition
==========
A realization of RelationStructure that is used to describe the sequence of Value Mappings in a Physical Layout. This can be more complex than a simple sequence.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst