.. _Standard:


Standard
********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Identifies the external standard used and describes the level of compliance with the standard in terms specific aspects of the standard's content.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst