.. _examples:


SentinelValueDomain - Examples
******************************


Missing categories expressed as Codes <-9, refused>; <-8, Don't Know> for a numeric variable with values greater than zero.