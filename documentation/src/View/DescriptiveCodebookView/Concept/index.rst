.. _Concept:


Concept
*******


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Unit of thought differentiated by characteristics [GSIM 1.1]


Synonyms
========



Explanatory notes
=================
Many DDI4 classes are subtypes of the Concept class including Category, Universe, UnitType, ConceptualVariable. This class realizes the pattern class Signified and as such a Concept can be denoted by a Sign.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst