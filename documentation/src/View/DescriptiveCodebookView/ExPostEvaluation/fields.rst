.. _fields:


ExPostEvaluation - Properties and Relationships
***********************************************





Properties
==========

=================  =================================  ===========
Name               Type                               Cardinality
=================  =================================  ===========
completionDate     Date                               0..1
evaluationProcess  InternationalStructuredString      0..n
outcomes           InternationalStructuredString      0..n
typeOfEvaluation   ExternalControlledVocabularyEntry  0..n
=================  =================================  ===========


completionDate
##############
Identifies the date the evaluation was completed.


evaluationProcess
#################
Describes the evaluation process. Supports multi-lingual content. Allows the optional use of structured content.


outcomes
########
Describes the outcomes of the evaluation process. Supports multi-lingual content. Allows the optional use of structured content.


typeOfEvaluation
################
Brief identification of the type of evaluation. Supports the use of an external controlled vocabulary.




Relationships
=============

=========  =====  ===========  ================
Name       Type   Cardinality  allways external
=========  =====  ===========  ================
evaluator  Agent  0..n           no
=========  =====  ===========  ================


evaluator
#########
Evaluator used in this process.



