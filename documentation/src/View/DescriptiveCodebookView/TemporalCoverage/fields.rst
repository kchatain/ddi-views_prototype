.. _fields:


TemporalCoverage - Properties and Relationships
***********************************************





Properties
==========

============  =============  ===========
Name          Type           Cardinality
============  =============  ===========
coverageDate  ReferenceDate  0..n
============  =============  ===========


coverageDate
############
A date referencing a specific aspect of temporal coverage. The date may be typed to reflect coverage date, collection date, referent date, etc. Subject and Keywords may be associated with the date to specify a specific set of topical information (i.e. Residence associated with a date 5 years prior to the collection date).

