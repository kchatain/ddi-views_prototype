.. _TemporalCoverage:


TemporalCoverage
****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Describes the temporal coverage of the annotated object by specifying dates and optionally associating them with Subject and Keyword. The date itself may be expressed as either and ISO and/or NonISO date. Dates may be catagorized by use of optional typeOfDate using and external controlled vocabulary entry.


Synonyms
========



Explanatory notes
=================
The association of subjects or keywords with the date periods allows for describing the meaning of the temporal period. A survey, for example might be administered in one time period, but ask questions about the preceding decade. Temporal coverage might then include the data collection coverage as well as the referenced time period.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst