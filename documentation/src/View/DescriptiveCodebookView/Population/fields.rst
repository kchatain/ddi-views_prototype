.. _fields:


Population - Properties and Relationships
*****************************************





Properties
==========

======================  =========  ===========
Name                    Type       Cardinality
======================  =========  ===========
timePeriodOfPopulation  DateRange  0..n
======================  =========  ===========


timePeriodOfPopulation
######################
The time period associated with the Population




Relationships
=============

=====================  ==============  ===========  ================
Name                   Type            Cardinality  allways external
=====================  ==============  ===========  ================
composedOf             Unit            0..n           no
geographyOfPopulation  GeographicUnit  0..n           no
narrowsUniverse        Universe        0..n           no
=====================  ==============  ===========  ================


composedOf
##########
Units in the Population 




geographyOfPopulation
#####################
The geographic location related to this population. May be repeated for inclusion of multiple locations.




narrowsUniverse
###############
Reference to a Universe that the Population narrows



