.. _examples:


LogicalRecordRelationStructure - Examples
*****************************************


A DataStore with a Household, Family, and Person LogicalRecord type. Allows for describing parent/child, whole/part, or other relationships as appropriate