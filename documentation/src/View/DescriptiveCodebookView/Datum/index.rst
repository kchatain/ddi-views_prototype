.. _Datum:


Datum
*****


Extends
=======
:ref:`Designation`


Definition
==========
A Datum is a designation (a representation of a concept by a sign) with a notion of equality defined. The sign itself is a perceivable object.


Synonyms
========



Explanatory notes
=================
From GSIM 1.1 "A Datum is the actual instance of data that was collected or derived. It is the value which populates one or more Data Points. A Datum is the value found in a cell of a table." (https://statswiki.unece.org/display/GSIMclick/Datum ) A Datum could be copied to one or more datasets. DDI4 takes a little more formal (semiotic) description of a Datum using the Signification Pattern. See the attached example. NOTE: This is NOT datum from DDI3.2 (which is quite specific).


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst