.. _fields:


DataPoint - Properties and Relationships
****************************************





Relationships
=============

=============  ================  ===========  ================
Name           Type              Cardinality  allways external
=============  ================  ===========  ================
hasDatum       Datum             0..n           no
isDescribedBy  InstanceVariable  0..n           no
realizes       CollectionMember  0..n           yes
=============  ================  ===========  ================


hasDatum
########
The Datum populating the DataPoint.




isDescribedBy
#############
The InstanceVariable delimits the values which can populate a DataPoint.




realizes
########
Classes which realize the class Member fufill the role of Member e.g. they may have relations defined on them, for example one DataPoint follows another in a  Record.



