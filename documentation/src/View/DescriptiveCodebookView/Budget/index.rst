.. _Budget:


Budget
******


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A description of the budget for any of the main publication types that can contain a reference to an external budget document.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst