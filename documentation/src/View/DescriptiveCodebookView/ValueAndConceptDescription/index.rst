.. _ValueAndConceptDescription:


ValueAndConceptDescription
**************************


Extends
=======
:ref:`Identifiable`


Definition
==========
A formal description of a set of values.


Synonyms
========



Explanatory notes
=================
The ValueAndConceptDescription may be used to describe either a value domain or a conceptual domain. For a value domain, the ValueAndConceptDescription contains the details for a “described” domain (as opposed to an enumerated domain). There are a number of properties which can be used for the description. The description could be just text such as: “an even number greater than zero”. Or a more formal logical expression like “x>0 and mod(x,2)=0”. A regular expression might be useful for describing a textual domain. It could also employ a format pattern from the Unicode Locale Data Markup Language (LDML) (http://www.unicode.org/reports/tr35/tr35.html. Some Conceptual Domains might be described with just a narrative. Others, though, might be described in much the same way as a value domain depending on the specificity of the concept. In ISO 11404 a value domain may be described either through enumeration or description, or both. This class provides the facility for that description. It may be just a text description, but a description through a logical expression is machine actionable and might, for example, be rendered as an integrity constraint. If both text and a logical expression are provided the logical expression is to be taken as the canonical description. The logical expression should conform to the expression syntax of VTL. https://sdmx.org/?page_id=5096


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst