.. _fields:


ValueAndConceptDescription - Properties and Relationships
*********************************************************





Properties
==========

=====================  =================================  ===========
Name                   Type                               Cardinality
=====================  =================================  ===========
classificationLevel    CategoryRelationCode               0..1
description            InternationalStructuredString      0..1
formatPattern          ExternalControlledVocabularyEntry  0..1
logicalExpression      ExternalControlledVocabularyEntry  0..1
maximumValueExclusive  String                             0..1
maximumValueInclusive  String                             0..1
minimumValueExclusive  String                             0..1
minimumValueInclusive  String                             0..1
regularExpression      TypedString                        0..1
=====================  =================================  ===========


classificationLevel
###################
Indicates the type of relationship, nominal, ordinal, interval, ratio, or continuous. Use where appropriate for the representation type.


description
###########
A formal description of the set of values.


formatPattern
#############
A pattern for a number as described in Unicode Locale Data Markup Language (LDML) (http://www.unicode.org/reports/tr35/tr35.html) Part 3: Numbers  (http://www.unicode.org/reports/tr35/tr35-numbers.html#Number_Format_Patterns) and Part 4. Dates (http://www.unicode.org/reports/tr35/tr35-dates.html#Date_Format_Patterns) . Examples would be    #,##0.### to describe the pattern for a decimal number, or yyyy.MM.dd G 'at' HH:mm:ss zzz for a datetime pattern.


logicalExpression
#################
A logical expression where the values of "x" making the expression true are the members of the set of valid values.  Example: (all reals x such that  x >0) describes the real numbers greater than 0


maximumValueExclusive
#####################
A string denotFrom https://www.w3.org/TR/tabular-metadata/  5.11.2 maxExclusive: “An atomic property that contains a single number or string that is the maximum valid value (exclusive). The value of this property becomes the maximum exclusive annotation for the described datatype. See Value Constraints in [tabular-data-model] for details.”   DDI3.2 handles this with a Boolean isInclusive attribute. ing the maximumpossible value (excluding this value)


maximumValueInclusive
#####################
A string denoting the maximum possible value. From https://www.w3.org/TR/tabular-metadata/  5.11.2 maximum: “An atomic property that contains a single number or string that is the maximum valid value (inclusive); equivalent to maxInclusive. The value of this property becomes the maximum annotation for the described datatype. See Value Constraints in [tabular-data-model] for details.”


minimumValueExclusive
#####################
A string denoting the minimum possible value (excluding this value) From https://www.w3.org/TR/tabular-metadata/  5.11.2 minExclusive: “An atomic property that contains a single number or string that is the minimum valid value (exclusive). The value of this property becomes the minimum exclusive annotation for the described datatype. See Value Constraints in [tabular-data-model] for details.”   DDI3.2 handles this with a Boolean isInclusive attribute. 


minimumValueInclusive
#####################
A string denoting the minimum possible value. From https://www.w3.org/TR/tabular-metadata/  5.11.2 minimum: “An atomic property that contains a single number or string that is the minimum valid value (inclusive); equivalent to minInclusive. The value of this property becomes the minimum annotation for the described datatype. See Value Constraints in [tabular-data-model] for details.”


regularExpression
#################
A regular expression where strings matching the expression belong to the set of valid values. Use typeOfContent to specify the syntax of the regularExpression found in content.

