.. _examples:


PhysicalRecordSegment - Examples
********************************


The file below has four InstanceVariables: PersonId, SegmentId, AgeYr, HeightCm. The data for each person (identified by PersonId) is recorded in two segments (identified by SegmentId), "a" and "b". AgeYr is on physical segment a, and HeightCm is on segment b. These are the same data as described in the UnitSegmentLayout documentation. 1 a 22 1 b 183 2 a 45 2 b 175