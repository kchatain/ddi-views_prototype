.. _fields:


PhysicalRecordSegment - Properties and Relationships
****************************************************





Properties
==========

================  ==================  ===========
Name              Type                Cardinality
================  ==================  ===========
contains          DataPointIndicator  0..n
isOrdered         Boolean             0..1
physicalFileName  String              0..1
================  ==================  ===========


contains
########
A PhysicalRecordSegment has a sequence of DataPoints. Think of these as the cells in the row of a (spreadsheet) table.


isOrdered
#########
The DataPointIndicators for this PhysicalRecordSegment have index values that indicate the order of the DataPoints.


physicalFileName
################
Use when each physical segment is stored in its own file.




Relationships
=============

====================  ==========================  ===========  ================
Name                  Type                        Cardinality  allways external
====================  ==========================  ===========  ================
definingConcept       Concept                     0..n           no
hasPhysicalLayout     PhysicalSegmentLayout       0..n           no
isStructuredBy        DataPointRelationStructure  0..n           no
mapTo                 LogicalRecord               0..n           no
realizes              StructuredCollection        0..n           yes
representsPopulation  Population                  0..n           no
====================  ==========================  ===========  ================


definingConcept
###############
The conceptual basis for the collection of members.




hasPhysicalLayout
#################
A PhysicalRecordSegment is associated with a one or more reusable PhysicalLayouts 




isStructuredBy
##############
There may be cases where there is a more complex structure to a sequence of DataPoints. A cell containing a list, for example, might be considered to have nested DataPoints that are the elements of the list.




mapTo
#####
Every DataRecord has at o..n PhysicalRecordSegments, It is possible to describe a PhysicalDataProduct and its RecordRecordSegment(s) without reference to a DataRecord




realizes
########
Collections class realized by this class




representsPopulation
####################
A record segment may represent a specific population or sub-population within a larger set of segments. Allows for the identification of this filter for membership in the segment.



