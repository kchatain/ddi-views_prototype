.. _RepresentedMeasurement:


RepresentedMeasurement
**********************


Extends
=======
:ref:`Capture`


Definition
==========
The description of a reusable non-question measurement that can be used as a template that describes the components of a measurement. A type code can be used to describe the type of measure that was used.


Synonyms
========



Explanatory notes
=================
Non-question measurements often involve the use of specific machines or protocols to ensure consistency and comparability of the measure. The RepresentedMeasurement allows the reusable format and protocol to be housed in a repository for use by multiple studies or repeated studies.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst