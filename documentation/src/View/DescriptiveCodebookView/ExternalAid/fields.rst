.. _fields:


ExternalAid - Properties and Relationships
******************************************





Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
stimulusType  ExternalControlledVocabularyEntry  0..1
============  =================================  ===========


stimulusType
############


