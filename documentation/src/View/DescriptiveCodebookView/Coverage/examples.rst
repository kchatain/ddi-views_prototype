.. _examples:


Coverage - Examples
*******************


A survey might have ask people about motels they stayed in (topical coverage) in the year 2015 (temporal coverage), while they were travelling in Kansas (spatial coverage). This is different than the temporal, and spatial attributes of the population studied – (international travelers to the US surveyed in 2017).