.. _examples:


DataStore - Examples
********************


The data lineage of an individual BusinessProcess or an entire DataPipeline are both examples of a LogicalRecordRelationStructures. In a data lineage we can observe how LogicalRecords are connected within a BusinessProcess or across BusinessProcesses.