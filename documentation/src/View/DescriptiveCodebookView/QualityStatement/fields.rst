.. _fields:


QualityStatement - Properties and Relationships
***********************************************





Properties
==========

============  =============================  ===========
Name          Type                           Cardinality
============  =============================  ===========
displayLabel  LabelForDisplay                0..n
name          ObjectName                     0..n
overview      InternationalStructuredString  0..1
rationale     InternationalStructuredString  0..1
usage         InternationalStructuredString  0..1
============  =============================  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.


rationale
#########
Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

============  ========  ===========  ================
Name          Type      Cardinality  allways external
============  ========  ===========  ================
usesStandard  Standard  0..n           no
============  ========  ===========  ================


usesStandard
############
Identifies the external standard used and describes the level of compliance with the standard in terms specific aspects of the standard's content.



