.. _examples:


UnitDataRecord - Examples
*************************


The UnitDataRecord can be used to specify the structure of a table in a relational database. It can be used to specify the structure of both the fact and dimension tables in a data warehouse. It can be used to specify the structure of a big data table. Likewise it can be used to specify the structures of "column-based" and "row-based" tables. The UnitDataRecord may also form an information model if it is a StructuredCollection. Blood Pressure as described by an openEHR archetype is an information model. So is a collection of FHIR (HL7) resources that form an Electronic Health Record (EHR).