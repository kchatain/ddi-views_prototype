.. _fields:


LevelStructure - Properties and Relationships
*********************************************





Properties
==========

==============  =============================  ===========
Name            Type                           Cardinality
==============  =============================  ===========
containsLevel   Level                          0..n
name            ObjectName                     0..n
usage           InternationalStructuredString  0..1
validDateRange  DateRange                      0..1
==============  =============================  ===========


containsLevel
#############
The association of a level number in an ordered array with the description of that level


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text. Use in locations where you need to relay information on the usage of the instantiated class.


validDateRange
##############
The dates for which the level structure is valid.  A structured DateRange with start and end Date (both with the structure of Date and supporting the use of ISO and non-ISO date structures); Use to relate a period with a start and end date.

