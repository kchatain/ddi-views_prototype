.. _Unit:


Unit
****


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The object of interest in a process step related to the collection or use of observational data.


Synonyms
========



Explanatory notes
=================
In a traditional data table each column might represent some variable (measurement). Each row would represent the entity (Unit) to which those variables relate. Height measurements might be made on persons (UnitType) of primary school age (Universe) at Pinckney Elementary School on September 1, 2005 (Population). The height for Mary Roe (Unit) might be 139 cm. The Unit is not invariably tied to some value. A median income might be calculated for a block in the U.S. but then used as an attribute of a person residing on that block. For the initial measurement the Unit was the block. In the reuse the unit would be that specific person to which the value was applied. In a big data table each row represents a unit/variable double. Together a unit identifier and a variable identifier define the key. And for each key there is just one value – the measure of the unit on the variable. A big data table is sometimes referred to as a column-oriented data store whereas a traditional database is sometimes referred to as a row-oriented data store. The unit plays an identifier role in both types of data stores.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst