.. _fields:


PhysicalOrderRelationStructure - Properties and Relationships
*************************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasMemberRelation         PhysicalRecordSegmentRelation      0..n
hasRelationSpecification  RelationSpecification              0..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasMemberRelation
#################
Member relations that comprise the relation structure


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  =================  ===========  ================
Name      Type               Cardinality  allways external
========  =================  ===========  ================
realizes  RelationStructure  0..n           yes
========  =================  ===========  ================


realizes
########
Collection class realized by this class



