.. _RepresentedQuestion:


RepresentedQuestion
*******************


Extends
=======
:ref:`Capture`


Definition
==========
The description of a reusable question, that can be used as a template that describes the components of a question


Synonyms
========



Explanatory notes
=================
A question that is repeated across waves of a panel study can be reused and also allow reference to a RepresentedVariable. A question that has been tested for consistency of response and is used by multiple studies.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst