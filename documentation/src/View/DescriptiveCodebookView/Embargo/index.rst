.. _Embargo:


Embargo
*******


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Provides information about data that are not currently available because of policies established by the principal investigators and/or data producers. Embargo provides a name and label of the embargo, the dates covered by the embargo, the rationale or reason for the embargo, a reference to the agency establishing the embargo, and a reference to the agency enforcing the embargo.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst