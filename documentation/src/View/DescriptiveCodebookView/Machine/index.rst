.. _Machine:


Machine
*******


Extends
=======
:ref:`Agent`


Definition
==========
Mechanism or computer program used to implement a process.


Synonyms
========



Explanatory notes
=================
May be used as the target to describe how an action was performed. Relevent to data capture and data processing or wherever a role is performed by a Machine.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst