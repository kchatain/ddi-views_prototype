.. _fields:


Machine - Properties and Relationships
**************************************





Properties
==========

====================  =================================  ===========
Name                  Type                               Cardinality
====================  =================================  ===========
function              ExternalControlledVocabularyEntry  0..n
hasAccessLocation     AccessLocation                     0..1
machineInterface      ExternalControlledVocabularyEntry  0..n
name                  ObjectName                         0..n
ownerOperatorContact  ContactInformation                 0..1
typeOfMachine         ExternalControlledVocabularyEntry  0..1
====================  =================================  ===========


function
########
The function of the machine


hasAccessLocation
#################
The locations where the machine can be access


machineInterface
################
Specified the machine interface. Supports the use of a controlled vocabulary.


name
####
The name of the machine. A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


ownerOperatorContact
####################
Contact information for the owner/operator including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.


typeOfMachine
#############
The kind of machine used - software, web service, physical machine, from a controlled vocabulary

