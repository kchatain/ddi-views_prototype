.. _StudySeries:


StudySeries
***********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A collection of studies which can be structured as a simple sequence, or with a more complex structure.


Synonyms
========



Explanatory notes
=================
A set of studies may be defined in many ways. A study may be repeated over time. One or more studies may attempt to replicate an earlier study. The StudySeries allows for the description of the relationships among a set of studies. A simple ordered or unordered sequence of studies can be described via the "contains StudyIndicator" property. More complex relationships among studies may also be described using the optional "isStructuredBy StudyRelationsStructure".


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst