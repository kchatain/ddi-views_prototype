***********************
DescriptiveCodebookView
***********************
Purpose:
This view is intended to provide a structure for the information needed to understand and make intelligent use of a set of data without other communication from the person or organization producing the data. It can be used to move the data into and among multiple software platforms. This information is useful for discovery of the data.

The view includes basic information about how the data were collected, but does not provide machine actionable details.

The view provides information about:
Variables and the representation of the data for each, 
Basic statistics for individual variables
Universe and population, 
Spatial, temporal, and topical coverage
The study under which the data were created
Overviews of Methodology, Design, and Process
Physical storage


Use Cases:
Basic search to find data (general search, known item search) by users looking to use data. 
--Searches by variable, coverage, creators, time frame, population, methodology and more
Machine actionable ingest to statistical software allowing users to analyze the data
--An open standard for representing the metadata needed to read the data 
Informational content to support intelligent use and understanding of the data
--The essential information for interpreting and using the data (e.g. units of measurement, relevant population)


Target Audiences:
Individual researchers, research groups, National Statistical Organizations focusing on stand-alone codebooks, students, instructors, archives, and libraries. Software tool makers.

Restricted Classes:
DesignOverview references Precondition, Goal, and WorkflowProcess
InstanceVariable references RepresentedVariable, and ConceptualVariable, and the abstract class Act(InstrumentComponent, ComputationAction, StudyControl, and MetadataDrivenAction)
Methodology references Methodology
ProcessOverview references ProcessSequence
RepresentedMeasurement references ExternalAid
RepresentedQuestion references ExternalAid
SamplingProcedure references SamplingDesign, SamplingAlgorithm, and SamplingProcess
SentinelConceptualDomain references ConceptualSystem
SpatialCoverage references GeographicUnitClassification, and GeographicUnitTypeClassification
Study references AuthorizationSource
SubstantativeConceptualDomain references ConceptualSystem


A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Access/index.rst
   AgentListing/index.rst
   AgentRelationStructure/index.rst
   AlgorithmOverview/index.rst
   AttributeRole/index.rst
   AuthorizationSource/index.rst
   BoundingBox/index.rst
   Budget/index.rst
   Category/index.rst
   CategorySet/index.rst
   Code/index.rst
   CodeList/index.rst
   CodeRelationStructure/index.rst
   ComplianceStatement/index.rst
   Concept/index.rst
   Coverage/index.rst
   DataPoint/index.rst
   DataPointRelationStructure/index.rst
   DataStore/index.rst
   Datum/index.rst
   DesignOverview/index.rst
   Embargo/index.rst
   ExPostEvaluation/index.rst
   ExternalAid/index.rst
   ExternalMaterial/index.rst
   FundingInformation/index.rst
   GeographicExtent/index.rst
   GeographicUnit/index.rst
   IdentifierRole/index.rst
   ImplementedInstrument/index.rst
   Individual/index.rst
   InstanceVariable/index.rst
   InstanceVariableRelationStructure/index.rst
   Instruction/index.rst
   LevelStructure/index.rst
   LogicalRecordRelationStructure/index.rst
   Machine/index.rst
   MeasureRole/index.rst
   MethodologyOverview/index.rst
   Organization/index.rst
   PhysicalDataSet/index.rst
   PhysicalLayoutRelationStructure/index.rst
   PhysicalOrderRelationStructure/index.rst
   PhysicalRecordSegment/index.rst
   Population/index.rst
   ProcessOverview/index.rst
   QualityStatement/index.rst
   RecordRelation/index.rst
   RepresentedMeasurement/index.rst
   RepresentedQuestion/index.rst
   SamplingProcedure/index.rst
   SegmentByText/index.rst
   SentinelConceptualDomain/index.rst
   SentinelValueDomain/index.rst
   SpatialCoverage/index.rst
   Standard/index.rst
   StandardWeight/index.rst
   Study/index.rst
   StudyRelationStructure/index.rst
   StudySeries/index.rst
   SubstantiveConceptualDomain/index.rst
   SubstantiveValueDomain/index.rst
   TemporalCoverage/index.rst
   TopicalCoverage/index.rst
   Unit/index.rst
   UnitDataRecord/index.rst
   UnitDataViewpoint/index.rst
   UnitSegmentLayout/index.rst
   UnitType/index.rst
   Universe/index.rst
   ValueAndConceptDescription/index.rst
   ValueMapping/index.rst
   VariableCollection/index.rst
   VariableRelationStructure/index.rst
   VariableStatistics/index.rst



Graph
=====

.. graphviz:: /images/graph/DescriptiveCodebookView/DescriptiveCodebookView.dot