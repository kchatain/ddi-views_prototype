.. _fields:


SegmentByText - Properties and Relationships
********************************************





Properties
==========

======================  =======  ===========
Name                    Type     Cardinality
======================  =======  ===========
characterLength         Integer  0..1
endCharacterPosition    Integer  0..1
endLine                 Integer  0..1
startCharacterPosition  Integer  0..1
startLine               Integer  0..1
======================  =======  ===========


characterLength
###############
The number of characters in the segment. The segment may include text from multiple lines of the resource. If it does, the length includes any line termination characters.


endCharacterPosition
####################
The character position of the last character of the segment.  If endLine is specified, the count begins at character 1 of endLine. If startLine and endLine are not specified, the count begins at character 1 of the first line of the resource and the count includes any line termination characters. The resulting segment may include text from multiple lines of the resource.


endLine
#######
The last line on which to count characters. If missing this defaults to startLine. 


startCharacterPosition
######################
The character position of the first character of the segment, with the count beginning at character 1 of startLine. 


startLine
#########
The line number, where 1 is the first line, on which to begin counting characters. If missing this defaults to 1 (the first line).

