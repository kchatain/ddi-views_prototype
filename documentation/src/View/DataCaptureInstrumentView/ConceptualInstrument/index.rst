.. _ConceptualInstrument:


ConceptualInstrument
********************


Extends
=======
:ref:`WorkflowProcess`


Definition
==========
Design plan for creating a data capture tool.


Synonyms
========
Idea; Intention; Theory; Design; LogicalInstrument


Explanatory notes
=================
A single ConceptualInstrument would contain workflow sequences of the intended flow logic of the instrument, which could be implemented as a web survey or paper survey, utilizing the same sequence and questions. Similarly a single ConceptualInstrument would contain workflow sequences of the intended flow logic of the instrument, which could be implemented as a protocol delivered by a nurse or by self-administration.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst