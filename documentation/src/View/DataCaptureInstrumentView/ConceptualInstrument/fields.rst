.. _fields:


ConceptualInstrument - Properties and Relationships
***************************************************





Properties
==========

============  =============================  ===========
Name          Type                           Cardinality
============  =============================  ===========
description   InternationalStructuredString  0..1
displayLabel  LabelForDisplay                0..n
name          ObjectName                     0..n
usage         InternationalStructuredString  0..1
============  =============================  ===========


description
###########
A description of the purpose or use of the Member. May be expressed in multiple languages and supports the use of structured content. 


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text. 


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

=========  =======  ===========  ================
Name       Type     Cardinality  allways external
=========  =======  ===========  ================
organizes  Capture  0..n           no
=========  =======  ===========  ================


organizes
#########
Refers to the RepresentedMeasures and RepresentedQuestions used by the ConceptualInstrument.



