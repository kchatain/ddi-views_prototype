.. _ConceptSystem:


ConceptSystem
*************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A set of Concepts structured by the relations among them. [GSIM 1.1]


Synonyms
========



Explanatory notes
=================
Note that this structure can be used to structure Concept, Classification, Universe, Population, Unit Type and any other class that extends from Concept.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst