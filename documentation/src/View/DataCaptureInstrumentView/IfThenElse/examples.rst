.. _examples:


IfThenElse - Examples
*********************


An IfThenElse object describes the conditional logic in the flow of a questionnaire or other data collection instrument, where if a stated condition is met, one path is followed through the flow, and if the stated condition is met, another path is taken.