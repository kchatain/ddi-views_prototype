.. _examples:


WorkflowStepSequence - Examples
*******************************


A WorkflowStepSequence can be used to describe the flow of questions in a questionnaire. A WorkflowStepSequence can be used to decompose a BusinessProcess like data integration or anonymization into a series of steps.