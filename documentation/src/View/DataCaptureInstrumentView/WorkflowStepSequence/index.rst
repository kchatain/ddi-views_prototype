.. _WorkflowStepSequence:


WorkflowStepSequence
********************


Extends
=======
:ref:`WorkflowControlStep`


Definition
==========
A WorkflowStepSequence controls the order of WorkflowSteps by defining a simple sequence. WorkflowSteps that may be used in a WorkflowStepSequence may be of many subtypes, covering simple or computational acts, conditional steps such as an IfThenElse, temporally defined ordering controls such as Split and SplitJoin, or others designed for various specializations of the WorkflowProcess. . WorkflowSteps in a WorkflowStepSequence exchange variables and values by way of output parameters, input parameters and their bindings.


Synonyms
========



Explanatory notes
=================
If more complex temporal or graph ordering is required use the subtype StructuredWorkflowSteps. The relationship "executes" in this instance of a WorkflowControlStep is an optional means of noting the set of WorkflowSteps managed by the sequence. Sequencing is done using orderedStep. This is currently an anomoly that should be resolved in future discussion application of transformation rules from UML model.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst