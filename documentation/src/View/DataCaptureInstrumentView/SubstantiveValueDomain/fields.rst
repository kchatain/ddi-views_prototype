.. _fields:


SubstantiveValueDomain - Properties and Relationships
*****************************************************





Relationships
=============

=====================  ===========================  ===========  ================
Name                   Type                         Cardinality  allways external
=====================  ===========================  ===========  ================
describedValueDomain   ValueAndConceptDescription   0..n           yes
enumeratedValueDomain  EnumerationDomain            0..n           no
takesConceptsFrom      SubstantiveConceptualDomain  0..n           yes
=====================  ===========================  ===========  ================


describedValueDomain
####################
A formal description of the set of valid values - for described value domains.




enumeratedValueDomain
#####################
Any subtype of an enumeration domain enumerating the set of valid values.




takesConceptsFrom
#################
Corresponding conceptual definition given by an SubstantiveConceptualDomain.



