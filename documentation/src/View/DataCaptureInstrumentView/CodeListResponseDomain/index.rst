.. _CodeListResponseDomain:


CodeListResponseDomain
**********************


Extends
=======
:ref:`ResponseDomain`


Definition
==========
A response domain capturing a coded response (where both codes and their related category value are displayed) for a question. This response domain allows the single selection of one coded response.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst