.. _ExternalAid:


ExternalAid
***********


Extends
=======
:ref:`ExternalMaterial`


Definition
==========
Any external material used in an instrument that aids or facilitates data capture, or that is presented to a respondent and about which measurements are made.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst