.. _examples:


RepeatWhile - Examples
**********************


A RepeatWhile loop is similar to a RepeatUntil loop, except that it tests the Boolean expression before the loop rather than after. Thus, a RepeatUntil loop always executes the loop once, whereas if the Boolean expression is initially False, a RepeatWhile loop does not execute the loop at all. For example, RepeatWhile may be used to pre-test a certain condition like if "the number of new people in a household is greater than 0" in which case a block of questions might be asked for each new person.