.. _fields:


InstanceMeasurement - Properties and Relationships
**************************************************





Relationships
=============

============  ======================  ===========  ================
Name          Type                    Cardinality  allways external
============  ======================  ===========  ================
instantiates  RepresentedMeasurement  0..n           no
============  ======================  ===========  ================


instantiates
############
The measurement being instantiated in the data collection process



