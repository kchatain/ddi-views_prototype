.. _TextResponseDomain:


TextResponseDomain
******************


Extends
=======
:ref:`ResponseDomain`


Definition
==========
A response domain capturing a textual response including the length of the text and restriction of content using a regular expression.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst