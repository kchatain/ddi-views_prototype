.. _ScaleResponseDomain:


ScaleResponseDomain
*******************


Extends
=======
:ref:`ResponseDomain`


Definition
==========
A response domain capturing a scaled response, such as a Likert scale. Note: This item still must be modeled and is incomplete at this time.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst