.. _fields:


BooleanResponseDomain - Properties and Relationships
****************************************************





Relationships
=============

===========  ========  ===========  ================
Name         Type      Cardinality  allways external
===========  ========  ===========  ================
forCategory  Category  0..n           no
===========  ========  ===========  ================


forCategory
###########
The category to which a boolean response is requested.



