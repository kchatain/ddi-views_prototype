.. _fields:


ClassificationIndexEntry - Properties and Relationships
*******************************************************





Properties
==========

=================  ===================  ===========
Name               Type                 Cardinality
=================  ===================  ===========
codingInstruction  CommandCode          0..1
entry              InternationalString  0..1
validDates         DateRange            0..1
=================  ===================  ===========


codingInstruction
#################
Additional information which drives the coding process. Required when coding is dependent upon one or many other factors.


entry
#####
Text describing the type of object/unit or object property.


validDates
##########
Date from which the Classification Index Entry became valid (startDate). The date must be defined if the Classification Index Entry belongs to a floating Classification Index. Date at which the Classification Index Entry became invalid (endDate). The date must be defined if the Classification Index Entry belongs to a floating Classification Index and is no longer valid. 




Relationships
=============

========  ================  ===========  ================
Name      Type              Cardinality  allways external
========  ================  ===========  ================
realizes  CollectionMember  0..n           yes
========  ================  ===========  ================


realizes
########
Class of the Collection pattern realized by this class. 



