.. _Category:


Category
********


Extends
=======
:ref:`Concept`


Definition
==========
A Concept whose role is to define and measure a characteristic.


Synonyms
========



Explanatory notes
=================
The Category is a Concept. It can have multiple name and display label properties as well as a definition and some descriptive text. As a "Signified" class there can be one or more "Sign" classes (e.g. a Code) that denotes it with some representation. The relationship is from the Code to the Category.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst