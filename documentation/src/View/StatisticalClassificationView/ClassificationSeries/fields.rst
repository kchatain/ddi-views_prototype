.. _fields:


ClassificationSeries - Properties and Relationships
***************************************************





Properties
==========

========================  ==================================  ===========
Name                      Type                                Cardinality
========================  ==================================  ===========
contains                  StatisticalClassificationIndicator  0..n
context                   ExternalControlledVocabularyEntry   0..1
isOrdered                 Boolean                             0..1
keyword                   ExternalControlledVocabularyEntry   0..n
name                      ObjectName                          0..n
objectsOrUnitsClassified  ExternalControlledVocabularyEntry   0..1
purpose                   InternationalStructuredString       0..1
subject                   ExternalControlledVocabularyEntry   0..n
type                      CollectionType                      0..1
========================  ==================================  ===========


contains
########
MemberIndicator Allows for the identification of the member and optionally provides an index for the member within an ordered array


context
#######
ClassificationSeries can be designed in a specific context. Supports the use of and External Controlled Vocabulary.


isOrdered
#########
If members are ordered set to true, if unordered set to false.


keyword
#######
A ClassificationSeries can be associated with one or a number of keywords. 


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


objectsOrUnitsClassified
########################
A ClassificationSeries is designed to classify a specific type of object/unit according to a specific attribute.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


subject
#######
Areas of statistics in which the ClassificationSeries is implemented.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.




Relationships
=============

===============  ==========================================  ===========  ================
Name             Type                                        Cardinality  allways external
===============  ==========================================  ===========  ================
definingConcept  Concept                                     0..n           no
isStructuredBy   StatisticalClassificationRelationStructure  0..n           no
owner            Agent                                       0..n           no
realizes         StructuredCollection                        0..n           yes
===============  ==========================================  ===========  ================


definingConcept
###############
The conceptual basis for the collection of members.




isStructuredBy
##############
Description of a complex structure for the Collection. A collection may be structured in more than one way for different uses.




owner
#####
The statistical office or other authority, which created and maintains the StatisticalClassification (s) related to the ClassificationSeries. A ClassificationSeries may have several owners.




realizes
########
Class of the Collection pattern realized by this class.



