.. _StatisticalClassificationRelationStructure:


StatisticalClassificationRelationStructure
******************************************


Extends
=======
:ref:`Identifiable`


Definition
==========
A structure for describing the complex relationship between statistical classifications in a classification series


Synonyms
========



Explanatory notes
=================
Can use relation specification information to more fully describe the relationship between members such as parent/child, whole/part, general/specific, equivalence, etc.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst