.. _fields:


StatisticalClassification - Properties and Relationships
********************************************************





Properties
==========

=================  =============================  ===========
Name               Type                           Cardinality
=================  =============================  ===========
availableLanguage                                 0..n
changeFromBase     InternationalStructuredString  0..1
contains           ClassificationItemIndicator    0..n
copyright          InternationalString            0..n
displayLabel       LabelForDisplay                0..n
isCurrent          Boolean                        0..1
isFloating         Boolean                        0..1
purposeOfVariant   InternationalStructuredString  0..1
rationale          InternationalStructuredString  0..1
releaseDate        Date                           0..1
updateChanges      InternationalStructuredString  0..n
usage              InternationalStructuredString  0..1
validDates         DateRange                      0..1
=================  =============================  ===========


availableLanguage
#################
A list of languages in which the Statistical Classification is available. Supports the indication of multiple languages within a single property. Supports use of codes defined by the RFC 1766.


changeFromBase
##############
Describes the relationship between the variant and its base Statistical Classification, including regroupings, aggregations added and extensions. (Source: GSIM StatisticalClassification/Changes from base Statistical Classification)


contains
########
Allows for the identification of the member and optionally provides an index for the member within an ordered array


copyright
#########
Copyright of the statistical classification.


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


isCurrent
#########
Indicates if the Statistical Classification is currently valid.


isFloating
##########
Indicates if the Statistical Classification is a floating classification. In a floating statistical classification, a validity period should be defined for all Classification Items which will allow the display of the item structure and content at different points of time. (Source: GSIM StatisticalClassification/Floating


purposeOfVariant
################
If the Statistical Classification is a variant, notes the specific purpose for which it was developed. (Source: GSIM StatisticalClassification/Purpose of variant)


rationale
#########
Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.


releaseDate
###########
Date the Statistical Classification was released


updateChanges
#############
Summary description of changes which have occurred since the most recent classification version or classification update came into force.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.


validDates
##########
The date the statistical classification enters production use and the date on which the Statistical Classification was superseded by a successor version or otherwise ceased to be valid. (Source: GSIM Statistical Classification)




Relationships
=============

==============  ===================================  ===========  ================
Name            Type                                 Cardinality  allways external
==============  ===================================  ===========  ================
distributedAs   ExternalMaterial                     0..n           no
isIndexedBy     ClassificationIndex                  0..n           no
isMaintainedBy  Organization                         0..n           no
isStructuredBy  ClassificationItemRelationStructure  0..n           no
predecessor     StatisticalClassification            0..n           no
realizes        StructuredCollection                 0..n           yes
successor       StatisticalClassification            0..n           no
variantOf       StatisticalClassification            0..n           no
==============  ===================================  ===========  ================


distributedAs
#############
Description and link to a publication, including print, PDF, HTML and other electronic formats, in which the Statistical Classification has been published. This is similar to dcat:Distribution.




isIndexedBy
###########
ClassificationIndex(es) related to the StatisticalClassification.




isMaintainedBy
##############
Organization, agency, or group within an agency responsible for the maintenance and upkeep of the statistical classification.




isStructuredBy
##############
A complex structure, hierarchical, for a ClassificationItem. Restricted to single structure.




predecessor
###########
Statistical Classification superseded by the actual Statistical Classification (for those Statistical Classifications that are versions or updates).






realizes
########
Collection class realized by this class




successor
#########
Statistical Classification that supersedes the actual Statistical Classification (for those Statistical Classifications that are versions or updates), 




variantOf
#########
Statistical Classification on which the current variant is based, and any subsequent versions of that Statistical Classification to which it is also applicable.




