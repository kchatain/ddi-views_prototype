.. _fields:


AuthorizationSource - Properties and Relationships
**************************************************





Properties
==========

========================  =============================  ===========
Name                      Type                           Cardinality
========================  =============================  ===========
authorizationDate         Date                           0..1
legalMandate              InternationalString            0..1
purpose                   InternationalStructuredString  0..1
statementOfAuthorization  InternationalStructuredString  0..1
========================  =============================  ===========


authorizationDate
#################
Identifies the date of Authorization.


legalMandate
############
Provide a legal citation to a law authorizing the study/data collection. For example, a legal citation for a law authorizing a country's census.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


statementOfAuthorization
########################
Text of the authorization (law, mandate, approved business case).




Relationships
=============

================  =====  ===========  ================
Name              Type   Cardinality  allways external
================  =====  ===========  ================
authorizingAgent  Agent  0..n           no
================  =====  ===========  ================


authorizingAgent
################
References the authorizing agent generally described as an organization or individual



