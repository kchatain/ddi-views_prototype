.. _examples:


LevelStructure - Examples
*************************


ISCO-08 (Major, Sub-Major, and Minor) or NAICS (2 digit sector codes, 3 digit subsector code list, 4 digit industry group code list, and 5 digit industry code list)