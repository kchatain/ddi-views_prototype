.. _CategorySet:


CategorySet
***********


Extends
=======
:ref:`ConceptSystem`


Definition
==========
Specialization of a Concept System focusing on sets of categories used as specifications for a generalized concept. Can be used directly by questions to express a set of categories for a response domain or as the related source for a CodeList or StatisticalClassification


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst