.. _fields:


CategorySet - Properties and Relationships
******************************************





Properties
==========

========  =================  ===========
Name      Type               Cardinality
========  =================  ===========
contains  CategoryIndicator  0..n
========  =================  ===========


contains
########
MemberIndicator Allows for the identification of the member and optionally provides an index for the member within an ordered array




Relationships
=============

==============  =========================  ===========  ================
Name            Type                       Cardinality  allways external
==============  =========================  ===========  ================
isStructuredBy  CategoryRelationStructure  0..n           no
==============  =========================  ===========  ================


isStructuredBy
##############
A structure containing specific relationships between members of the collection.



