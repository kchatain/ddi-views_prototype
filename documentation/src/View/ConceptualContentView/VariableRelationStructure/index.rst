.. _VariableRelationStructure:


VariableRelationStructure
*************************


Extends
=======
:ref:`Identifiable`


Definition
==========
RelationStructure for use with any mixture of Variables in the Variable Cascade (Conceptual, Represented, Instance).


Synonyms
========



Explanatory notes
=================
A VariableCollection can be described as a simple list, or as having a more complex structure. All of the "administrative" variables for a study might be described as just a simple list without a VariableRelationStructure. If, however, the administrative variables needed to be described in subgroups, a VariableRelationStructure could be employed.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst