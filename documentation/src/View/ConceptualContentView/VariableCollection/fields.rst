.. _fields:


VariableCollection - Properties and Relationships
*************************************************





Properties
==========

================  =================================  ===========
Name              Type                               Cardinality
================  =================================  ===========
contains          VariableIndicator                  0..n
displayLabel      LabelForDisplay                    0..n
groupingSemantic  ExternalControlledVocabularyEntry  0..1
isOrdered         Boolean                            0..1
name              ObjectName                         0..n
purpose           InternationalStructuredString      0..1
type              CollectionType                     0..1
usage             InternationalStructuredString      0..1
================  =================================  ===========


contains
########
Allows for the identification of the member and optionally provides an index for the member within an ordered array


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text. 


groupingSemantic
################
A semantic term defining the factor used for defining this group


isOrdered
#########
If members are ordered set to true, if unordered set to false.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

===============  =========================  ===========  ================
Name             Type                       Cardinality  allways external
===============  =========================  ===========  ================
definingConcept  Concept                    0..n           no
isStructuredBy   VariableRelationStructure  0..n           no
realizes         StructuredCollection       0..n           yes
===============  =========================  ===========  ================


definingConcept
###############
The conceptual basis for the collection of members.




isStructuredBy
##############
This supplies information for linking disparate records in a dataset. RecordRelations describe corresponding InstanceVariables between record types.




realizes
########
Collection class realized by the class



