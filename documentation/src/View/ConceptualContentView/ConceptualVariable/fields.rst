.. _fields:


ConceptualVariable - Properties and Relationships
*************************************************





Properties
==========

===============  =============================  ===========
Name             Type                           Cardinality
===============  =============================  ===========
descriptiveText  InternationalStructuredString  0..1
===============  =============================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.




Relationships
=============

============================  ===========================  ===========  ================
Name                          Type                         Cardinality  allways external
============================  ===========================  ===========  ================
takesSentinelConceptsFrom     SentinelConceptualDomain     0..n           no
takesSubstantiveConceptsFrom  SubstantiveConceptualDomain  0..n           no
usesConcept                   Concept                      0..n           no
usesUnitType                  UnitType                     0..n           no
============================  ===========================  ===========  ================


takesSentinelConceptsFrom
#########################
Identifies the ConceptualDomain containing the set of sentinel concepts used to describe the  ConceptualVariable.




takesSubstantiveConceptsFrom
############################
Identifies the ConceptualDomain containing the set of substantive concepts used to describe the  ConceptualVariable.




usesConcept
###########
Reference to a Concept that is being used




usesUnitType
############
Identifies the UnitType associated with the ConceptualVariable



