.. _examples:


RepresentedVariable - Examples
******************************


The pair (Number of Employees, Integer), where "Number of Employees" is the characteristic of the population (Variable) and "Integer" is how that measure will be represented (Value Domain).