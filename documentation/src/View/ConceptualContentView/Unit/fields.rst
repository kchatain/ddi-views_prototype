.. _fields:


Unit - Properties and Relationships
***********************************





Properties
==========

============  =============================  ===========
Name          Type                           Cardinality
============  =============================  ===========
definition    InternationalStructuredString  0..1
displayLabel  LabelForDisplay                0..n
name          ObjectName                     0..n
============  =============================  ===========


definition
##########
Natural language statement conveying the meaning of a concept, differentiating it from other concepts. Supports the use of multiple languages and structured text.


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 




Relationships
=============

===========  ========  ===========  ================
Name         Type      Cardinality  allways external
===========  ========  ===========  ================
hasUnitType  UnitType  0..n           no
===========  ========  ===========  ================


hasUnitType
###########
The UnitType of the Unit



