.. _examples:


BusinessProcess - Examples
**************************


In a single BusinessProcess we might receive data from a data source or merge data from different data sources or create new variables and recodes or format data for data dissemination