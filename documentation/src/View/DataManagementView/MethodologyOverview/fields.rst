.. _fields:


MethodologyOverview - Properties and Relationships
**************************************************





Properties
==========

====================  =================================  ===========
Name                  Type                               Cardinality
====================  =================================  ===========
name                  ObjectName                         0..n
overview              InternationalStructuredString      0..1
rationale             InternationalStructuredString      0..1
subjectOfMethodology  ExternalControlledVocabularyEntry  0..1
usage                 InternationalStructuredString      0..1
====================  =================================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided, provide a context to differentiate usage.


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.


rationale
#########
Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.


subjectOfMethodology
####################
A term describing the subject of the Methodology. Supports the use of a controlled vocabulary


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

====================  ===================  ===========  ================
Name                  Type                 Cardinality  allways external
====================  ===================  ===========  ================
componentMethodology  MethodologyOverview  0..n           no
hasDesign             DesignOverview       0..n           no
isDescribedIn         ExternalMaterial     0..n           no
isExpressedBy         AlgorithmOverview    0..n           no
realizes              Methodology          0..n           yes
====================  ===================  ===========  ================


componentMethodology
####################
A methodology which is a component part of this methodology. The target may be any realized Methodology. This allows the DescribedMethodology to incorporate more explicit or detailed methodologies for different forms of use without replicating the descriptive content.




hasDesign
#########
The design used by the methodology. A restriction of the Methdology class to constrain the target to DescribedDesign.




isDescribedIn
#############
An external document or other source that provides descriptive information on the object.




isExpressedBy
#############
The algorithm which expresses the methodology. A restriction of the Methdology class to constrain the target to DescribedAlgorithm.




realizes
########
A realization of the pattern class Methodology



