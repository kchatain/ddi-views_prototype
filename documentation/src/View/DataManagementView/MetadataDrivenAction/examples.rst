.. _examples:


MetadataDrivenAction - Examples
*******************************


Some data management systems present users with a menu of actions they can use in a sequence to perform a specific task. The system engages the user in a dialog driven by an action template. The dialog completes the template which the system then uses to perform the action. The user doesn't write any code.