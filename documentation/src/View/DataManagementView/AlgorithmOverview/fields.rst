.. _fields:


AlgorithmOverview - Properties and Relationships
************************************************





Properties
==========

==================  =================================  ===========
Name                Type                               Cardinality
==================  =================================  ===========
overview            InternationalStructuredString      0..1
subjectOfAlgorithm  ExternalControlledVocabularyEntry  0..1
==================  =================================  ===========


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.


subjectOfAlgorithm
##################
A term identifying the subject of the algorithm being described. Supports the use of a controlled vocabulary




Relationships
=============

=============  ================  ===========  ================
Name           Type              Cardinality  allways external
=============  ================  ===========  ================
isDescribedIn  ExternalMaterial  0..n           no
realizes       Algorithm         0..n           yes
=============  ================  ===========  ================


isDescribedIn
#############
An external document or other source that provides descriptive information on the object.




realizes
########
A realization of the pattern class Algorithm



