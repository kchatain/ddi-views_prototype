.. _examples:


Study - Examples
****************


ICPSR study 35575 Americans and the Arts [1973 - 1992] (ICPSR 35575). https://www.icpsr.umich.edu/icpsrweb/ICPSR/studies/35575?dataFormat%5B0%5D=SAS&keyword%5B0%5D=public+opinion&geography%5B0%5D=United+States&searchSource=revise