.. _SplitJoin:


SplitJoin
*********


Extends
=======
:ref:`TemporalRelationControlStep`


Definition
==========
SplitJoin consists of process steps that are executed concurrently (execution with barrier synchronization). That is, SplitJoin completes when all of its components processes have completed.


Synonyms
========



Explanatory notes
=================
Supports parallel processing that requires completion of all included process steps to exit.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst