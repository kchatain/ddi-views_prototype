.. _Loop:


Loop
****


Extends
=======
:ref:`ConditionalControlStep`


Definition
==========
Iterative control structure to be repeated a specified number of times based on one or more conditions. Inside the loop, one or more Workflow Steps are evaluated and processed in the order they appear.


Synonyms
========
ForLoop


Explanatory notes
=================
The Workflow Sequence contained in the Loop is executed until the condition is met, and then control is handed back to the containing control construct.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst