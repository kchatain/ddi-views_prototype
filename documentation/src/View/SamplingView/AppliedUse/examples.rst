.. _examples:


AppliedUse - Examples
*********************


Links one or more guides for the use of a sample (result) obtained in the first stage of complex sample with the unit type. A sample of Counties from which households will be selected in the next sample stage.