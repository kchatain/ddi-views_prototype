.. _CodeRelationStructure:


CodeRelationStructure
*********************


Extends
=======
:ref:`Identifiable`


Definition
==========
Relation structure of codes within a codelist. Allows for the specification of complex relationships among codes.


Synonyms
========



Explanatory notes
=================
The CodeRelationStructure employs a set of CodeRelations to describe the relationship among concepts. Each CodeRelation is a one to many description of connections between codes. Together they might commonly describe relationships as complex as a hierarchy.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst