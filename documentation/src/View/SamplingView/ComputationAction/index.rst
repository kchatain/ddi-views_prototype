.. _ComputationAction:


ComputationAction
*****************


Extends
=======
:ref:`Act`


Definition
==========
Provides an extensible framework for specific computation or transformation objects.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst