.. _Universe:


Universe
********


Extends
=======
:ref:`Concept`


Definition
==========
A defined set or class of people, entities, events, or objects, with no specification of time and geography, contextualizing a Unit Type


Synonyms
========



Explanatory notes
=================
Universe sits in a hierarchy between UnitType and Population, with UnitType being most general and Population most specific. A Universe is a set of entities defined by a more narrow specification than that of an underlying UnitType. A Population further narrows the specification to a specific time and geography. If the Universe consists of people, a number of factors may be used in defining membership in the Universe, such as age, sex, race, residence, income, veteran status, criminal convictions, etc. The universe may consist of elements other than persons, such as housing units, court cases, deaths, countries, etc. A universe may be described as "inclusive" or "exclusive". Not to be confused with Population, which includes the specification of time and geography.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst