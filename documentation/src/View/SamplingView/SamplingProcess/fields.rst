.. _fields:


SamplingProcess - Properties and Relationships
**********************************************





Properties
==========

========  =============================  ===========
Name      Type                           Cardinality
========  =============================  ===========
overview  InternationalStructuredString  0..1
========  =============================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.




Relationships
=============

===================  ======================  ===========  ================
Name                 Type                    Cardinality  allways external
===================  ======================  ===========  ================
hasResults           SamplePopulationResult  0..n           no
implementsAlgorithm  SamplingAlgorithm       0..n           no
isDiscussedIn        ExternalMaterial        0..n           no
isSpecifiedBy        WorkflowStepSequence    0..n           no
realizes             Process                 0..n           yes
===================  ======================  ===========  ================


hasResults
##########
Results of the Sampling Process. Restriction to SamplePopulation, a  type of Result 




implementsAlgorithm
###################
The sampling algorithm implanted by this sampling process. Constraint of the relationship implementsAlgorithm in Process pattern class by limiting the target to a SamplingAlgorithm




isDiscussedIn
#############
Description and link to external material discussing this Sampling Process




isSpecifiedBy
#############
The Process Sequence which initiate the process flow and describes the sequence of the process flow




realizes
########
Realizes the pattern Process



