.. _examples:


RepeatUntil - Examples
**********************


A RepeatUntil loop is similar to a RepeatWhile loop, except that it tests the Boolean expression after the loop rather than before. Thus, a RepeatUntil loop always executes the loop once, whereas if the Boolean expression is initially False, a RepeatWhile loop does not execute the loop at all. For example, in a household there is at last one person and a loop might contain a block of person questions. After the first person the last question in the block might be "Anyone else in the household"? The RepeatUntil would continue iterating until there was no-one else in the household.