.. _examples:


Precondition - Examples
***********************


Using goals and preconditions processes can be chained to form work flows. Note that these work flows do not include data flows. Instead data flows consisting ouf outputs, inputs and bindings are a separate swim lane.