.. _Precondition:


Precondition
************


Extends
=======
:ref:`BusinessFunction`


Definition
==========
A precondition is a state. The state includes one or more goals that were previously achieved. This state is the necessary condition before a process can begin.


Synonyms
========



Explanatory notes
=================
A precondition related to a design defines the state that must exist in order for a design being applied. For example in applying a Sampling Design there may be a precondition for the existance of a sampling frame meeting certain specifications.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst