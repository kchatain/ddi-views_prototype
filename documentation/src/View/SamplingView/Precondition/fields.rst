.. _fields:


Precondition - Properties and Relationships
*******************************************





Relationships
=============

==================  ================  ===========  ================
Name                Type              Cardinality  allways external
==================  ================  ===========  ================
basedOnPriorResult  Result            0..n           no
isDiscussedIn       ExternalMaterial  0..n           no
==================  ================  ===========  ================


basedOnPriorResult
##################
The Precondition may be based on the Results of earlier Processes, indicated with this relationship.




isDiscussedIn
#############
Identifies material discussing the precondition. The material may be in DDI or other format.



