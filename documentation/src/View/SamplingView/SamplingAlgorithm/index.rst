.. _SamplingAlgorithm:


SamplingAlgorithm
*****************


Extends
=======
:ref:`AlgorithmOverview`


Definition
==========
Generic description of a sampling algorithm expressed by a Sampling Design and implemented by a Sampling Process. May include common algebraic formula or statistical package instructions (use of specific selection models).


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst