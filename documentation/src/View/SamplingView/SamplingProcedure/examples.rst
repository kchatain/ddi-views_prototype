.. _examples:


SamplingProcedure - Examples
****************************


Census of Population and Housing, 1980 Summary Tape File 4, chapter on Technical Information. Overview is the Introduction generally describing the sample; there is a sample design section which provides a more detailed overview of the design. Additional external documentation provides detailed processes for sample adjustments needed for small area estimations, sample frames etc.