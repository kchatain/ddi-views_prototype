.. _CodeList:


CodeList
********


Extends
=======
:ref:`EnumerationDomain`


Definition
==========
A list of Codes and associated Categories. May be flat or hierarchical. A hierarchical structure may have an indexed order for intended presentation even though the content within levels of the hierarchy are conceptually unordered. For hierarchical structures ClassificationRelationStructure is used to provide additional information on the structure and organization of the categories. Note that a CategorySet can be structured by a ClassificationRelationStructure without the need for associating any Codes with the Categories. This allows for the creation of a CategorySet, for example for a response domain, without an associated CodeList.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst