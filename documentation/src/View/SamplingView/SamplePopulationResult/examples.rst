.. _examples:


SamplePopulationResult - Examples
*********************************


A list of phone numbers representing a random sample from a telephone book. The Counties selected from within a State from which Households will be selected using another sampling method.