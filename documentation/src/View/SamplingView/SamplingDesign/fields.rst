.. _fields:


SamplingDesign - Properties and Relationships
*********************************************





Relationships
=============

==================  =================  ===========  ================
Name                Type               Cardinality  allways external
==================  =================  ===========  ================
expressesAlgorithm  SamplingAlgorithm  0..n           no
implementedBy       SamplingProcess    0..n           no
specifiesGoal       SamplingGoal       0..n           no
==================  =================  ===========  ================


expressesAlgorithm
##################
Is an expression of this Sampling Algorithm




implementedBy
#############
The design of the expressed algorithm is implemented by the Sampling Process. Constrains impliementedBy of the Design Pattern by limiting the target to a Sampling Process.




specifiesGoal
#############
Specification of the goal for the Sampling Design. Constrained to SamplingGoal.



