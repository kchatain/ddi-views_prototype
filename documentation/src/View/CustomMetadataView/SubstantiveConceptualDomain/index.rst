.. _SubstantiveConceptualDomain:


SubstantiveConceptualDomain
***************************


Extends
=======
:ref:`ConceptualDomain`


Definition
==========
Set of valid Concepts. The Concepts can be described by either enumeration or by an expression.


Synonyms
========



Explanatory notes
=================
A ConceptualVariable links a UnitType to a SubstantiveConceptualDomain. The latter can be an enumeration or description of the values (Signified) that the variable may take on. In the enumerated case these are the categories in a CategorySet that can be values, not the codes that represent the values. An example might be the conceptual domain for a variable representing self-identified gender. An enumeration might include the concept of “male” and the concept of “female”. These, in turn, would be represented in a SubstantiveValueDomain by codes in a CodeList like “m” and “f”, or “0” and “1”. A conceptual domain might be described through a ValueAndConceptDescription’s description property of “a real number greater than 0” or through a more formal logicalExpression of “(all reals x such that x >0) “. Even in the described case, what is being described are conceptual, not the symbols used to represent the values. This may be a subtle distinction, but allows specifying that the same numeric value might be represented by 32 bits or by 64 bits or by an Arabic numeral or a Roman numeral.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst