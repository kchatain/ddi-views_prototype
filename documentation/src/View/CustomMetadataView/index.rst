******************
CustomMetadataView
******************
Purpose:
The DDI4 CustomMetadataView allows for the definition of a set of keys and their interrelationships. This structure can be shared and reused by a community. These keys can be used in key-value pairs to enter metadata that is more actionable than would be if an uncontrolled set of keys were employed. They could also be used in a NoSQL key/value store.

Use Cases:
User defined structures for notes. This corresponds to the UserAttributePair in DDI3.2, but allows for additional specification of a structure for the keys and a datatype and cardinalities for the values.
User defined structures for specifying the location of parts of qualitative objects. The example below shows the location of words or sentences from material arranged into volume, chapter, sentence and word
User defined structures for specifying the location of  parts of cited materials

Target Audiences:
Users of DDI4 needing structured extensions to be shared by their community. A user community, for example, could share the structure for locating material by volume, chapter, sentence and word.

Restricted Classes:
The list below displays each class in the View that is extended by classes not in the view.
Concept is extended by ConceptualVariable and Population.
ExternalMaterial is extended by ExternalAid.
Code is extended by ClassificationItem.
CodeList is extended by GeographicUnitClassification, GeographicUnitTypeClassification and StatisticalClassification.

General Documentation:
The keys and their interrelationships are defined in a CustomStructure which is a collection of CustomItems. Each CustomItem defines a key, giving it a cardinality and associating it with a RepresentedVariable and a ValueDomain. The set of CustomItems can be arranged into a structure, for example a hierarchy, by a CustomItemRelationStructure.
[image to be inserted]
Once the structure is defined, it can be used in a CustomInstance. This is a collection of CustomValues. A CustomValue contains a key-value pair where the key corresponds to a previously defined CustomItem. A CustomValue also may be associated with an InstanceVariable.  The CustomValues may also be structured by a CustomValueRelationStructure.
[image to be inserted]


A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Category/index.rst
   CategoryRelationStructure/index.rst
   CategorySet/index.rst
   Code/index.rst
   CodeList/index.rst
   Concept/index.rst
   ConceptRelationStructure/index.rst
   ConceptSystem/index.rst
   CustomInstance/index.rst
   CustomItem/index.rst
   CustomItemRelationStructure/index.rst
   CustomStructure/index.rst
   CustomValue/index.rst
   ExternalMaterial/index.rst
   InstanceVariable/index.rst
   LevelStructure/index.rst
   RepresentedVariable/index.rst
   SentinelConceptualDomain/index.rst
   SentinelValueDomain/index.rst
   SubstantiveConceptualDomain/index.rst
   SubstantiveValueDomain/index.rst
   UnitType/index.rst
   Universe/index.rst
   ValueAndConceptDescription/index.rst



Graph
=====

.. graphviz:: /images/graph/CustomMetadataView/CustomMetadataView.dot