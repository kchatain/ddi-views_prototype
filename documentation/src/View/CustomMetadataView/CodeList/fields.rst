.. _fields:


CodeList - Properties and Relationships
***************************************





Properties
==========

========  =============  ===========
Name      Type           Cardinality
========  =============  ===========
contains  CodeIndicator  0..n
========  =============  ===========


contains
########
Allows for the identification of the member and optionally provides an index for the member within an ordered array




Relationships
=============

==============  =====================  ===========  ================
Name            Type                   Cardinality  allways external
==============  =====================  ===========  ================
isStructuredBy  CodeRelationStructure  0..n           yes
realizes        StructuredCollection   0..n           yes
==============  =====================  ===========  ================


isStructuredBy
##############
A complex structure, hierarchical, for a CodeList. Restricted to single structure.




realizes
########
The Collection pattern class realized by this class



