.. _fields:


CustomItem - Properties and Relationships
*****************************************





Properties
==========

=========  =======  ===========
Name       Type     Cardinality
=========  =======  ===========
key        String   0..1
maxOccurs  Integer  0..1
minOccurs  Integer  0..1
=========  =======  ===========


key
###
The key to be used by a key,value pair


maxOccurs
#########
The maximum number of occurrences of an associated CustomValue


minOccurs
#########
The minimum number of occurrences of an associated CustomValue




Relationships
=============

==========  ===================  ===========  ================
Name        Type                 Cardinality  allways external
==========  ===================  ===========  ================
comparesTo  RepresentedVariable  0..n           no
hasConcept  Concept              0..n           no
realizes    CollectionMember     0..n           yes
uses        ValueDomain          0..n           no
==========  ===================  ===========  ================


comparesTo
##########
This optional relationship indicates a RepresentedVariable compatible with this key. Custom metadata might be reused as data and data might be used as metadata. This mechanism could enable these transformations.




hasConcept
##########
A Concept associated with the key of a CustomValue




realizes
########
Class can play the role of a Member in a Collection




uses
####
The type of value for the key.value pair



