.. _InstanceVariable:


InstanceVariable
****************


Extends
=======
:ref:`RepresentedVariable`


Definition
==========
The use of a Represented Variable within a Data Set. The InstanceVariable describes actual instances of data that have been collected.


Synonyms
========



Explanatory notes
=================
The InstanceVariable class inherits all of the properties and relationships of the RepresentedVariable (RV) class and, in turn, the ConceptualVariable (CV) class. This means that an InstanceVariable can be completely populated without the need to create an associated RepresentedVariable or ConceptualVariable. If, however, a user wishes to indicate that a particular InstanceVariable is patterned after a particular RepresentedVariable or a particular ConceptualVariable that may be indicated by including a relationship to the RV and or CV. Including these references is an important method of indicating that multiple InstanceVariables have the same representation, measure the same concept, and are drawn from the same Universe. If two InstanceVariables of a person's height reference the same RepresentedVariable. This indicates that they are intended to: be measured with the same unit of measurement, have the same intended data type, have the same SubstantativeValueDomain, use a SentinelValueDomain drawn from the same set of SentinelValueDomains, have the same sentinel (missing value) concepts, and draw their Population from the same Universe. In other words, the two InstanceVariables should be comparable.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst