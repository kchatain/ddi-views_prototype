.. _examples:


ExternalMaterial - Examples
***************************


ExternalMaterial is used to identify material and optionally specific sections of the material that have a relationship to a class. There is a generic relatedMaterial on AnnotatedIdentifiable. This should be used to attach any additional material that the user identifies as important to the class. The properties typeOfMaterial, descriptiveText, and relationshipDescription should be used to clarify the purpose and coverage of the related material.