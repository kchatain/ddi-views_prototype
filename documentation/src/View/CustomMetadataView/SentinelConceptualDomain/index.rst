.. _SentinelConceptualDomain:


SentinelConceptualDomain
************************


Extends
=======
:ref:`ConceptualDomain`


Definition
==========
Description or list of possible sentinel concepts , e.g. missing values.


Synonyms
========
missing categories


Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst