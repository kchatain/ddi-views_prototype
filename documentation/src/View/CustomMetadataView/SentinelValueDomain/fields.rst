.. _fields:


SentinelValueDomain - Properties and Relationships
**************************************************





Properties
==========

============  =================================  ===========
Name          Type                               Cardinality
============  =================================  ===========
platformType  ExternalControlledVocabularyEntry  0..1
============  =================================  ===========


platformType
############
The type of platform under which sentinel codes will be used. Statistical software platforms use different sets of codes to indicate missing values. The external controlled vocabulary should list platform tpes and a description of the allowed missing value types. A sample list would be: • BlankString - A Blank string indicates missing. Comparison is based on lexical order.  • EmptyString - An empty string indicates missing. Use in comparisons returns missing.   • Rstyle - Codes drawn from  NA and the IEEE 754 values of  NaN  -Inf   +Inf.   Comparisons return NA.   • SASNumeric - codes drawn from . ._ .A .B .C .D .E .F .G .H .I .J .K .L .M .N .O .P .Q .R .S .T .U .V .W .X .Y .Z    Sentinel code treated as less than any substantive value   • SPSSstyle - System missing (a dot) a set of individual values drawn from the same datatype as the SubstantiveValueDomain, and a range of values  drawn from the same datatype as the SubstantiveValueDomain.  Comparisons return system missing.  Some functions substitute with valid values (e.g. SUM replaces missing values with 0)   • StataNumeric - codes drawn from . ._ .A .B .C .D .E .F .G .H .I .J .K .L .M .N .O .P .Q .R .S .T .U .V .W .X .Y .Z  Sentinel code treated as greater than any substantive value  • Unrestricted - No restrictions on codes for sentinel values. Use in comparisons is indeterminate.  




Relationships
=============

=====================  ==========================  ===========  ================
Name                   Type                        Cardinality  allways external
=====================  ==========================  ===========  ================
describedValueDomain   ValueAndConceptDescription  0..n           no
enumeratedValueDomain  EnumerationDomain           0..n           no
takesConceptsFrom      SentinelConceptualDomain    0..n           no
=====================  ==========================  ===========  ================


describedValueDomain
####################
A formal description of the set of valid values - for described value domains.




enumeratedValueDomain
#####################
Any subtype of an Enumeration Domain enumerating the set of valid values.




takesConceptsFrom
#################
Corresponding conceptual definition given by a SentinelConceptualDomain.



