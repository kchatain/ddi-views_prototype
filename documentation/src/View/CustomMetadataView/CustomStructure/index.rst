.. _CustomStructure:


CustomStructure
***************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A Collection containing a set of item descriptions defining a structure for a set of key,value pairs


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst