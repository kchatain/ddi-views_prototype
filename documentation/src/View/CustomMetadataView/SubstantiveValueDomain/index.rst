.. _SubstantiveValueDomain:


SubstantiveValueDomain
**********************


Extends
=======
:ref:`ValueDomain`


Definition
==========
The Value Domain for a substantive conceptual domain.


Synonyms
========
value domain [ISO11179]


Explanatory notes
=================
In DDI4 the value domain for a variable is separated into “substantive” and “sentinel” values. Substantive values are the values of primary interest. Sentinel values are additional values that may carry supplementary information, such as reasons for missing. This duality is described in ISO 11404. Substantive values for height might be real numbers expressed in meters. The full value domain might also include codes for different kinds of missing values - one code for “refused” and another for “don’t know”. Sentinel Values may also convey some substantive information and at the same time represent missing values. An example might be where a numeric variable like number of employees be sometimes a count and sometimes a code representing a range of counts in order to avoid disclosure of information about a specific entity. The SubstantiveValueDomain may use either a ValueDescription, for described values, or a CodeList for enumerated values, or both. A value domain may consist of substantive values or sentinel values. Substantive values are those associated directly with some subject matter area. They do not address concerns around processing, such as missing values. Substantive values are called "regular values" in ISO/IEC 11404 - General purpose datatypes. The enumerated case is one where all values are listed. An example is the categorical values for gender: The conceptual domain could consist of the concept of male and the concept of female. These might be represented in codes and associated labels as 1 (="Male") 2 (="Female") The described case is one where some description is needed to define the set of values. Take the following description for height in meters: "a real number between 0 and 3, represented to two Arabic decimal places". This description might be structured in some way to be machine actionable (datatype="double", min=0, max=3, decimals=2).


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst