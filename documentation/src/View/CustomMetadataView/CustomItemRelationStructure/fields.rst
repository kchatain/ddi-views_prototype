.. _fields:


CustomItemRelationStructure - Properties and Relationships
**********************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
criteria                  InternationalStructuredString      0..1
displayLabel              LabelForDisplay                    0..n
hasMemberRelation         CustomItemRelation                 0..n
hasRelationSpecification  RelationSpecification              0..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
usage                     InternationalStructuredString      0..1
========================  =================================  ===========


criteria
########
Intentional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


displayLabel
############
A display label for the RelationStructure. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


hasMemberRelation
#################
Member relationships that comprise the relationship structure


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification. .


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

========  =================  ===========  ================
Name      Type               Cardinality  allways external
========  =================  ===========  ================
realizes  RelationStructure  0..n           yes
========  =================  ===========  ================


realizes
########
Collection pattern class realized by this class



