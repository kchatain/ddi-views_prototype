.. _BoundingBox:


BoundingBox
***********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A type of Spatial coverage describing a rectangular area within which the actual range of location fits. A BoundingBox is described by 4 numbers - the maxima of the north, south, east, and west coordinates found in the area.


Synonyms
========
r:BoundingBox


Explanatory notes
=================
A BoundingBox is often described by two x,y coordinates where the x coordinates are used for the North and South Latitudes and y coordinates for the West and East Longitudes


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst