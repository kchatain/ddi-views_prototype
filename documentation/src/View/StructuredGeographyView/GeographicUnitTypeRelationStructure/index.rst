.. _GeographicUnitTypeRelationStructure:


GeographicUnitTypeRelationStructure
***********************************


Extends
=======
:ref:`Identifiable`


Definition
==========
Defines the relationships between Geographic Unit Types in a collection.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst