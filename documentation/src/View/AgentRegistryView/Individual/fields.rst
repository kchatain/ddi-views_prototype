.. _fields:


Individual - Properties and Relationships
*****************************************





Properties
==========

=====================  ==================  ===========
Name                   Type                Cardinality
=====================  ==================  ===========
ddiId                  String              0..n
hasContactInformation  ContactInformation  0..1
hasIndividualName      IndividualName      0..n
=====================  ==================  ===========


ddiId
#####
The agency identifier of the individual according to the DDI Alliance agent registry.


hasContactInformation
#####################
Contact information for the individual including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.


hasIndividualName
#################
The name of an individual broken out into its component parts of prefix, first/given name, middle name, last/family/surname, and suffix.

