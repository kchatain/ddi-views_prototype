.. _AgentRelationStructure:


AgentRelationStructure
**********************


Extends
=======
:ref:`Identifiable`


Definition
==========
Defines the relationships between Agents in a collection. Clarifies valid period and the purpose the relationship serve


Synonyms
========



Explanatory notes
=================
Describes relations between agents not roles within a project or in relationship to a product. Roles are defined by the parent class and relationship name that uses an Agent as a target.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst