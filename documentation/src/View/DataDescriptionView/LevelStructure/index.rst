.. _LevelStructure:


LevelStructure
**************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The LevelStructure describes the nesting structure of a hierarchical collection. The levels within the structure begin at the root level '1' and continue as an ordered array through each level of nesting.


Synonyms
========



Explanatory notes
=================
Levels are used to organize a hierarchy. Usually, a hierarchy often contains one root member at the top, though it could contain several. These are the first Level. All members directly related to those in the first Level compose the second Level. The third and subsequent Levels are defined similarly. A Level often is associated with a Concept, which defines it. These correspond to kinds of aggregates. For example, in the US Standard Occupational Classification (2010), the Level below the top is called Major Occupation Groups, and the next Level is called Minor Occupational Groups. These ideas convey the structure. In particular, Health Care Practitioners (a Major Group) can be broken into Chiropractors, Dentists, Physicians, Vets, Therapists, etc. (Minor Groups) The Categories in the Nodes at the lower Level aggregate to the Category in Node above them. "Classification schemes are frequently organized in nested levels of increasing detail. ISCO-08, for example, has four levels: at the top level are ten major groups, each of which contain sub-major groups, which in turn are subdivided in minor groups, which contain unit groups. Even when a classification is not structured in levels ("flat classification"), the usual convention, which is adopted here, is to consider that it contains one unique level." (http://rdf-vocabulary.ddialliance.org/xkos.html#) Individual classification items organized in a hierarchy may be associated with a specific level.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst