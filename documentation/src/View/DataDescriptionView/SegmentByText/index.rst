.. _SegmentByText:


SegmentByText
*************


Extends
=======
:ref:`PhysicalSegmentLocation`


Definition
==========
Defines the location of a segment of text through character, and optionally line, counts. An adequate description will always include a startCharacterPosition and then may include an endCharacterPosition or a characterLength. If StartLine is specified, the character counts begin within that line. An endCharacterPosition of 0 indicates that whole lines are specified


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst