.. _fields:


UnitDataViewpoint - Properties and Relationships
************************************************





Relationships
=============

=================  ==============  ===========  ================
Name               Type            Cardinality  allways external
=================  ==============  ===========  ================
hasAttributeRole   AttributeRole   0..1           no
hasIdentifierRole  IdentifierRole  0..1           no
hasMeasureRole     MeasureRole     0..1           no
=================  ==============  ===========  ================


hasAttributeRole
################
The InstanceVaribles functioning as attributes




hasIdentifierRole
#################
The InstanceVaribles functioning as identifiers




hasMeasureRole
##############
The InstanceVaribles functioning as measures



