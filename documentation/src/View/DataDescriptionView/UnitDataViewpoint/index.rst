.. _UnitDataViewpoint:


UnitDataViewpoint
*****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The assignment of measure, identifier and attribute roles to InstanceVariables Each of three roles within a UnitDataViewpoint is a SimpleCollection of instance variables.


Synonyms
========



Explanatory notes
=================
Viewpoint is a UnitDataViewpoint to underline that the composition of a UnitDataRecord viewpoint is different from the composition of DataCube viewpoint. The DataCube viewpoint is out of scope currently. It is sometimes useful to describe a set of variables and their roles within the set. In the blood pressure example above, values for a set of variables are captured in one setting - the taking of a blood pressure. Some of the variables may be the measures of interest, for example the systolic and diastolic pressure. Other variables may take on the role of identifiers, such as the hospital’s patient ID and a government ID such as the U.S. Social Security Number. A third subgroup of variables may serve as attributes. The age of the patient, whether she is sitting or standing, the time of day. The assignment of these three roles, identifier, measure, and attribute, is the function of the Viewpoint. Viewpoints are not fixed attributes of variables. This is why there is a separate Viewpoint class which maps to InstanceVariables. In the example above, the Viewpoint is within the context of the original measurement. In a reanalysis of the data the roles might change. Time of day might be the measure of interest in a study of hospital practices.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst