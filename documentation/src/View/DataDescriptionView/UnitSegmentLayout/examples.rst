.. _examples:


UnitSegmentLayout - Examples
****************************


A simple spreadsheet. Commonly the first row of the table will contain variable names or descriptions. The following csv file has a rectangular layout and would import into a simple table in a spreadsheet: PersonId,AgeYr,HeightCm 1,22,183 2,45,175