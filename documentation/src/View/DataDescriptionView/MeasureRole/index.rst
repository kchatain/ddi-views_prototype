.. _MeasureRole:


MeasureRole
***********


Extends
=======
:ref:`ViewpointRole`


Definition
==========
A MeasureRole identifies one or more InstanceVariables as being measures within a ViewPoint. A MeasureRole is a SimpleCollection of InstanceVariables acting in the MeasureRole.


Synonyms
========



Explanatory notes
=================
See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst