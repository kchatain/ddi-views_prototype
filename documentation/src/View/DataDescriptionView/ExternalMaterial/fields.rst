.. _fields:


ExternalMaterial - Properties and Relationships
***********************************************





Properties
==========

==========================  =================================  ===========
Name                        Type                               Cardinality
==========================  =================================  ===========
citationOfExternalMaterial  Annotation                         0..1
descriptiveText             InternationalStructuredString      0..1
mimeType                    ExternalControlledVocabularyEntry  0..1
relationshipDescription     InternationalStructuredString      0..n
typeOfMaterial              ExternalControlledVocabularyEntry  0..1
uri                         anyURI                             0..n
usesSegment                 Segment                            0..n
==========================  =================================  ===========


citationOfExternalMaterial
##########################
Bibliographic citation for the external resource.


descriptiveText
###############
A description of the referenced material. This field can map to a Dublin Core abstract. Note that Dublin Core does not support structure within the abstract element. Supports multiple languages and optional structured content.


mimeType
########
Provides a standard Internet MIME type for use by processing applications.


relationshipDescription
#######################
Describes the reason the external material is being related to the DDI metadata instance.


typeOfMaterial
##############
Designation of the type of material being described. Supports the use of a controlled vocabulary.


uri
###
URI for the location of the material


usesSegment
###########
Can describe a segment within a larger object such as a text or video segment.

