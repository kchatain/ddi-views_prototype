.. _LogicalRecordRelationStructure:


LogicalRecordRelationStructure
******************************


Extends
=======
:ref:`Identifiable`


Definition
==========
Allows for the complex structuring of relationships between LogicalRecords in a DataStore


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst