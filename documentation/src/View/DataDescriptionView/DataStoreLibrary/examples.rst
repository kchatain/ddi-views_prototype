.. _examples:


DataStoreLibrary - Examples
***************************


A StudySeries is a time series and a DataStoreRelationStructure is dedicated to tracking "supplements". There is a study at T0. Perhaps it is never executed. It is the "core" study. At T1 there is a study that includes the core and Supplement A. At T2 there is a study that includes the core, Supplement A and Supplement B. At T3 there is a study that includes the core and a one off supplement that is never asked again. T4 though T6 repeats T1 through T3.