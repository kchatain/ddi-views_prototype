.. _DataPointRelationStructure:


DataPointRelationStructure
**************************


Extends
=======
:ref:`Identifiable`


Definition
==========
A means for describing the complex relational structure of Data Points in a Logical Record


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst