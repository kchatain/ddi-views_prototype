.. _examples:


RecordRelation - Examples
*************************


One LogicalRecord containing a PersonIdentifier and a PersonName and another LogicalRecord containing a MeasurementID, a PersonID, a SystolicPressure, and a DiastolicPressure could be linked by a RecordRelation. The RecordRelation could employ an InstanceVariableValueMap to describe the linkage between PersonIdentifier and PersonID.