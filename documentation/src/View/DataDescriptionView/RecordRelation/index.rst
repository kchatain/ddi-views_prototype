.. _RecordRelation:


RecordRelation
**************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The RecordRelation object is used to indicate relationships among record types within and between LogicalRecords. For InstanceVariables existing in a LogicalRecord with multiple record layouts, pairs of InstanceVariables may function as paired keys to permit the expression of hierarchical links between records of different types. These links between keys in different record types could also be used to link records in a relational structure.


Synonyms
========



Explanatory notes
=================
A household-level LogicalRecord might contain an InstanceVariable called HouseholdID and a person-level LogicalRecord might contain an InstanceVariable called HID. Describing a link between HouseholdID and HID would allow the linking of a person-level LogicalRecord to their corresponding household-level LogicalRecord.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst