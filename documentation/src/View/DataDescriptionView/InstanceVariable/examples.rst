.. _examples:


InstanceVariable - Examples
***************************


1) Gender: Dan Gillman has gender <m, male>, Arofan Gregory has gender<m, male>, etc. 2) Number of employees: Microsoft has 90,000 employees; IBM has 433,000 employees, etc. 3) Endowment: Johns Hopkins has endowment of <3, $1,000,000 and above>, Yale has endowment of <3, $1,000,000 and above>, etc. Two InstanceVariables of a person's height reference the same RepresentedVariable. This indicates that they are intended to: be measured with the same unit of measurement, have the same intended data type, have the same SubstantativeValueDomain, use a SentinelValueDomain drawn from the same set of SentinelValueDomains, have the same sentinel (missing value) concepts, and draw their Population from the same Universe. In other words, the two InstanceVariables should be comparable.