.. _PhysicalRecordSegment:


PhysicalRecordSegment
*********************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A description of each physical storage segment required to completely cover the logical record. A logical record may be stored in one or more segments housed hierarchically in a single file or in separate data files. All logical records have at least one segment.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst