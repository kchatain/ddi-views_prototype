*******************
DataDescriptionView
*******************
Purpose
Use the DataDescriptionView to describe a collection of LogicalRecords by themselves or along with their PhysicalRecordSegments in a Study and/or a StudySeries.

Recall that a LogicalRecord is a record definition and a PhysicalRecordSegment describes the physical storage arrangement of a LogicalRecord.

Think of the collection of LogicalRecords and their PhysicalRecordSegment counterparts in a Study as a succession of states that are the products of one or more data transforming processes. In the DataDescriptionView we just describe products in this succession, not the processes through which these products are created.

In this succession operational data may be transformed into analysis-ready data and then analytical data in a series of steps. The DataDescriptionView captures the products from one, some or all of these interim steps. 

Products can include “skinny” and “fat” data structures; relational datastores; star schemas (data warehouses); structured collections of instance variables that take the form of objects, not tables; data cubes (not supported in the prototype) and so forth.

Instead of using the DataDescriptionView, use the DataManagementView for creating data descriptions that are both process-centered and product-centered. 


Use Cases
Construct a catalog of data records produced across the life of a study. Order the records by category: initial or submission records, interim or archival records, analysis or dissemination records
Expose variables in LogicalRecords and their PhysicalRecordSegments so they are searchable by: 
study, 
structural role(s) (identifier, measure and attribute) in one or more viewpoints and 
their functional role(s) as specified in one or more external controlled vocabularies
Specify the information needed for a Submission Information Package (SIP), an Archival Information Package (AIP) or a Dissemination Information Package (DIP) when the data is a CSV file
Support the description of a CSV file that uses a list datatype to, for example, report health measures that consist of compound elements like blood pressure
Support the description of medical information as specified both by Fast Healthcare Interoperability Resources (FHIR) and the Clinical Information Modeling openEHR Initiative (CIMI). With respect to FHIR, support, for example, the Observation and DiagnosticReport resources. With respect to CIMI, support, for example, the various observation archetypes including the newborn Apgar score, blood pressure, body mass index, physical examination findings, etc.
Support the description of a NoSQL key/value store where the key is either a simple collection of one or more identifiers or a structured (hierarchical) collection and the value can be text, video, sound or a JSON document

Target Audiences
Digital archivists
Search engines intent on exposing data lineage within a study
Researchers who are preparing a Data Management Plan (DMP)
Governments intent on exposing in a single datastore registries that include demographic data and health information in order to promote various types of research

Restricted Classes
Code is extended by ClassificationItem.
CodeList is extended by GeographicUnitClassification, GeographicUnitTypeClassification and StatisticalClassification.
Concept is extended by ConceptualVariable, RepresentedVariable, UnitType and Universe.
ExternalMaterial is extended by ExternalAid.







A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AttributeRole/index.rst
   Category/index.rst
   CategoryRelationStructure/index.rst
   CategorySet/index.rst
   Code/index.rst
   CodeList/index.rst
   CodeRelationStructure/index.rst
   Concept/index.rst
   ConceptRelationStructure/index.rst
   ConceptSystem/index.rst
   DataPoint/index.rst
   DataPointRelationStructure/index.rst
   DataStore/index.rst
   DataStoreLibrary/index.rst
   DataStoreRelationStructure/index.rst
   Datum/index.rst
   ExternalMaterial/index.rst
   IdentifierRole/index.rst
   InstanceVariable/index.rst
   InstanceVariableRelationStructure/index.rst
   LevelStructure/index.rst
   LogicalRecordRelationStructure/index.rst
   MeasureRole/index.rst
   PhysicalDataSet/index.rst
   PhysicalLayoutRelationStructure/index.rst
   PhysicalOrderRelationStructure/index.rst
   PhysicalRecordSegment/index.rst
   Population/index.rst
   RecordRelation/index.rst
   SegmentByText/index.rst
   SentinelConceptualDomain/index.rst
   SentinelValueDomain/index.rst
   SubstantiveConceptualDomain/index.rst
   SubstantiveValueDomain/index.rst
   UnitDataRecord/index.rst
   UnitDataViewpoint/index.rst
   UnitSegmentLayout/index.rst
   ValueAndConceptDescription/index.rst
   ValueMapping/index.rst



Graph
=====

.. graphviz:: /images/graph/DataDescriptionView/DataDescriptionView.dot