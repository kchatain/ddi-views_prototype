.. _ControlledVocabulary:


ControlledVocabulary
********************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The specification of a controlled vocabulary defines a set of values and their definitions together with the order relationships among those entries.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst