.. _VocabularyEntry:


VocabularyEntry
***************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
One entry term and its definition in an ordered list comprising a controlled vocabulary.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst