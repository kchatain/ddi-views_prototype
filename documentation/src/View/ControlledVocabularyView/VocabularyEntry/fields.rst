.. _fields:


VocabularyEntry - Properties and Relationships
**********************************************





Properties
==========

==========  =============================  ===========
Name        Type                           Cardinality
==========  =============================  ===========
definition  InternationalStructuredString  0..1
entryTerm   InternationalStructuredString  0..1
==========  =============================  ===========


definition
##########
An explanation (definition) of the value


entryTerm
#########
the term being defined




Relationships
=============

========  ================  ===========  ================
Name      Type              Cardinality  allways external
========  ================  ===========  ================
realizes  CollectionMember  0..n           yes
========  ================  ===========  ================


realizes
########
Class can play the role of a Member in a Collection



