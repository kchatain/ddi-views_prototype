Production Framework
=====================

The model is being developed in Drupal at http://lion.ddialliance.org

Documentation Flow
-------------------

**Figure 1. Documentation Flow in the Production Process**

.. |figure1| image:: ../images/documentation_flow.png

|figure1|

Bindings Production Flow in the Production Framework
-----------------------------------------------------

**Figure 2. Bindings Flow in the Production Process**

.. |figure2| image:: ../images/pim_psm_binding.png

|figure2|

The XMI for a portion of the model (as configured in Drupal) is exported as XMI for the platform-independent model (PIM). This is then transformed again, for each binding type (XML or RDF) to be produced, creating the platform-specific model (PSM) specific to that binding. This transformation from PIM to a specific PSM is informed by any needed configuration information. The PSM is optimized for the expressive capabilities of the binding to be produced (RDF and XML have different expressive capabilities). A transformation is then run on each PSM to produce the desired bindings. Each view will be expressed as an XML document type and as an RDF Vocabulary expressed in OWL. There will also be an XML and OWL binding for the library as a whole.

XML Bindings
=============

`XML Binding from the DDI Model <https://bitbucket.org/ddi-alliance/ddi-views-production/src/master/transform/XML%20Binding%20from%20the%20DDI%20Model.docx>`_ presents the mapping rules from the XML export from Drupal to the XML schema deliverables. This process is split into two steps. First an intermediate reduction is produced as a second XMI file that focuses on what is the best implementation of the platform independent model in XSD. Second the actual XML schemas are created.

Documentation Flow
===================
