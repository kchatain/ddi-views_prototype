*********************
Plugging into the Model
*********************
At the center of the DDI 4 model is the Concept. There are many specialized forms of Concepts, collections and comparisons of concepts, and development of the Data Item (a concept linked to a unit type with specified representation. Much of DDI focuses on how these core classes are developed or captured, organized, and managed. These activities are reflected in patterns; Methodology Pattern focusing on development and planning, Process Pattern focusing on processes used to capture and transform core classes, and Collections Pattern focusing on how core classes are grouped into collections, ordered internally, compared, and managed over time. The following simplified structures show how various regions of the model plug into the core pattern and uses it as a means of traversing from one region to another. The purpose of this document is to provide a high level view of how regions of the model interact.

.. image:: ../images/plugging_into_model_concept_diagram.png

The left side shows the specialization of Unit Types as it narrows down to a specific Unit of a Unit Type. The right side shows the specialization of a Concept to a Category, a collection of which is used to describe a Concept Domain which is then specialized to associate specific values with those categories. In the center is the Variable Cascade, the DDI version of ISO/IEC 11179 Data Item, shows that it reflects the specialization occurring in the other columns with Conceptual Variable linking a Concept more explicitly described by a Concept Domain and applied to a Unit Type. Represented Variable further specified the Concept by associating specific Values and linking it to the narrowed Universe. Instance Variable further narrows the Universe of the Represented Variable to a Population. The Data Point is defined by the Instance Variable and related to a specific Unit. The Datum is the value that occupies the Data Point for a specific Unit.

.. image:: ../images/plugging_into_model_managing_systems.png

Geographic Systems organize and manage specialization of Unit Types and Geographic Units using Collections Patterns and specialized descriptive structure Level as used in Statistical Classification. This provides a formal means of describing these highly structured and managed systems. These are structures that are published and used across organizations. Examples would be the structure of Geographic Unit Types used by the United States Census Bureau, the International Standard Industrial Classification (ISIC).

.. image:: ../images/plugging_into_model_data_processing.png

Data processing uses the Process Pattern and generic or specialized Workflows using Datum for both input and output of the process. The results may be individual datum or sets organized into records or data sets.
 
.. image:: ../images/plugging_into_model_data_capture.png

Data Capture may be widely viewed as various means of capturing specific Unit level Datum associated with an Instance Variable. They are generally captured through some specialized us of Workflows (Process Pattern) through transformation of input data, measurement systems (scales, thermometers, etc.) or questions. Represented Questions (Measures) are instantiated in an Instance Question (Measure), organized within a Conceptual Instrument (a specialized Workflow Process), and administered to a sample of a population using an Implemented Instrument. 

.. image:: ../images/plugging_into_model_logical_org.png

Describing the logical organization of Instance Variables and physical organization of data is done by applying the Collections Pattern to the lower portion of the core classes. A Logical Record defines a simple collection of Instance Variables clarifying the set of Instance Variables related to a single Unit in the Population of the Data Store. The Data Store describes the set of Logical Records and how they relate to each other (for example a Data Store of Household, Student, and School records). When physically stored the Physical Data Set organizes Physical Segment Layouts related to the Logical Records, and defines the specific Data Point relations within the Physical Segment Layout using Value Mapping.  

.. image:: ../images/plugging_into_model_sampling.png

Sampling is an example of the realization of the Methodology Pattern and Process Pattern which interacts primarily the left hand column of the core classes. Sampling is a process based on specific methodologies and is used to identify specific Units from a defined Population to include in a sample from which unit level data will be captured.



