************
Relationships
************
When creating a relationship to a target class it is assumed that the target may be represented by the class noted in the UML OR any of its subclasses. 

.. image:: ../images/relationships_agent.png

In this case Agent is an abstract class with 3 subtypes: Individual, Machine, and Organization. In the XML Binding this would be represented by a:

.. code-block:: xml

         <xs:element name="Source" minOccurs="1" maxOccurs="1">
            <xs:annotation>
               <xs:documentation/>
            </xs:annotation>
            <xs:complexType>
               <xs:complexContent>
                  <xs:extension base="ReferenceType">
                     <xs:attribute name="typeOfClass" use="required">
                        <xs:simpleType>
                           <xs:restriction base="xs:NMTOKEN">
                              <xs:enumeration value="Individual"/>
                              <xs:enumeration value="Machine"/>
                              <xs:enumeration value="Organization"/>
                           </xs:restriction>
                        </xs:simpleType>
                     </xs:attribute>
                  </xs:extension>
               </xs:complexContent>
            </xs:complexType>
         </xs:element>

The same is true for instantiated classes that have subtypes. For example in locations where multiple Methodologies may be an appropriate target, the target class is MethodologyOverview which allows for simple descriptive information on the Methodology. Optionally any available subtype may serve as the target.

.. image:: ../images/relationships_methodologyoverview.png

The would be represented in the XML binding as follows:

.. code-block:: xml

		<xs:element name="ComponentMethodology" minOccurs="0" maxOccurs="unbounded">
			<xs:annotation> 
				<xs:documentation/> 
			</xs:annotation> 
			<xs:complexType> 
				<xs:complexContent> 
					<xs:extension base="ReferenceType"> 
						<xs:attribute name="typeOfClass" use="required"> 
							<xs:simpleType> 
								<xs:restriction base="xs:NMTOKEN"> 
									<xs:enumeration value="MethodologyOverview"/> 
									<xs:enumeration value="SamplingProcedure"/> 
								</xs:restriction> 
							</xs:simpleType> 
						</xs:attribute> 
					</xs:extension> 
				</xs:complexContent> 
			</xs:complexType> 
		</xs:element>

In most cases relationships will be to a specific class and contain only a single enumeration in the typeOfClass. 
