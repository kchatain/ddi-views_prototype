*********
Preface
*********

The intent of the DDI-Views documentation is to provide information on the individual classes used in the model; their relationship to each other, and their relationship to DDI Lifecycle
3.2 and other standards such as the General Statistical Information Model.

.. toctree::
   :maxdepth: 3

   development.rst
   funding.rst
   furtherinfo.rst
   organization.rst
   contactus.rst
   acknowledgements.rst
   
