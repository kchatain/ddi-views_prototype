**************
SimpleCodebook
**************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   StandardWeight/index.rst
   VariableStatistics/index.rst



Graph
=====

.. graphviz:: /images/graph/SimpleCodebook/SimpleCodebook.dot