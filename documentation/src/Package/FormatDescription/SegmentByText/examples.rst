.. _examples:


SegmentByText - Examples
************************


The segment beginning at line 3, character 4 and ending at line 27 character 13. Alternatively the segment beginning at character 257 and ending at character 1350 of the whole body of text. StartLine of 10, endLine of 12, startCharacterPosition of 1, endCharacterPosition of 0 means all of lines 10,11, and 12.