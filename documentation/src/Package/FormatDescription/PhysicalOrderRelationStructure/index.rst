.. _PhysicalOrderRelationStructure:


PhysicalOrderRelationStructure
******************************


Extends
=======
:ref:`Identifiable`


Definition
==========
PhysicalStructureOrder orders thePhysicalRecordSegments which map to a LogicalRecord.


Synonyms
========



Explanatory notes
=================
The same LogicalRecordLayout may be the sourceMember in several adjacency lists. This can happen when PhysicalRecordSegments are also population specific. In this instance each adjacency list associated with a LogicalRecordLayout is associated with a different population.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst