.. _PhysicalSegmentLayout:


PhysicalSegmentLayout
*********************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The PhysicalSegmentLayout is an abstract class used as an extension point in the description of the different layout styles of data structure description.


Synonyms
========



Explanatory notes
=================
A PhysicalLayout is a physical description (e.g. UnitSegmentLayout) of the associated Logical Record Layout consisting of a Collection of Value Mappings describing the physical interrelationship of each related ValueMapping and associated Instance Variable.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst