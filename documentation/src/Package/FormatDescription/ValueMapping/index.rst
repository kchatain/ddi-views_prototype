.. _ValueMapping:


ValueMapping
************


Extends
=======
:ref:`Identifiable`


Definition
==========
Provides physical characteristics for an InstanceVariable as part of a PhysicalSegmentLayout


Synonyms
========



Explanatory notes
=================
An InstanceVariable has details of value domain and datatype, but will not have the final details of how a value is physically represented in a data file. A variable for height, for example, may be represented as a real number, but may be represented as a string in multiple ways. The decimal separator might be, for example a period or a comma. The string representing the value of a payment might be preceded by a currency symbol. The same numeric value might be written as “1,234,567” or “1.234567 E6”. A missing value might be written as “.”, “NA”, “.R” or as “R”. The ValueMapping describes how the value of an InstanceVariable is physically expressed. The properties of the ValueMapping as intended to be compatible with the W3C Metadata Vocabulary for Tabular Data (https://www.w3.org/TR/tabular-metadata/ ) as well as common programming languages and statistical packages. The “format” property, for example can draw from an external controlled vocabulary such as the set of formats for Stata, SPSS, or SAS.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst