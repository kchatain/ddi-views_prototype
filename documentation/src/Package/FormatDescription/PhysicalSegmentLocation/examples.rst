.. _examples:


PhysicalSegmentLocation - Examples
**********************************


A segment of text in a plain text file beginning at character 3 and ending at character 123.