.. _PhysicalSegmentLocation:


PhysicalSegmentLocation
***********************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Among other things defines the location of a DataPoint in a PhysicalSegment (e.g. the location of the representation of a variable in a text file). This is abstract since there are many different ways to describe the location of a segment - character counts, start and end times, etc.


Synonyms
========



Explanatory notes
=================
While this has no properties or relationships of its own, it is useful as a target of relationships where its extensions may serve.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst