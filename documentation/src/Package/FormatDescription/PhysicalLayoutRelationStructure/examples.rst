.. _examples:


PhysicalLayoutRelationStructure - Examples
******************************************


The W3C Tabular Data on the Web specification allows for a list datatype. In the example below there are three top level InstanceVariables PersonID – the person identifier AgeYr – age in year BpSys_Dia – blood pressure (a list containing Systolic and Diastolic values) There are two variables at a secondary level of the hierarchy Systolic – the systolic pressure Diastolic – the diastolic pressure The delimited file below uses the comma to separate "columns" and forward slash to separate elements of a blood pressure list. PersonID, AgeYr, BpSys_Dia 1,22,119/67 2,68,122/70 The PhysicalRelationStructure in this case would describe a BpSys_Dia list variable as containing an ordered sequence of the Systolic and Diastolic InstanceVariables.