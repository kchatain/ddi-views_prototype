.. _PhysicalDataSet:


PhysicalDataSet
***************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The information needed for understanding the physical structure of data coming from a file or other source. A StructureDescription also points to the Data Store it physically represents.


Synonyms
========



Explanatory notes
=================
Multiple styles of structural description are supported: including describing files as unit-record (UnitSegmentLayout) files; describing cubes; and describing event-history (spell) data.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst