.. _BusinessAlgorithm:


BusinessAlgorithm
*****************


Extends
=======
:ref:`AlgorithmOverview`


Definition
==========
A Business Algorithm is used to express the generalized function of the Business Process. The underlying properties of the algorithm or method rather than the specifics of any particular implementation. In short a description of the method in its simplest and most general representation.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst