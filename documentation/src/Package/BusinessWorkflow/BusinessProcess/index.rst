.. _BusinessProcess:


BusinessProcess
***************


Extends
=======
:ref:`WorkflowProcess`


Definition
==========
BusinessProcesses could be Generic Longitudinal Business Process Model (GLBPM) and/or Generic Statistical Business Process Model (GSBPM) steps and/or sub-steps. BusinessProcesses participate in a DataPipeline by virtue of their preconditions and postconditions. A BusinessProcess differs from a WorkflowStep by virtue of its granularity. Higher level operations are the subject of BusinessProcesses. A BusinessProcess has a WorkflowStepSequence through which these higher levels operations may optionally be decomposed into WorkflowSteps. BusinessProcess preconditions and postconditions differ from input and output parameters because they don't take variables and values. Instead the subject of preconditions and postconditions are typically whole datasets. Given this level of granularity it is possible to view a BusinessProcess through the Open Archival Information System (OAIS) Reference Model. So a BusinessProcess may optionally be described as a component of a Submission Information Package (SIP), an Archival Information Package (AIP) or a Dissemination Information Package (DIP).


Synonyms
========
Task


Explanatory notes
=================
One or more PairedExternalControlledVocabularyEntry objects may be used to identify and characterize controlled vocabularies like the GLBPM, GSBPM and OAIS. The PairedExternalControlledVocabularyEntry both identifies both the model (term) and the step/sub-step of the model (extent). If a BusinessProcess combines two steps from the same model (GLBPM, GSBPM, OAIS, etc.) or from different models, multiple PairedExternalControlledVocabularyEntry objects can be specified.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst