.. _QualityStatement:


QualityStatement
****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A statement of quality which may be related to an external standard or contain a simple overview, rationale, and usage statement. When relating to an external standard information on compliance may be added via the ComplianceStatement referenced by the Standard class.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst