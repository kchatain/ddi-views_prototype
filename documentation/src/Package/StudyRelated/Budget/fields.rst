.. _fields:


Budget - Properties and Relationships
*************************************





Properties
==========

========  =============================  ===========
Name      Type                           Cardinality
========  =============================  ===========
name      ObjectName                     0..n
overview  InternationalStructuredString  0..1
========  =============================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text. 




Relationships
=============

==============  ================  ===========
Name            Type              Cardinality
==============  ================  ===========
budgetDocument  ExternalMaterial  0..n
==============  ================  ===========


budgetDocument
##############
References to one or more external budget documents.



