.. _StudyControl:


StudyControl
************


Extends
=======
:ref:`Act`


Definition
==========
StudyControl references a Study. StudyControl enables the representation of a research protocol at the Study level. [If the only purpose of the object is to carry a Study, it should be seriously considered for melting with another object. Modeling rules state that any object needs to have a reality of its own. Carrying another object is not a reality. If I misunderstood, please clarify the description instead]


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst