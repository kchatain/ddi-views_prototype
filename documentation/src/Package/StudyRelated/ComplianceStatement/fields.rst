.. _fields:


ComplianceStatement - Properties and Relationships
**************************************************





Properties
==========

======================  =================================  ===========
Name                    Type                               Cardinality
======================  =================================  ===========
externalComplianceCode  ExternalControlledVocabularyEntry  0..1
usage                   InternationalStructuredString      0..1
======================  =================================  ===========


externalComplianceCode
######################
Specification of a code which relates to an area of coverage of the standard. Supports the use of an external controlled vocabulary.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

===============  =======  ===========
Name             Type     Cardinality
===============  =======  ===========
definingConcept  Concept  0..n
===============  =======  ===========


definingConcept
###############
A reference to a concept which relates to an area of coverage of the standard.



