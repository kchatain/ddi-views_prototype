.. _fields:


Study - Properties and Relationships
************************************





Properties
==========

=====================  =================================  ===========
Name                   Type                               Cardinality
=====================  =================================  ===========
bibliographicCitation  InternationalStructuredString      0..1
kindOfData             ExternalControlledVocabularyEntry  0..n
overview               InternationalStructuredString      0..1
=====================  =================================  ===========


bibliographicCitation
#####################
Complete bibliographic reference containing all of the standard elements of a citation that can be used to cite the work. The "format" attribute is provided to enable specification of the particular citation style used, e.g., APA, MLA, Chicago, etc.


kindOfData
##########
Describes, with a string or a term from a controlled vocabulary, the kind of data documented in the logical product(s) of a study unit. Examples include survey data, census/enumeration data, administrative data, measurement data, assessment data, demographic data, voting data, etc.


overview
########
A general descriptive overview of the Study




Relationships
=============

======================  =====================  ===========
Name                    Type                   Cardinality
======================  =====================  ===========
accessInformation       Access                 0..n
hasAgentListing         AgentListing           0..n
hasAlgorithm            AlgorithmOverview      0..n
hasAnalysisUnit         UnitType               0..n
hasAuthorizationSource  AuthorizationSource    0..n
hasBudget               Budget                 0..n
hasConcept              Concept                0..n
hasCoverage             Coverage               0..n
hasDesign               DesignOverview         0..n
hasEmbargo              Embargo                0..n
hasExPostEvaluation     ExPostEvaluation       0..1
hasFundingInformation   FundingInformation     0..n
hasInstanceVariable     InstanceVariable       0..n
hasInstrument           ImplementedInstrument  0..n
hasMethodology          MethodologyOverview    0..n
hasPopulation           Population             0..n
hasProcess              ProcessOverview        0..n
hasQualityStatement     QualityStatement       0..n
hasSamplingProcedure    SamplingProcedure      0..n
hasUniverse             Universe               0..n
hasVariableCollection   VariableCollection     0..n
partOfSeries            StudySeries            0..n
realizes                CollectionMember       0..n
======================  =====================  ===========


accessInformation
#################
Information relating to the availability of data from the study.




hasAgentListing
###############
Makes use of the Agent Listing to define relationships between agents.




hasAlgorithm
############
A relationship to a description of a simple overview, in which case the AlgorithmOverView class should be the target. 




hasAnalysisUnit
###############
Unit(s) of analysis for the study, e.g. persons, households.




hasAuthorizationSource
######################
The authorizing agency and allows for the full text of the authorization (law, regulation, or other form of authorization).




hasBudget
#########
A description of the budget for the Study




hasConcept
##########
A Study may relate to multiple concepts. These may also be denoted by keywords in an Annotation.




hasCoverage
###########
Spatial, Temporal, and Topical coverage information for the Study




hasDesign
#########
The design for the Study




hasEmbargo
##########
Information about data that are not currently available because of policies established by the principal investigators and/or data producers.




hasExPostEvaluation
###################
Evaluation for the purpose of reviewing the study, data collection, data processing, or management processes.




hasFundingInformation
#####################
Information about the individual, agency and/or grant(s) which funded the described entity. 




hasInstanceVariable
###################
A variable describing actual instances of data as recorded in the study. An example might be self reported gender, with categories of male encoded as "0"  and female encoded as "1", with missing values recorded as  "-9" for refused and "-8" as "not asked", recorded (physically) as an integer.




hasInstrument
#############
The implemented instruments used in capturing the data. The ImplentedInstrument designates the mode of delivery, a relation to an external representation of the instrument, and reference to the conceptual instrument that contains more detailed information on the content of the instrument.




hasMethodology
##############
A relationship to a description of a simple overview, in which case the MethodologyOverView class should be the target. 




hasPopulation
#############
The set of specific Units studied. Examples: 1. Canadian adult persons residing in Canada on 13 November 1956 2. US computer companies at the end of 2012 3. Universities in Denmark 1 January 2011.




hasProcess
##########
Overview of the Process employed in the Study




hasQualityStatement
###################
A statement of quality which may be related to an external standard or contain a simple statement of internal quality goals or expectations.




hasSamplingProcedure
####################
The methodologies for sampling employed by the study.




hasUniverse
###########
The defined classes of Units related to the data,  Examples: 1. Canadian adults (not limited to those residing in Canada) 2. Computer companies 3. Universities




hasVariableCollection
#####################
A Study may relate to a multiple collections of variables which may be defined by type, subject, use, source, etc.




partOfSeries
############
A series in which this Study takes part.




realizes
########
A Study can be a member of a Collection (a StudySeries)



