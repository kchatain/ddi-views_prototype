.. _Study:


Study
*****


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Study is the conceptual anchor for an individual collection activity resulting in a data set or data file. It can be both a part of a StudySeries as a wave etc. or an unrelated activity like a single survey project.


Synonyms
========



Explanatory notes
=================
The Study class brings together many properties and relationships describing a set of data – coverage, kind of data, methodology, citation information, access information and more.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst