************
StudyRelated
************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Budget/index.rst
   ComplianceStatement/index.rst
   Embargo/index.rst
   ExPostEvaluation/index.rst
   QualityStatement/index.rst
   Standard/index.rst
   Study/index.rst
   StudyControl/index.rst
   StudyRelationStructure/index.rst
   StudySeries/index.rst



Graph
=====

.. graphviz:: /images/graph/StudyRelated/StudyRelated.dot