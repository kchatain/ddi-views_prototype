.. _Identifiable:


Identifiable
************



Definition
==========
Used to identify objects for purposes of internal and/or external referencing. Elements of this type are versioned and provide administrative metadata properties. Use for First Order Classes whose content does not need to be discoverable in its own right but needs to be related to multiple classes.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst