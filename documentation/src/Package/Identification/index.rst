**************
Identification
**************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AnnotatedIdentifiable/index.rst
   Identifiable/index.rst



Graph
=====

.. graphviz:: /images/graph/Identification/Identification.dot