.. _fields:


AnnotatedIdentifiable - Properties and Relationships
****************************************************





Properties
==========

=============  ==========  ===========
Name           Type        Cardinality
=============  ==========  ===========
hasAnnotation  Annotation  0..1
=============  ==========  ===========


hasAnnotation
#############
Provides annotation information on the object to support citation and crediting of the creator(s) of the object.




Relationships
=============

===============  ================  ===========
Name             Type              Cardinality
===============  ================  ===========
relatedMaterial  ExternalMaterial  0..n
===============  ================  ===========


relatedMaterial
###############
Used to reference external resources. Should include the citation to such material, an external reference to a URI, and a statement of the relationship of the material to the object it has been associated with.



