.. _CustomInstance:


CustomInstance
**************


Extends
=======
:ref:`Identifiable`


Definition
==========
A set of CustomValues to be attached to some object.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst