.. _CustomItem:


CustomItem
**********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A custom item description. This allows the definition of an item which is a member of a CustomStructure. It defines the minimum and maximum number of occurrences and representation of a CustomValue.


Synonyms
========



Explanatory notes
=================
This definition will be referenced by a CustomValue when recording a key,value instance


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst