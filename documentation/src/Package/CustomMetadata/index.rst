**************
CustomMetadata
**************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   ControlledVocabulary/index.rst
   CustomInstance/index.rst
   CustomItem/index.rst
   CustomItemRelationStructure/index.rst
   CustomStructure/index.rst
   CustomValue/index.rst
   VocabularyEntry/index.rst
   VocabularyRelationStructure/index.rst



Graph
=====

.. graphviz:: /images/graph/CustomMetadata/CustomMetadata.dot