.. _fields:


CustomValue - Properties and Relationships
******************************************





Properties
==========

=====  ===========  ===========
Name   Type         Cardinality
=====  ===========  ===========
key    String       0..1
value  ValueString  0..1
=====  ===========  ===========


key
###
The unique String identifying the value


value
#####
the value associated with a particular key expressed as a string. The ultimate value representation is defined by the corresponding CustomValue.




Relationships
=============

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
comparesTo     InstanceVariable  0..n
correspondsTo  CustomItem        0..n
realizes       CollectionMember  0..n
=============  ================  ===========


comparesTo
##########
This optional relationship indicates an InstanceVariable compatible with the value associated with this key. Custom metadata might be reused as data and data might be used as metadata. This mechanism could enable these transformations. The related InstanceVariable could also carry information about units of measurement, missing values etc.




correspondsTo
#############
The CustomItem which defines the key for this key,value pair, It also defines a ValueRepresentation




realizes
########
Collection class realized by this class



