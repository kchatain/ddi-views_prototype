.. _fields:


Service - Properties and Relationships
**************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
serviceLocation  ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


serviceLocation
###############
Specifies where the service can be accessed.




Relationships
=============

========  =====  ===========
Name      Type   Cardinality
========  =====  ===========
hasAgent  Agent  0..n
========  =====  ===========


hasAgent
########
Actor that performs a role in the service,



