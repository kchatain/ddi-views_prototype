**************
ProcessPattern
**************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Process/index.rst
   ProcessControlStep/index.rst
   ProcessStep/index.rst
   Service/index.rst



Graph
=====

.. graphviz:: /images/graph/ProcessPattern/ProcessPattern.dot