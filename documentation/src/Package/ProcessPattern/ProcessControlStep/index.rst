.. _ProcessControlStep:


ProcessControlStep
******************


Extends
=======
:ref:`ProcessStep`


Definition
==========
A Process Step that controls the ordering of execution for one or more Process Steps. May be realized to provide needed controls such as sequencing, temporal relationships, or conditions as the means of ordering or triggering a Process Step. The abstract class does not dictate the means of designating the trigger for initiating a Process Step or ordering of those Process Steps. This provides the flexibility to define these in a way most appropriate to the realization and use.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst