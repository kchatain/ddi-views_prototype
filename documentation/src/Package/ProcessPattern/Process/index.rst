.. _Process:


Process
*******


Extends
=======
:ref:`Identifiable`


Definition
==========
Process is an implementation of an algorithm. It is the set of process steps taken as a whole. It is decomposable into a single ProcessStep or multiple ProcessSteps organized by a Process Control Step.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst