.. _examples:


Process - Examples
******************


The Generic Longitudinal Business Process Model (GLBPM) identifies a series of processes like data integration, anonymization, analysis variable construction and so forth in the data lifecycle.