.. _ProcessStep:


ProcessStep
***********


Extends
=======
:ref:`Identifiable`


Definition
==========
One of the constituents of a Process. It can be a composition or atomic and might be performed by a Service.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst