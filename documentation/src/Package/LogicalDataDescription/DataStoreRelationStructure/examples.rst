.. _examples:


DataStoreRelationStructure - Examples
*************************************


If the succession of DataStores are created by a StudySeries that is a time series, from one DataStore to the next part/whole relations can obtain as supplements are added and subtracted.