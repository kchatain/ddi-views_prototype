.. _fields:


DataStore - Properties and Relationships
****************************************





Properties
==========

=============  =================================  ===========
Name           Type                               Cardinality
=============  =================================  ===========
aboutMissing   InternationalStructuredString      0..1
characterSet   String                             0..1
contains       LogicalRecordIndicator             0..n
dataStoreType  ExternalControlledVocabularyEntry  0..1
isOrdered      Boolean                            0..1
name           ObjectName                         0..n
purpose        InternationalStructuredString      0..1
recordCount    Integer                            0..1
type           CollectionType                     0..1
=============  =================================  ===========


aboutMissing
############
General information about missing data, e.g., that missing data have been standardized across the collection, missing data are present because of merging, etc.-  corresponds to DDI2.5 dataMsng.


characterSet
############
Default character set used in the Data Store.


contains
########
Data in the form of Data Records contained in the Data Store. Allows for the identification of the member and optionally provides an index for the member within an ordered array 


dataStoreType
#############
The type of DataStore. Could be delimited file, fixed record length file, relational database, etc. Points to an external definition which can be part of a controlled vocabulary maintained by the DDI Alliance.


isOrdered
#########
If members are ordered set to true, if unordered set to false.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


recordCount
###########
The number of records in the Data Store.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.




Relationships
=============

==================  ==============================  ===========
Name                Type                            Cardinality
==================  ==============================  ===========
definingConcept     Concept                         0..n
isInStudy           Study                           0..1
isStructuredBy      LogicalRecordRelationStructure  0..n
realizes            StructuredCollection            0..n
usesRecordRelation  RecordRelation                  0..n
==================  ==============================  ===========


definingConcept
###############
The conceptual basis for the collection of members.




isInStudy
#########
A Study has at most one DataStore. Many studies can have the same set of record types.




isStructuredBy
##############
Description of a complex structure for the Collection. A collection may be structured in more than one way for different uses.




realizes
########
Allows the DataStore to function as a Collection, enabling, for example,  ordering of its components.




usesRecordRelation
##################
The Record Relation that defines the relationship and linking requirements between LogicalRecords in the DataStore 



