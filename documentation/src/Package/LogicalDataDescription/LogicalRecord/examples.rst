.. _examples:


LogicalRecord - Examples
************************


SQL Data Definition Language (DDL) traffics in record definitions. SQL queries on "system tables" discover record definitions.