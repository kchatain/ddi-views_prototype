.. _ViewpointRole:


ViewpointRole
*************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A ViewpointRole designates the function an InstanceVariable performs in the context of the Viewpoint. (IdentifierRole, AttributeRole, or MeasureRole of interest). Each of three roles within a Viewpoint may be a collection. This happens when a role is mapped to multiple instance variables. In this event a role forms a SimpleCollection. There are SimpleCollections of instance variables in each role.


Synonyms
========



Explanatory notes
=================
See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst