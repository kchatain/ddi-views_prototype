.. _DataPoint:


DataPoint
*********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A DataPoint is a container for a Datum.


Synonyms
========



Explanatory notes
=================
The DataPoint is structural and distinct from the value (the Datum) that it holds. [GSIM 1.1]


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst