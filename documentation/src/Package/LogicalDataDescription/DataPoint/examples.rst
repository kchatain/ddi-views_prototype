.. _examples:


DataPoint - Examples
********************


A cell in a spreadsheet table. Note that this could be empty. It exists independently of the value to be stored in it.