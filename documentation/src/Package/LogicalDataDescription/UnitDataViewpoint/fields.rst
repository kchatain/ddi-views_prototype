.. _fields:


UnitDataViewpoint - Properties and Relationships
************************************************





Relationships
=============

=================  ==============  ===========
Name               Type            Cardinality
=================  ==============  ===========
hasAttributeRole   AttributeRole   0..1
hasIdentifierRole  IdentifierRole  0..1
hasMeasureRole     MeasureRole     0..1
=================  ==============  ===========


hasAttributeRole
################
The InstanceVaribles functioning as attributes




hasIdentifierRole
#################
The InstanceVaribles functioning as identifiers




hasMeasureRole
##############
The InstanceVaribles functioning as measures



