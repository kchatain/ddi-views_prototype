.. _fields:


RecordRelation - Properties and Relationships
*********************************************





Properties
==========

==============  =============================  ===========
Name            Type                           Cardinality
==============  =============================  ===========
correspondence  InstanceVariableValueMap       0..n
displayLabel    LabelForDisplay                0..n
purpose         InternationalStructuredString  0..1
usage           InternationalStructuredString  0..1
==============  =============================  ===========


correspondence
##############
Correspondences between Instance Variables of different Logical Record Layout. 


displayLabel
############
A display label for the CollectionCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

========  ==============  ===========
Name      Type            Cardinality
========  ==============  ===========
maps      UnitDataRecord  0..n
realizes  Comparison      0..n
========  ==============  ===========


maps
####
Map related record types, which are Collections of Instance Variables. Realization of structures in Symmetric Relation.





realizes
########
Collection class realized by this class



