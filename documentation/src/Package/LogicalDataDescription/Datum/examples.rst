.. _examples:


Datum - Examples
****************


A systolic blood pressure of 122 is measured. The Signifier for that measurement in this paragraph is the character string "122". The Datum in this case is a kind of Designation (Sign), which is the association of the underlying measured concept (a blood pressure at that level) with that Signifier. The Datum has an association with an InstanceVariable which allows the attachment of a unit of measurement (mm Hg), a datatype, a Population, and the Act which produced the measurement. These InstanceVariable attributes are critical for interpreting the Signifier.