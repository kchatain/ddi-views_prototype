**********************
LogicalDataDescription
**********************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AttributeRole/index.rst
   DataPoint/index.rst
   DataStore/index.rst
   DataStoreLibrary/index.rst
   DataStoreRelationStructure/index.rst
   Datum/index.rst
   IdentifierRole/index.rst
   InstanceVariableRelationStructure/index.rst
   LogicalRecord/index.rst
   LogicalRecordRelationStructure/index.rst
   MeasureRole/index.rst
   RecordRelation/index.rst
   UnitDataRecord/index.rst
   UnitDataViewpoint/index.rst
   ViewpointRole/index.rst



Graph
=====

.. graphviz:: /images/graph/LogicalDataDescription/LogicalDataDescription.dot