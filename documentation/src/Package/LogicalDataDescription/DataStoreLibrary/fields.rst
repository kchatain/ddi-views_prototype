.. _fields:


DataStoreLibrary - Properties and Relationships
***********************************************





Properties
==========

=========  =============================  ===========
Name       Type                           Cardinality
=========  =============================  ===========
contains   DataStoreIndicator             0..n
isOrdered  Boolean                        0..1
name       ObjectName                     0..n
purpose    InternationalStructuredString  0..1
type       CollectionType                 0..1
=========  =============================  ===========


contains
########
The DataStores in the Catalog. Allows for the identification of the member and optionally provides an index for the member within an ordered array 


isOrdered
#########
If members are ordered set to true, if unordered set to false.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.




Relationships
=============

===============  ==========================  ===========
Name             Type                        Cardinality
===============  ==========================  ===========
definingConcept  Concept                     0..n
isInStudySeries  StudySeries                 0..n
isStructuredBy   DataStoreRelationStructure  0..n
realizes         StructuredCollection        0..n
===============  ==========================  ===========


definingConcept
###############
The conceptual basis for the collection of members.




isInStudySeries
###############
Whereas a DataStore is associated with a Study, a DataSoreLibrary is associated with a StudySeries. Each StudySeries has at most one DataStoreLibrary




isStructuredBy
##############
Description of a complex structure for the Collection. A collection may be structured in more than one way for different uses.




realizes
########
Realizes the pattern collection



