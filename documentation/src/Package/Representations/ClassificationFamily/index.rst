.. _ClassificationFamily:


ClassificationFamily
********************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A Classification Family is a collection of Classification Series related from a particular point of view. The Classification Family is related by being based on a common Concept, as documented by the definingConcept relationship (e.g. economic activity).[GSIM1.1]


Synonyms
========



Explanatory notes
=================
Different classification databases may use different types of Classification Families and have different names for the families, as no standard has been agreed upon. [GSIM1.1]


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst