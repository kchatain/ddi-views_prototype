.. _IndexEntryRelationStructure:


IndexEntryRelationStructure
***************************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Structures relationship of Classification Index Entries in a Classification Index.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst