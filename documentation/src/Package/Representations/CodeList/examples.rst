.. _examples:


CodeList - Examples
*******************


The codes "M" and "F" could point to "Male" and "Female" categories respectively. A CodeList for an occupational classification system like ISCO-08 could use a ClassificationRelationStructure to describe a hierarchy (Chief Executives and Administrative and Commercial Managers as subtypes of Managers)