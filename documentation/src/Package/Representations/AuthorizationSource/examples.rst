.. _examples:


AuthorizationSource - Examples
******************************


May be used to list authorizations from oversight committees and other regulatory agencies.