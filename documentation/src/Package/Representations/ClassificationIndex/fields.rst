.. _fields:


ClassificationIndex - Properties and Relationships
**************************************************





Properties
==========

=================  =================================  ===========
Name               Type                               Cardinality
=================  =================================  ===========
availableLanguage                                     0..n
codingInstruction  CommandCode                        0..n
contains           ClassificationIndexEntryIndicator  0..n
corrections        InternationalString                0..n
isOrdered          Boolean                            0..1
name               ObjectName                         0..n
purpose            InternationalStructuredString      0..1
releaseDate        Date                               0..1
type               CollectionType                     0..1
=================  =================================  ===========


availableLanguage
#################
A Classification Index can exist in several languages. Indicates the languages available. If a Classification Index exists in several languages, the number of entries in each language may be different, as the number of terms describing the same phenomenon can change from one language to another. However, the same phenomena should be described in each language. Supports the indication of multiple languages within a single property. Supports use of codes defined by the RFC 1766.


codingInstruction
#################
Additional information which drives the coding process for all entries in a Classification Index.


contains
########
Allows for the identification of the member and optionally provides an index for the member within an ordered array


corrections
###########
Verbal summary description of corrections, which have occurred within the Classification Index. Corrections include changing the item code associated with an Classification Index Entry.


isOrdered
#########
If members are ordered set to true, if unordered set to false.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


releaseDate
###########
Date when the current version of the Classification Index was released.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.




Relationships
=============

===============  ===========================  ===========
Name             Type                         Cardinality
===============  ===========================  ===========
contactPerson    Agent                        0..n
definingConcept  Concept                      0..n
hasPublication   ExternalMaterial             0..n
isStructuredBy   IndexEntryRelationStructure  0..n
maintenanceUnit  Agent                        0..n
realizes         StructuredCollection         0..n
===============  ===========================  ===========


contactPerson
#############
Person(s) who may be contacted for additional information about the Classification Index.




definingConcept
###############
The conceptual basis for the collection of members.




hasPublication
##############
A list of the publications in which the Classification Index has been published.




isStructuredBy
##############
Description of a complex structure for the Collection. A collection may be structured in more than one way for different uses.




maintenanceUnit
###############
The unit or group of persons within the organization responsible for the Classification Index, i.e. for adding, changing or deleting Classification Index Entries.




realizes
########
Class of the Collection pattern realized by this class. 



