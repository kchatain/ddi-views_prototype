.. _CorrespondenceTable:


CorrespondenceTable
*******************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A Correspondence Table expresses relationships between the members within or between StatisticalClassifications.


Synonyms
========



Explanatory notes
=================
CorrespondenceTables are used with Statistical Classifications. For instance, it can relate two versions from the same Classification Series; Statistical Classifications from different Classification Series; a variant and the version on which it is based; or, different versions of a variant. In the first and last examples, the Correspondence Table facilitates comparability over time.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst