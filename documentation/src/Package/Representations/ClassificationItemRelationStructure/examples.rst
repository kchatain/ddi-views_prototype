.. _examples:


ClassificationItemRelationStructure - Examples
**********************************************


A ClassificationRelationStructure for ISCO-08 would describe each of the major classifications as a parent of its sub-classifications. 1 Managers, for example would be listed as a parent of four sub groups: 11 Chief Executives, Senior Officials and Legislators; 12 Chief Executives, Senior Officials and Legislators; 13 Production and Specialized Services Managers; and 14 Hospitality, Retail and Other Services Managers.