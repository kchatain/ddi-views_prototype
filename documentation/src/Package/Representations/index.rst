***************
Representations
***************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AuthorizationSource/index.rst
   ClassificationFamily/index.rst
   ClassificationIndex/index.rst
   ClassificationIndexEntry/index.rst
   ClassificationItem/index.rst
   ClassificationItemRelationStructure/index.rst
   ClassificationSeries/index.rst
   ClassificationSeriesRelationStructure/index.rst
   Code/index.rst
   CodeList/index.rst
   CodeRelationStructure/index.rst
   CorrespondenceTable/index.rst
   Designation/index.rst
   EnumerationDomain/index.rst
   IndexEntryRelationStructure/index.rst
   LevelStructure/index.rst
   SentinelValueDomain/index.rst
   StatisticalClassification/index.rst
   StatisticalClassificationRelationStructure/index.rst
   SubstantiveValueDomain/index.rst
   ValueAndConceptDescription/index.rst
   ValueDomain/index.rst



Graph
=====

.. graphviz:: /images/graph/Representations/Representations.dot