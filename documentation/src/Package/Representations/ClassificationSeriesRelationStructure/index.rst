.. _ClassificationSeriesRelationStructure:


ClassificationSeriesRelationStructure
*************************************


Extends
=======
:ref:`Identifiable`


Definition
==========
Describes the complex relation structure of a classification family.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst