.. _Designation:


Designation
***********


Extends
=======
:ref:`Identifiable`


Definition
==========
A sign denoting a concept.


Synonyms
========



Explanatory notes
=================
The representation of a concept by a sign (e.g., string, pictogram, bitmap) which denotes it. An example is the code M designating the marital status Married, which is a concept. In this context, M means Married.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst