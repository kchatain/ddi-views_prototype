******************
MethodologyPattern
******************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Algorithm/index.rst
   Design/index.rst
   Methodology/index.rst



Graph
=====

.. graphviz:: /images/graph/MethodologyPattern/MethodologyPattern.dot