.. _Methodology:


Methodology
***********


Extends
=======
:ref:`Identifiable`


Definition
==========
Methodology brings together the design, algorithm, and process as a logically structured approach to an activity such as sampling, weighting, harmonization, imputation, collection management, etc. A methodology in normally informed by earlier research and clarifies how earlier research methods were incorporated into the current work.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst