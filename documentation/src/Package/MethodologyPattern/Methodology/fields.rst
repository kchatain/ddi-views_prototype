.. _fields:


Methodology - Properties and Relationships
******************************************





Properties
==========

=========  =============================  ===========
Name       Type                           Cardinality
=========  =============================  ===========
name       ObjectName                     0..n
overview   InternationalStructuredString  0..1
rationale  InternationalStructuredString  0..1
usage      InternationalStructuredString  0..1
=========  =============================  ===========


name
####
A linguistic signifier. Human  understandable name (word, phrase, or  mnemonic) that reflects the ISO/IEC  11179-5 naming principles. If more than  one name is provided provide a context to  differentiate usage. 


overview
########
Short natural language account of the  information obtained from the  combination of properties and  relationships associated with an object.  Supports the use of multiple languages  and structured text. 


rationale
#########
Explanation of the reasons some decision  was made or some object exists. Supports  the use of multiple languages and  structured text.  


usage
#####
Explanation of the ways in which some  decision or object is employed. Supports  the use of multiple languages and  structured text.  




Relationships
=============

====================  ===========  ===========
Name                  Type         Cardinality
====================  ===========  ===========
componentMethodology  Methodology  0..n
hasDesign             Design       0..n
hasProcess            Process      0..n
isExpressedBy         Algorithm    0..n
====================  ===========  ===========


componentMethodology
####################
A methodology which is a component part of this Methodology




hasDesign
#########
Design used to form the Methodology




hasProcess
##########
Process used to implement the Methodology




isExpressedBy
#############
An algorithm that defines the Methodology in a generic way.



