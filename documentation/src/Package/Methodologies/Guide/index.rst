.. _Guide:


Guide
*****


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Provides a guide for the usage of a result within a specified application


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst