.. _fields:


Goal - Properties and Relationships
***********************************





Relationships
=============

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
isDiscussedIn  ExternalMaterial  0..n
=============  ================  ===========


isDiscussedIn
#############
Identifies material discussing the goal. The material may be in DDI or other format.



