.. _examples:


Goal - Examples
***************


To further distinguish a "goal" from an "output", consider the construction of a statistic. A goal may be the construction of the statistic. The output is the statistic. The output is data. The statistic by itself -- the output -- is arguably meaningless even if we capture the process of its construction.