.. _fields:


AppliedUse - Properties and Relationships
*****************************************





Properties
==========

========  =============================  ===========
Name      Type                           Cardinality
========  =============================  ===========
overview  InternationalStructuredString  0..1
========  =============================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.




Relationships
=============

=================  =====================  ===========
Name               Type                   Cardinality
=================  =====================  ===========
appliesToUnitType  UnitType               0..n
hasGuide           Guide                  0..n
isUsedBy           AnnotatedIdentifiable  0..n
=================  =====================  ===========


appliesToUnitType
#################
The unit type or types for which the guide is appropriate




hasGuide
########
The guide describing the appropriate usage of the result in the context of specified unit types and/or objects.




isUsedBy
########
Specifies the object for which the guide is appropriate



