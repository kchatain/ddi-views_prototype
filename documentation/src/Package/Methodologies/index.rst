*************
Methodologies
*************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AppliedUse/index.rst
   BusinessFunction/index.rst
   Goal/index.rst
   Guide/index.rst
   Precondition/index.rst
   Result/index.rst



Graph
=====

.. graphviz:: /images/graph/Methodologies/Methodologies.dot