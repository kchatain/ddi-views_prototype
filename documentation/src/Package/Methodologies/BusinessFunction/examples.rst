.. _examples:


BusinessFunction - Examples
***************************


A Business Function may be defined directly with descriptive text and/or through reference to an existing catalogue of Business Functions.