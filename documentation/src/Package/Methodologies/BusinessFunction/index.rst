.. _BusinessFunction:


BusinessFunction
****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Something an enterprise does, or needs to do, in order to achieve its objectives. A Business Function delivers added value from a business point of view. It is delivered by bringing together people, processes and technology (resources), for a specific business purpose. Business Functions answer in a generic sense "What business purpose does this Business Service or Process Step serve?" Through identifying the Business Function associated with each Business Service or Process Step it increases the documentation of the use of the associated Business Services and Process Steps, to enable future reuse.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst