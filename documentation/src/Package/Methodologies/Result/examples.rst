.. _examples:


Result - Examples
*****************


The use of a weight resulting from a sampling process by an analyst within the context of a specific set of variables. The class containing the weight would use the extension base Result, adding any additional required properties and relationships to describe it.