.. _fields:


Result - Properties and Relationships
*************************************





Properties
==========

==========  =============================  ===========
Name        Type                           Cardinality
==========  =============================  ===========
hasBinding  Binding                        0..n
overview    InternationalStructuredString  0..1
==========  =============================  ===========


hasBinding
##########
Can bind data attached to parameters into, out of, and between processes.


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.




Relationships
=============

===================  ==========  ===========
Name                 Type        Cardinality
===================  ==========  ===========
evaluateAgainstGoal  Goal        0..n
hasAppliedUse        AppliedUse  0..n
hasInputParameter    Parameter   0..1
hasOutputParameter   Parameter   0..1
===================  ==========  ===========


evaluateAgainstGoal
###################
Comparison of Result of the Process against the Goal to inform further activities.




hasAppliedUse
#############
The result may have one or more specific usages which provides guidance for the use of the result with a specific unit type.




hasInputParameter
#################
Used by Binding to link the output of another class to the input of this class 




hasOutputParameter
##################
Used by Binding to link the output of this class to the input of another class 



