*************************
SimpleMethodologyOverview
*************************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AlgorithmOverview/index.rst
   DesignOverview/index.rst
   MethodologyOverview/index.rst
   ProcessOverview/index.rst



Graph
=====

.. graphviz:: /images/graph/SimpleMethodologyOverview/SimpleMethodologyOverview.dot