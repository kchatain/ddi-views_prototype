.. _fields:


ProcessOverview - Properties and Relationships
**********************************************





Properties
==========

========  =============================  ===========
Name      Type                           Cardinality
========  =============================  ===========
name      ObjectName                     0..n
overview  InternationalStructuredString  0..1
========  =============================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text. Edit




Relationships
=============

===================  ===================  ===========
Name                 Type                 Cardinality
===================  ===================  ===========
implementsAlgorithm  AlgorithmOverview    0..n
isSpecifiedBy        WorkflowControlStep  0..n
realizes             Process              0..n
===================  ===================  ===========


implementsAlgorithm
###################
An implementation and/or execution of an algorithm which is associated with the design of the process.




isSpecifiedBy
#############
The Process Control Step which describes the organization of the process flow. This could be realizes as a sequence, a temporal relationship, or conditional control step. It should provide a clear entry and exit point(s) for the process.




realizes
########
Realization of the pattern class Process



