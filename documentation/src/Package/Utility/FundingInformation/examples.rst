.. _examples:


FundingInformation - Examples
*****************************


A "millionaire grant" (funding description) from John Beresford Tipton, Jr. (individual). Exploration of the effect of sudden wealth and basis for a television episode (funder role).