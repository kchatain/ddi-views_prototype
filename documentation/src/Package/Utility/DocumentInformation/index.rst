.. _DocumentInformation:


DocumentInformation
*******************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Provides a consistent path to identifying information regarding the DDI Document and is automatically available for all Functional Views (/DocumentInformation/Annotation etc.). It covers annotation information, coverage (general, plus specific spatial, temporal, and topical coverage), access information from the producer (persistent access control), access information from the local distributor (Local Access Control), information on related series (i.e. a study done as a series of data capture events over time, qualitative material that is part of a larger set, one of a series of funded data capture activities, etc.), funding information, and other document level information used to identify and discover information about a DDI document regardless of its Functional View type. Use the Annotation at this level to provide the full annotation of the DDI Document. Note that related materials can be entered within the Annotation fields.


Synonyms
========



Explanatory notes
=================
Annotation associated with a specific class is the annotation of the metadata class itself, such as the Study, InstanceVariable, Concept, etc. The annotation associated with Document Information is to refer to the Document itself. Note that not all DDI instances are intended to be persistent documents. For those that are DocumentInformation provides a consistent set of discovery level information on the content of the whole rather than specific parts of the instance.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst