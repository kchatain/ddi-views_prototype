.. _GeographicUnitTypeClassification:


GeographicUnitTypeClassification
********************************


Extends
=======
:ref:`EnumerationDomain`


Definition
==========
A structured collection of Unit Types defining a geographic structure. As a subtype of CodeList it may be used directly to describe a value domain.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst