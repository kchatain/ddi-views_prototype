************************
GeographicClassification
************************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   GeographicExtent/index.rst
   GeographicUnit/index.rst
   GeographicUnitClassification/index.rst
   GeographicUnitRelationStructure/index.rst
   GeographicUnitTypeClassification/index.rst
   GeographicUnitTypeRelationStructure/index.rst



Graph
=====

.. graphviz:: /images/graph/GeographicClassification/GeographicClassification.dot