.. _fields:


GeographicUnitRelationStructure - Properties and Relationships
**************************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasMemberRelation         GeographicUnitRelation             0..n
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasMemberRelation
#################
MemberRelation used to define the relationship of members within the collection


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list


semantic
########
Provides semantic context for the relationship context for the relationship using an External Controlled Vocabulary. Examples might include "parent-child", or "immediate supervisee"


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown.




Relationships
=============

========  =================  ===========
Name      Type               Cardinality
========  =================  ===========
realizes  RelationStructure  0..n
========  =================  ===========


realizes
########
Collection class realized by this class



