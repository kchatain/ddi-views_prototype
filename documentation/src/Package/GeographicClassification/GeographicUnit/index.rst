.. _GeographicUnit:


GeographicUnit
**************


Extends
=======
:ref:`Unit`


Definition
==========
A specific geographic unit of a defined Unit Type. A geographic unit may change its name, composition, or geographic extent over time. This may be tracked by versioning the content of the geographic unit. Normally a new version of a geographic unit would have a geographic (spatial) overlap with its previous version (a city annexing new area). A geographic unit ends when it is no longer a unit of the same unit type.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst