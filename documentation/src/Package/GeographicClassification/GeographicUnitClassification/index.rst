.. _GeographicUnitClassification:


GeographicUnitClassification
****************************


Extends
=======
:ref:`EnumerationDomain`


Definition
==========
Describes the classification of specific geographic units into a classification system. As a subtype of Code List it can be used directly for the description of a value domain.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst