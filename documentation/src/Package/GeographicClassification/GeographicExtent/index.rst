.. _GeographicExtent:


GeographicExtent
****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Defines the extent of a geographic unit for a specified period of time using Bounding Box, Inclusive and Exclusive Polygons, and an area coverage that notes a type of coverage (land, water, etc.) and measurement for the coverage. The same geographic extent may be used by multiple versions of a single geographic version or by different geographic units occupying the same spatial area.


Synonyms
========



Explanatory notes
=================
Clarifies the source of a change in terms of footprint of an area as opposed to a name or coding change.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst