.. _Agent:


Agent
*****


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
An actor that performs a role in relation to a process or product.


Synonyms
========
Agent


Explanatory notes
=================
foaf:Agent is: An agent (eg. person, group, software or physical artifact) prov:Agent is An agent is something that bears some form of responsibility for an activity taking place, for the existence of an entity, or for another agent's activity.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst