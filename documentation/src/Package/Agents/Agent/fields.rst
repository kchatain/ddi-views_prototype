.. _fields:


Agent - Properties and Relationships
************************************





Properties
==========

==========  =============================  ===========
Name        Type                           Cardinality
==========  =============================  ===========
hasAgentId  AgentId                        0..n
image       PrivateImage                   0..n
purpose     InternationalStructuredString  0..1
==========  =============================  ===========


hasAgentId
##########
An identifier within a specified system for specifying an agent


image
#####
References an image using the standard Image description. In addition to the standard attributes provides an effective date (period), the type of image, and a privacy ranking.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.




Relationships
=============

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
realizes  CollectionMember  0..n
========  ================  ===========


realizes
########
An Agent can perform the role of a Member in an Agent Registry



