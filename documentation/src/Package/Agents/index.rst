******
Agents
******

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Agent/index.rst
   AgentListing/index.rst
   AgentRelationStructure/index.rst
   Individual/index.rst
   Machine/index.rst
   Organization/index.rst



Graph
=====

.. graphviz:: /images/graph/Agents/Agents.dot