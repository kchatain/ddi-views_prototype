.. _InstanceVariableRelation:


InstanceVariableRelation
************************



Definition
==========
Defines the relationship between 2 or more InstanceVariables.


Synonyms
========



Explanatory notes
=================
Use when relationships are limited to InstanceVariables only. Use VariableRelation when mixing relationships between various levels in the Variable Cascade. Use ConceptRelation when any subtype of Concept may be part of the relationship.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst