.. _LabelForDisplay:


LabelForDisplay
***************


Extends
=======
:ref:`InternationalStructuredString`


Definition
==========
A structured display label. Label provides display content of a fully human readable display for the identification of the object.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst