.. _AgentId:


AgentId
*******



Definition
==========
Persistent identifier for a researcher using a system like ORCID


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst