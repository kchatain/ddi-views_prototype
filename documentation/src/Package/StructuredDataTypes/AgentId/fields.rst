.. _fields:


AgentId - Properties and Relationships
**************************************





Properties
==========

============  ======  ===========
Name          Type    Cardinality
============  ======  ===========
agentIdType   String  1..1
agentIdValue  String  1..1
============  ======  ===========


agentIdType
###########
The identifier system in use.


agentIdValue
############
The identifier for the agent.

