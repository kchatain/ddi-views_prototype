.. _GeographicUnitRelation:


GeographicUnitRelation
**********************



Definition
==========
Describes structured relationship between Geographic Units.


Synonyms
========



Explanatory notes
=================
A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows".


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst