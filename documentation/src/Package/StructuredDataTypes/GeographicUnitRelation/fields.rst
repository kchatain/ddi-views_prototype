.. _fields:


GeographicUnitRelation - Properties and Relationships
*****************************************************





Properties
==========

===============================  =================================  ===========
Name                             Type                               Cardinality
===============================  =================================  ===========
hasRelationSpecification         RelationSpecification              1..1
hasSpatialRelationSpecification  SpatialRelationSpecification       0..1
semantic                         ExternalControlledVocabularyEntry  0..1
totality                         TotalityType                       0..1
===============================  =================================  ===========


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list


hasSpatialRelationSpecification
###############################
Provides information on the spatial relationship between the parent and child.


semantic
########
Provide a semantic that provides a context for the relationship using and External Controlled Vocabulary


totality
########
Type of relation in terms of its totality using an enumeration list.




Relationships
=============

========  ==============  ===========
Name      Type            Cardinality
========  ==============  ===========
realizes  MemberRelation  0..n
source    GeographicUnit  0..n
target    GeographicUnit  0..n
========  ==============  ===========


realizes
########
Collection class realized by this class




source
######
The first member of the relationship.




target
######
Second member in the relationship. Notes this could be realized as a collection supporting tuple relationship.



