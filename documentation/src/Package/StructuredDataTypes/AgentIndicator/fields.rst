.. _fields:


AgentIndicator - Properties and Relationships
*********************************************





Properties
==========

=====  =======  ===========
Name   Type     Cardinality
=====  =======  ===========
index  Integer  0..1
=====  =======  ===========


index
#####
Index value within an ordered array




Relationships
=============

========  ===============  ===========
Name      Type             Cardinality
========  ===============  ===========
member    Agent            0..n
realizes  MemberIndicator  0..n
========  ===============  ===========


member
######
Restricts member target to Agent




realizes
########
Collection Pattern class realized by this class



