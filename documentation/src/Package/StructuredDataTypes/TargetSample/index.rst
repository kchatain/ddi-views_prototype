.. _TargetSample:


TargetSample
************



Definition
==========
Specifies details of target sample size, percentage, type, and universe


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst