.. _fields:


BibliographicName - Properties and Relationships
************************************************





Properties
==========

===========  ======  ===========
Name         Type    Cardinality
===========  ======  ===========
affiliation  String  0..1
===========  ======  ===========


affiliation
###########
The affiliation of this person to an organization. This is generally an organization or sub-organization name and should be related to the specific role within which the individual is being listed.

