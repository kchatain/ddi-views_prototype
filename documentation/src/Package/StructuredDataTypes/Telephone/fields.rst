.. _fields:


Telephone - Properties and Relationships
****************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
effectiveDates   DateRange                          0..1
isPreferred      Boolean                            0..1
privacy          ExternalControlledVocabularyEntry  0..1
telephoneNumber  String                             0..1
typeOfTelephone  ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


effectiveDates
##############
Time period during which the telephone number is valid.


isPreferred
###########
Set to "true" if this is the preferred telephone number for contact.


privacy
#######
Specify the level privacy for the telephone number as public, restricted, or private. Supports the use of an external controlled vocabulary.


telephoneNumber
###############
The telephone number including country code if appropriate.


typeOfTelephone
###############
Indicates type of telephone number provided (home, fax, office, cell, etc.). Supports the use of a controlled vocabulary.

