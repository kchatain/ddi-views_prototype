.. _PrivateImage:


PrivateImage
************


Extends
=======
:ref:`Image`


Definition
==========
References an image using the standard Image description. In addition to the standard attributes provides an effective date (period), the type of image, and a privacy ranking.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst