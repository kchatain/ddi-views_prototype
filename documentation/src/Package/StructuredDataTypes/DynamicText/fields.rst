.. _fields:


DynamicText - Properties and Relationships
******************************************





Properties
==========

===================  ==================  ===========
Name                 Type                Cardinality
===================  ==================  ===========
audienceLanguage                         0..n
isStructureRequired  Boolean             0..1
textContent          DynamicTextContent  1..n
===================  ==================  ===========


audienceLanguage
################
Specifies the language of the intended audience. This is particularly important for clarifying the primary language of a mixed language textual string, for example when language testing and using a foreign word withing the question text. Supports the inclusion of multiple languages.


isStructureRequired
###################
If textual structure (e.g. size, color, font, etc.) is required to understand the meaning of the content change value to "true".


textContent
###########
This is the head of a substitution group and is never used directly as an element name. Instead it is replaced with either LiteralText or ConditionalText.

