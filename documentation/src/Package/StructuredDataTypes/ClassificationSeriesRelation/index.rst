.. _ClassificationSeriesRelation:


ClassificationSeriesRelation
****************************



Definition
==========
Defines the Classification Series in a complex relation and specifies the relationshi


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst