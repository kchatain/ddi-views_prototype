.. _ValueString:


ValueString
***********



Definition
==========
The Value expressed as an xs:string with the ability to preserve whitespace if critical to the understanding of the content.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst