.. _fields:


IndexEntryRelation - Properties and Relationships
*************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  ========================  ===========
Name      Type                      Cardinality
========  ========================  ===========
realizes  MemberRelation            0..n
source    ClassificationIndexEntry  0..n
target    ClassificationIndexEntry  0..n
========  ========================  ===========


realizes
########
Class of the Collection pattern realized by this class. 




source
######
Restricts soruce to  ClassificationEntry.




target
######
Restricts target to  ClassificationEntry.



