.. _fields:


DateRange - Properties and Relationships
****************************************





Properties
==========

=========  ====  ===========
Name       Type  Cardinality
=========  ====  ===========
endDate    Date  0..1
startDate  Date  0..1
=========  ====  ===========


endDate
#######
The date (time) designating the end of the period or range.


startDate
#########
The date (time) designating the beginning of the period or range.

