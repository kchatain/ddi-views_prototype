.. _examples:


ResourceIdentifier - Examples
*****************************


Standard usage may include: describesDate, isDescribedBy, isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires, etc.