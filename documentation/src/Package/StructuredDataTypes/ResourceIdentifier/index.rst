.. _ResourceIdentifier:


ResourceIdentifier
******************


Extends
=======
:ref:`InternationalIdentifier`


Definition
==========
Provides a means of identifying a related resource and provides the typeOfRelationship.


Synonyms
========



Explanatory notes
=================
Makes use of a controlled vocabulary for typing the relationship.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst