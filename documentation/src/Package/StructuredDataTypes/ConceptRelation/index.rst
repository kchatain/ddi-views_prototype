.. _ConceptRelation:


ConceptRelation
***************



Definition
==========
Specifies the relationship between concepts in a collection of concepts. Use controlled vocabulary provided in hasRelationSpecification to identify the type of relationship (e.g. ParentChild, WholePart, etc.)


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst