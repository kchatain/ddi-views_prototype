.. _fields:


ConceptRelation - Properties and Relationships
**********************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasRelationSpecification
########################
RelationSpecification Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list


semantic
########
Provides semantic context for the relationship


totality
########
type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  ==============  ===========
Name      Type            Cardinality
========  ==============  ===========
realizes  MemberRelation  0..n
source    Concept         0..n
target    Concept         0..n
========  ==============  ===========


realizes
########
realizes the pattern OrderedPair




source
######
Restricts source object to Concept for relationshp




target
######
Restricts target object to Concept for relationshp



