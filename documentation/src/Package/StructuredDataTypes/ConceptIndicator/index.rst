.. _ConceptIndicator:


ConceptIndicator
****************



Definition
==========
Member Indicator for use with member type Concept and all subtypes of Concept: Category, Universe, Population, Unit Type


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst