.. _fields:


OrganizationName - Properties and Relationships
***********************************************





Properties
==========

======================  =================================  ===========
Name                    Type                               Cardinality
======================  =================================  ===========
abbreviation            InternationalString                0..1
effectiveDates          DateRange                          0..1
isFormal                Boolean                            0..1
typeOfOrganizationName  ExternalControlledVocabularyEntry  0..1
======================  =================================  ===========


abbreviation
############
An abbreviation or acronym for the name. This may be expressed in multiple languages. It is assumed that if only a single language is provided that it may be used in any of the other languages within which the name itself is expressed.


effectiveDates
##############
The time period for which this name is accurate and in use.


isFormal
########
The legal or formal name of the organization should have the isFormal attribute set to true. To avoid confusion only one organization name should have the isFormal attribute set to true. Use the TypeOfOrganizationName to further differentiate the type and applied usage when multiple names are provided.


typeOfOrganizationName
######################
The type of organization name provided. the use of a controlled vocabulary is strongly recommended. At minimum this should include, e.g. PreviousFormalName, Nickname (or CommonName), Other.

