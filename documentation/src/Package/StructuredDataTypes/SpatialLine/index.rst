.. _SpatialLine:


SpatialLine
***********



Definition
==========
Defines a geographic line (minimum of 2 points)


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst