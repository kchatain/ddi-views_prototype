.. _fields:


CommandFile - Properties and Relationships
******************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
location         InternationalString                0..1
programLanguage  ExternalControlledVocabularyEntry  0..1
uri              anyURI                             0..1
===============  =================================  ===========


location
########
A description of the location of the file. This may not be machine actionable. It supports a description expressed in multiple languages.


programLanguage
###############
Designates the programming language used for the command. Supports the use of a controlled vocabulary.


uri
###
The URL or URN of the command file.

