.. _fields:


LocalIdFormat - Properties and Relationships
********************************************





Properties
==========

==============  ======  ===========
Name            Type    Cardinality
==============  ======  ===========
localIdType     String  1..1
localIdValue    String  1..1
localIdVersion  String  0..1
==============  ======  ===========


localIdType
###########
Type of identifier, specifying the context of the identifier.


localIdValue
############
Value of the local ID.


localIdVersion
##############
Version of the Local ID.

