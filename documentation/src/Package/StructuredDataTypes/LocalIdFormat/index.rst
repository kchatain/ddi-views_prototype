.. _LocalIdFormat:


LocalIdFormat
*************



Definition
==========
This is an identifier in a given local context that uniquely references an object, as opposed to the full ddi identifier which has an agency plus the id.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst