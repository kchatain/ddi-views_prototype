.. _AgentRelation:


AgentRelation
*************



Definition
==========
Used to define the relation agents in a hierarchical structure


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst