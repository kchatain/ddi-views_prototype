.. _fields:


ExternalControlledVocabularyEntry - Properties and Relationships
****************************************************************





Properties
==========

==============================  ======  ===========
Name                            Type    Cardinality
==============================  ======  ===========
content                         String  0..1
controlledVocabularyAgencyName  String  0..1
controlledVocabularyID          String  0..1
controlledVocabularyName        String  0..1
controlledVocabularyVersionID   String  0..1
language                                0..1
otherValue                      String  0..1
uri                             anyURI  0..1
==============================  ======  ===========


content
#######
The value of the entry of the controlled vocabulary. If no controlled vocabulary is used the term is entered here and none of the properties defining the controlled vocabulary location are used.


controlledVocabularyAgencyName
##############################
The name of the agency maintaining the code list.


controlledVocabularyID
######################
The ID of the code list (controlled vocabulary) that the content was taken from.


controlledVocabularyName
########################
The name of the code list.


controlledVocabularyVersionID
#############################
The version number of the code list (default is 1.0).


language
########
Language of the content value if applicable


otherValue
##########
If the value of the string is "Other" or the equivalent from the codelist, this attribute can provide a more specific value not found in the codelist.


uri
###
The URN or URL of the controlled vocabulary

