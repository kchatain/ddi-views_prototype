.. _Level:


Level
*****



Definition
==========
Provides Level information for the members of the LevelStructure. levelNumber provides the level number which may or may not be associated with a category which defines level.


Synonyms
========



Explanatory notes
=================
ISCO-08: index='1' label of associated category 'Major', index='2' label of associated category 'Sub-Major', index='3' label of associated category 'Minor',


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst