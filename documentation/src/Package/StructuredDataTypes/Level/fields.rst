.. _fields:


Level - Properties and Relationships
************************************





Properties
==========

============  ===============  ===========
Name          Type             Cardinality
============  ===============  ===========
displayLabel  LabelForDisplay  0..n
levelNumber   Integer          1..1
============  ===============  ===========


displayLabel
############
A display label for the object. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


levelNumber
###########
Provides an association between a level number and optional concept which defines it within an ordered array. Use is required.




Relationships
=============

=========  =======  ===========
Name       Type     Cardinality
=========  =======  ===========
definedBy  Concept  0..n
=========  =======  ===========


definedBy
#########
A concept or concept sub-type which describes the level.



