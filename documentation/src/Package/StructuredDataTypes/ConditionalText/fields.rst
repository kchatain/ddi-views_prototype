.. _fields:


ConditionalText - Properties and Relationships
**********************************************





Properties
==========

==========  ===========  ===========
Name        Type         Cardinality
==========  ===========  ===========
expression  CommandCode  0..1
==========  ===========  ===========


expression
##########
The condition on which the associated text varies expressed by a command code. For example, a command that inserts an age by calculating the difference between today’s date and a previously defined date of birth.

