.. _ConditionalText:


ConditionalText
***************


Extends
=======
:ref:`DynamicTextContent`


Definition
==========
Text which has a changeable value depending on a stated condition, response to earlier questions, or as input from a set of metrics (pre-supplied data). The command code indicates the condition and/or calculation used to determine the content of the text.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst