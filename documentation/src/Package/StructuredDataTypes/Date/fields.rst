.. _fields:


Date - Properties and Relationships
***********************************





Properties
==========

==========  ==============  ===========
Name        Type            Cardinality
==========  ==============  ===========
isoDate     IsoDateType     0..1
nonIsoDate  NonIsoDateType  0..n
==========  ==============  ===========


isoDate
#######
Strongly recommend that ALL dates be expressed in an ISO format at a minimum. A single point in time expressed in an ISO standard structure. Note that while it supports an ISO date range structure this should be used in Date only when the single date is unclear i.e. occurring at some time between two dates. 


nonIsoDate
##########
A simple date expressed in a non-ISO date format, including a specification of the date format and calendar used.

