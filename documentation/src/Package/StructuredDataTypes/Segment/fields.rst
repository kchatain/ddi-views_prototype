.. _fields:


Segment - Properties and Relationships
**************************************





Properties
==========

===================  ==============  ===========
Name                 Type            Cardinality
===================  ==============  ===========
usesAudioSegment     AudioSegment    0..n
usesImageArea        ImageArea       0..n
usesVideoSegment     VideoSegment    0..n
useseTextualSegment  TextualSegment  0..n
xml                  String          0..n
===================  ==============  ===========


usesAudioSegment
################
Describes the type and length of the audio segment.


usesImageArea
#############
Defines the shape and area of an image used as part of a location representation. The shape is defined as a Rectangle, Circle, or Polygon and Coordinates provides the information required to define it.


usesVideoSegment
################
Describes the type and length of the video segment.


useseTextualSegment
###################
Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment


xml
###
An X-Pointer expression identifying a node in the XML document.

