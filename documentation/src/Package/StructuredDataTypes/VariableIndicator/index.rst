.. _VariableIndicator:


VariableIndicator
*****************



Definition
==========
Member Indicator for use with member type ConceptualVariable, RepresentedVariable, and InstanceVariable


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst