.. _fields:


RangeValue - Properties and Relationships
*****************************************





Properties
==========

========  =======  ===========
Name      Type     Cardinality
========  =======  ===========
included  Boolean  0..1
========  =======  ===========


included
########
Set to "true" if the value is included in the range.

