.. _fields:


GeographicUnitIndicator - Properties and Relationships
******************************************************





Properties
==========

=========  =======  ===========
Name       Type     Cardinality
=========  =======  ===========
index      Integer  0..1
isInLevel  Integer  0..1
=========  =======  ===========


index
#####
Index number expressed as an integer. The position of the member in an ordered array. Optional for unordered Collections.


isInLevel
#########
Indicates the level within which the CodeItem resides




Relationships
=============

========  ===============  ===========
Name      Type             Cardinality
========  ===============  ===========
member    GeographicUnit   0..n
realizes  MemberIndicator  0..n
========  ===============  ===========


member
######
Member of the collection.




realizes
########
Collection class realized by this class



