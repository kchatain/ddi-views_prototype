.. _CorrespondenceType:


CorrespondenceType
******************



Definition
==========
Describes the commonalities and differences between two members using a textual description of both commonalities and differences plus an optional coding of the type of commonality.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst