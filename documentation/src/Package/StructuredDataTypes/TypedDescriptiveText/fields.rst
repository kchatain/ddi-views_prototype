.. _fields:


TypedDescriptiveText - Properties and Relationships
***************************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
descriptiveText  InternationalStructuredString      0..1
typeOfContent    ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.


typeOfContent
#############
Uses a controlled vocabulary entry to classify the description provided.

