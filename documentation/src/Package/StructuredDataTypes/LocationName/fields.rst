.. _fields:


LocationName - Properties and Relationships
*******************************************





Properties
==========

==============  =========  ===========
Name            Type       Cardinality
==============  =========  ===========
effectiveDates  DateRange  0..1
==============  =========  ===========


effectiveDates
##############
The time period for which this name is accurate and in use.

