.. _LocationName:


LocationName
************


Extends
=======
:ref:`ObjectName`


Definition
==========
Name of the location using the DDI Name structure and the ability to add an effective date.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst