.. _fields:


AreaCoverage - Properties and Relationships
*******************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
areaMeasure      Real                               0..1
measurementUnit  ExternalControlledVocabularyEntry  0..1
typeOfArea       ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


areaMeasure
###########
The area measure expressed as a decimal for the measurement unit designated.


measurementUnit
###############
Records the measurement unit, for example, Square Kilometer, Square Mile. Supports the use of an external controlled vocabulary.


typeOfArea
##########
Specify the type of area covered i.e. Total, Land, Water, etc. Supports the use of an external controlled vocabulary.

