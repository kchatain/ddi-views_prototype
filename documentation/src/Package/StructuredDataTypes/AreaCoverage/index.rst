.. _AreaCoverage:


AreaCoverage
************



Definition
==========
Use to specify the area of land, water, total or other area coverage in terms of square miles/kilometers or other measure as part of a Geographic Extent.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst