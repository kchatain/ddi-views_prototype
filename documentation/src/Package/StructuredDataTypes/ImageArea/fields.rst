.. _fields:


ImageArea - Properties and Relationships
****************************************





Properties
==========

=======  ==========  ===========
Name     Type        Cardinality
=======  ==========  ===========
content  String      0..1
shape    ShapeCoded  0..1
=======  ==========  ===========


content
#######
A comma-delimited list of x,y coordinates, listed as a set of adjacent points for rectangles and polygons, and as a center-point and a radius for circles (x,y,r).


shape
#####
A fixed set of valid responses includes Rectangle, Circle, and Polygon.

