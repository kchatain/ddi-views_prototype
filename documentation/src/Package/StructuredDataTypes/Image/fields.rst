.. _fields:


Image - Properties and Relationships
************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
dpi              Integer                            0..1
languageOfImage                                     0..n
typeOfImage      ExternalControlledVocabularyEntry  0..1
uri              anyURI                             0..1
===============  =================================  ===========


dpi
###
Provides the resolution of the image in dots per inch to assist in selecting the appropriate image for various uses.


languageOfImage
###############
Language of image. Supports the indication of multiple languages. Supports use of codes defined by the RFC 1766.


typeOfImage
###########
Brief description of the image type. Supports the use of an external controlled vocabulary.


uri
###
A reference to the location of the image using a URI.

