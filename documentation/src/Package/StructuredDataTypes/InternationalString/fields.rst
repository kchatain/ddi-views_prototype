.. _fields:


InternationalString - Properties and Relationships
**************************************************





Properties
==========

======================  ==========================  ===========
Name                    Type                        Cardinality
======================  ==========================  ===========
languageSpecificString  LanguageSpecificStringType  0..n
======================  ==========================  ===========


languageSpecificString
######################
A non-formatted string of text with an attribute that designates the language of the text. Repeat this object to express the same content in another language.

