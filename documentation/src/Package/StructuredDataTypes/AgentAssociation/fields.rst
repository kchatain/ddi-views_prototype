.. _fields:


AgentAssociation - Properties and Relationships
***********************************************





Properties
==========

=========  =======================================  ===========
Name       Type                                     Cardinality
=========  =======================================  ===========
agentName  BibliographicName                        0..1
role       PairedExternalControlledVocabularyEntry  0..n
=========  =======================================  ===========


agentName
#########
Full name of the contributor. Language equivalents should be expressed within the International String structure.


role
####
The role of the of the Agent within the context of the parent property name with information on the extent to which the role applies. Allows for use of external controlled vocabularies. Reference should be made to the vocabulary within the structure of the role. Recommended role for contributors can be found in the CASRAI Contributor Roles Vocabulary (CRediT) http://dictionary.casrai.org/Contributor_Roles, or the OPENRIF contribution ontology https://github.com/openrif/contribution-ontology




Relationships
=============

===============  =====  ===========
Name             Type   Cardinality
===============  =====  ===========
associatedAgent  Agent  0..n
===============  =====  ===========


associatedAgent
###############
Reference to an agent as described by any object that is a member of the abstract type Agent.



