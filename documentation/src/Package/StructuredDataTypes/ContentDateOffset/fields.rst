.. _fields:


ContentDateOffset - Properties and Relationships
************************************************





Properties
==========

================  =======  ===========
Name              Type     Cardinality
================  =======  ===========
isNegativeOffset  Boolean  0..1
numberOfUnits     Real     0..1
================  =======  ===========


isNegativeOffset
################
If set to "true" the date is offset the number of units specified PRIOR to the default date of the data. A setting of "false" indicates a date the specified number of units in the FUTURE from the default date of the data.


numberOfUnits
#############
The number of units to off-set the date for this item expressed as a decimal.

