.. _Email:


Email
*****



Definition
==========
An e-mail address which conforms to the internet format (RFC 822) including its type and time period for which it is valid.


Synonyms
========
e-mail address; internet email address


Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst