.. _BusinessProcessIndicator:


BusinessProcessIndicator
************************



Definition
==========
Allows for the identification of the BusinessProcess specifically as a member and optionally provides an index for the member within an ordered array.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst