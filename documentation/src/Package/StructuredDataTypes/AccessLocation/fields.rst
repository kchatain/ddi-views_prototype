.. _fields:


AccessLocation - Properties and Relationships
*********************************************





Properties
==========

================  =================================  ===========
Name              Type                               Cardinality
================  =================================  ===========
mimeType          ExternalControlledVocabularyEntry  0..1
physicalLocation  InternationalString                0..n
uri               anyURI                             0..n
================  =================================  ===========


mimeType
########
The mime type. Supports the use of an controlled vocabulary.


physicalLocation
################
The physical location of the machine


uri
###
A URI for access, normally expressed as a URL

