.. _AccessLocation:


AccessLocation
**************



Definition
==========
A set of access information for a Machine including URI, mime type, and physical location


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst