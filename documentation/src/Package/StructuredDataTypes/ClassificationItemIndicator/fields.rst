.. _fields:


ClassificationItemIndicator - Properties and Relationships
**********************************************************





Properties
==========

========  =======  ===========
Name      Type     Cardinality
========  =======  ===========
hasLevel  Integer  0..1
index     Integer  0..1
========  =======  ===========


hasLevel
########
Indicates the level within which the CodeItem resides


index
#####
Provides an index for the member within an ordered array




Relationships
=============

========  ==================  ===========
Name      Type                Cardinality
========  ==================  ===========
member    ClassificationItem  0..n
realizes  MemberIndicator     0..n
========  ==================  ===========


member
######
The member containing the designation of the Classification Item




realizes
########
Collection pattern class realized by this class



