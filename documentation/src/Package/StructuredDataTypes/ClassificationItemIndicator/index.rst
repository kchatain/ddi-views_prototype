.. _ClassificationItemIndicator:


ClassificationItemIndicator
***************************



Definition
==========
A ClassificationItemIndicator realizes and extends a MemberIndicator which provides a Classification Item with an index indicating order and a level reference providing the level location of the Classification Item within a hierarchical structure.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst