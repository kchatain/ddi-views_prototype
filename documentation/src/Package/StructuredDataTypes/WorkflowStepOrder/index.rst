.. _WorkflowStepOrder:


WorkflowStepOrder
*****************



Definition
==========
Associates an sequence ordering number with any WorflowStep or sub-type,


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst