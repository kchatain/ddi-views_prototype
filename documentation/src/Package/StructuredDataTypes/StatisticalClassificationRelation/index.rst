.. _StatisticalClassificationRelation:


StatisticalClassificationRelation
*********************************



Definition
==========
Specifies the Statistical Classifications for the source and target of the complex relationship and defines the relationship.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst