.. _fields:


InternationalStructuredString - Properties and Relationships
************************************************************





Properties
==========

================================  ====================================  ===========
Name                              Type                                  Cardinality
================================  ====================================  ===========
languageSpecificStructuredString  LanguageSpecificStructuredStringType  1..n
================================  ====================================  ===========


languageSpecificStructuredString
################################
Supports the optional use of XHTML formatting tags within the string structure. In addition to the language designation and information regarding translation, the attribute isPlain can be set to true to indicate that the content should be treated as plain unstructured text, including any XHTML formatting tags. Repeat the content element to provide multiple language versions of the same content.

