.. _fields:


PhysicalRecordSegmentRelation - Properties and Relationships
************************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  =====================  ===========
Name      Type                   Cardinality
========  =====================  ===========
realizes  MemberRelation         0..n
source    PhysicalRecordSegment  0..n
target    PhysicalRecordSegment  0..n
========  =====================  ===========


realizes
########
Collection Pattern class realized by this class




source
######
first member in the relationship. Constrained to a single source




target
######
second member in the relationship



