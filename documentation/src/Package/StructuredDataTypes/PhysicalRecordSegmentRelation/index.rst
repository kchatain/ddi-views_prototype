.. _PhysicalRecordSegmentRelation:


PhysicalRecordSegmentRelation
*****************************



Definition
==========
Defines structured relationship between PhysicalRecordSegments.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst