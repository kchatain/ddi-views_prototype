.. _fields:


Form - Properties and Relationships
***********************************





Properties
==========

==========  ===================  ===========
Name        Type                 Cardinality
==========  ===================  ===========
formNumber  String               0..1
isRequired  Boolean              0..1
statement   InternationalString  0..1
uri         anyURI               0..1
==========  ===================  ===========


formNumber
##########
The number or other means of identifying the form.


isRequired
##########
Set to "true" if the form is required. Set to "false" if the form is optional.


statement
#########
A statement regarding the use, coverage, and purpose of the form.


uri
###
The URN or URL of the form.

