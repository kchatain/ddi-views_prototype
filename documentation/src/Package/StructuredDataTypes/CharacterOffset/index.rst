.. _CharacterOffset:


CharacterOffset
***************



Definition
==========
Specification of the character offset for the beginning and end of the segment, or beginning and length.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst