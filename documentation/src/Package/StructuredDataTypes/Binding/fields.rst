.. _fields:


Binding - Properties and Relationships
**************************************





Relationships
=============

=============  =========  ===========
Name           Type       Cardinality
=============  =========  ===========
fromParameter  Parameter  0..n
toParameter    Parameter  0..n
=============  =========  ===========


fromParameter
#############
Parameter supplying the data




toParameter
###########
Parameter accepting the data as input to the process step



