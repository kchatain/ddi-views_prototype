.. _Binding:


Binding
*******



Definition
==========
Binds two parameters together to direct flow of data through a process


Synonyms
========



Explanatory notes
=================
Binding is used to define the flow of data into, out of, and within a process. It is separate from the flow of a process. When used in the workflow of a data capture process most data may go from a capture to an instance variable, but when needed as a check sum in a loop, a recoding process, or conditional content for DynamicText, Binding provides the means for explicitly directing the movement of data from one point to another in the process.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst