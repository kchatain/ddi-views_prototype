.. _ValueMappingRelation:


ValueMappingRelation
********************



Definition
==========
Variable relationships in PhysicalLayout as defined using ValueMappingRelation


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst