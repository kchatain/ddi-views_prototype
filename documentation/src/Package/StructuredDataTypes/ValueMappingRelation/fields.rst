.. _fields:


ValueMappingRelation - Properties and Relationships
***************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  ==============  ===========
Name      Type            Cardinality
========  ==============  ===========
realizes  MemberRelation  0..n
source    ValueMapping    0..n
target    ValueMapping    0..n
========  ==============  ===========


realizes
########
Class of the Collection pattern realized by this class.




source
######
Specialization of source to VariableMapping. The First Variableapping in the pair.




target
######
Specialization of target to VariableMapping. Restricts cardinality. The following VariableMapping in the pair.



