.. _examples:


LanguageSpecificStructuredStringType - Examples
***********************************************


The content: <p>a bit of text</p> <p>with some specialties <span class="highlight">highlighted</span> </p> Would be entered as: &lt;p&gt;with some specialties &lt;span class=&quot;highlight&quot;&gt;highlighted&lt;/span&gt; &lt;/p&gt;