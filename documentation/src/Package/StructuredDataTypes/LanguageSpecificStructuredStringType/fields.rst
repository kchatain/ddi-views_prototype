.. _fields:


LanguageSpecificStructuredStringType - Properties and Relationships
*******************************************************************





Properties
==========

=============  ===================  ===========
Name           Type                 Cardinality
=============  ===================  ===========
isPlainText    Boolean              0..1
otherDefined   String               0..1
structureUsed  StringStructureType  0..1
=============  ===================  ===========


isPlainText
###########
Indicates that the content is to be treated as plain text (no formatting). You may use DDIProfile to fix the value of this attribute to 'true' in cases where you wish to indicate that your system treats all content should be treated as plain text.


otherDefined
############
If the value of structureUsed is Other enter the structue type.


structureUsed
#############
Uses a enumerated value to specify the structuring elements used. 

