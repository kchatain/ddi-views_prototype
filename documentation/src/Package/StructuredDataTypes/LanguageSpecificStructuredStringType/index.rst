.. _LanguageSpecificStructuredStringType:


LanguageSpecificStructuredStringType
************************************


Extends
=======
:ref:`LanguageSpecificStringType`


Definition
==========
Supports the optional use of formatting tags expressed with escape characters within the string structure. Language of the string is defined by the attribute language. Scope of the string is defined by scope. Language and Scope are used to filter selection and use of content based on unique combinations of these two properties. The content can be identified as translated (isTranslated), subject to translation (isTranslatable), the result of translation from one or more languages (translationSourceLanguages), carry an indication whether or not it should be treated as plain text (isPlain), identification of the structure used (structureUsed) which is an internal enumeration, and the ability to identify a structure not available in the enumeration by declaring structureUsed="Other" and specifying the other structure (otherDefined).


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst