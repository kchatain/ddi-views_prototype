.. _fields:


ReferenceDate - Properties and Relationships
********************************************





Properties
==========

=======  =================================  ===========
Name     Type                               Cardinality
=======  =================================  ===========
keyword  ExternalControlledVocabularyEntry  0..n
subject  ExternalControlledVocabularyEntry  0..n
=======  =================================  ===========


keyword
#######
If the date is for a subset of data only such as a referent date for residence 5 years ago, use keyword to specify the coverage of the data this date applies to. May be repeated to reflect multiple keywords.


subject
#######
If the date is for a subset of data only such as a referent date for residence 5 years ago, use Subject to specify the coverage of the data this date applies to. May be repeated to reflect multiple subjects.

