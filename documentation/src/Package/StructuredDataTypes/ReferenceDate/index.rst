.. _ReferenceDate:


ReferenceDate
*************


Extends
=======
:ref:`AnnotationDate`


Definition
==========
The date covered by the annotated object. In addition to specifying a type of date (e.g. collection period, census year, etc.) the date or time span may be associated with a particular subject or keyword. This allows for the expression of a referent date associated with specific subjects or keywords.


Synonyms
========



Explanatory notes
=================
Note that if needed you may use Start and Duration or Duration and End. These options may not be recognizable by all systems.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst