.. _examples:


ReferenceDate - Examples
************************


A set of date items on income and labor force status may have a referent date for the year prior to the collection date. To express a duration the preference is for Start and End dates expressing two time points separated by a "/".