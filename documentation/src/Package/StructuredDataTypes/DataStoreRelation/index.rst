.. _DataStoreRelation:


DataStoreRelation
*****************



Definition
==========
Defines the complex relationship between 2 or more DataStores in a DataLibrary


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst