.. _ClassificationItemRelation:


ClassificationItemRelation
**************************



Definition
==========
Source target relationship between classification items in a classification item relation structure


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst