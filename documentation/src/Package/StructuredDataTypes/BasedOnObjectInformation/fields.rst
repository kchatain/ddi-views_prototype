.. _fields:


BasedOnObjectInformation - Properties and Relationships
*******************************************************





Properties
==========

===========================  =================================  ===========
Name                         Type                               Cardinality
===========================  =================================  ===========
basedOnRationaleCode         ExternalControlledVocabularyEntry  0..1
basedOnRationaleDescription  InternationalString                0..1
===========================  =================================  ===========


basedOnRationaleCode
####################
RationaleCode is primarily for internal processing flags within an organization or system. Supports the use of an external controlled vocabulary.


basedOnRationaleDescription
###########################
Textual description of the rationale/purpose for the based on reference to inform users as to the extent and implication of the version change. May be expressed in multiple languages.




Relationships
=============

=======  ============  ===========
Name     Type          Cardinality
=======  ============  ===========
basedOn  Identifiable  0..n
=======  ============  ===========


basedOn
#######
The identification for the object upon which the current object is based.



