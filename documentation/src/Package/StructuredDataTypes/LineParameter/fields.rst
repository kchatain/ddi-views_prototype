.. _fields:


LineParameter - Properties and Relationships
********************************************





Properties
==========

===========  =======  ===========
Name         Type     Cardinality
===========  =======  ===========
endLine      Integer  0..1
endOffset    Integer  0..1
startLine    Integer  0..1
startOffset  Integer  0..1
===========  =======  ===========


endLine
#######
Number of lines from beginning of the document.


endOffset
#########
Number of characters from the start of the line specified in EndLine.


startLine
#########
Number of lines from beginning of the document.


startOffset
###########
Number of characters from start of the line specified in StartLine.

