.. _fields:


InternationalIdentifier - Properties and Relationships
******************************************************





Properties
==========

=================  =================================  ===========
Name               Type                               Cardinality
=================  =================================  ===========
identifierContent  String                             0..1
isURI              Boolean                            0..1
managingAgency     ExternalControlledVocabularyEntry  0..1
=================  =================================  ===========


identifierContent
#################
An identifier as it should be listed for identification purposes.


isURI
#####
Set to "true" if Identifier is a URI


managingAgency
##############
The identification of the Agency which assigns and manages the identifier, i.e., ISBN, ISSN, DOI, etc.

