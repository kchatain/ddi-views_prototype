.. _GeographicUnitTypeIndicator:


GeographicUnitTypeIndicator
***************************



Definition
==========
Provides ability to declare an optional sequence or index order to a Unit Type describing a Geographic Unit Type. Extended to support membership in a specific level.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst