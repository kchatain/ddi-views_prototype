.. _WebLink:


WebLink
*******



Definition
==========
A web site (normally a URL) with information on type of site, privacy flag, and effective dates.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst