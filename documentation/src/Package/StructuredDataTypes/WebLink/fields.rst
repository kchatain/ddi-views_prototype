.. _fields:


WebLink - Properties and Relationships
**************************************





Properties
==========

==============  =================================  ===========
Name            Type                               Cardinality
==============  =================================  ===========
effectiveDates  DateRange                          0..1
isPreferred     Boolean                            0..1
privacy         ExternalControlledVocabularyEntry  0..1
typeOfWebsite   ExternalControlledVocabularyEntry  0..1
uri             anyURI                             0..1
==============  =================================  ===========


effectiveDates
##############
The period for which this URL is valid.


isPreferred
###########
Set to "true" if this is the preferred URL.


privacy
#######
Indicates the privacy level of this URL


typeOfWebsite
#############
The type of URL for example personal, project, organization, division, etc.


uri
###
Normally a URL

