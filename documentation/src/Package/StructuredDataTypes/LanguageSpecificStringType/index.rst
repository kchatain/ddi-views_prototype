.. _LanguageSpecificStringType:


LanguageSpecificStringType
**************************



Definition
==========
Allows for non-formatted language specific strings that may be translations from other languages, or that may be translatable into other languages. Only one string per language/scope type is allowed. LanguageSpecificString contains the following attributes, xmlang to designate the language, isTranslated with a default value of false to designate if an object is a translation of another language, isTranslatable with a default value of true to designate if the content can be translated, translationSourceLanguage to indicate the source languages used in creating this translation, translationDate, and scope which can be used to define intended audience or use such as internal, external, etc.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst