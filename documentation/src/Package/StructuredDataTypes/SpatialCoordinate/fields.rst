.. _fields:


SpatialCoordinate - Properties and Relationships
************************************************





Properties
==========

==============  ===========  ===========
Name            Type         Cardinality
==============  ===========  ===========
content         String       0..1
coordinateType  PointFormat  0..1
==============  ===========  ===========


content
#######
The value of the coordinate expressed as a string.


coordinateType
##############
Identifies the type of point coordinate system using a controlled vocabulary. Point formats include decimal degree, degrees minutes seconds, decimal minutes, meters, and feet.

