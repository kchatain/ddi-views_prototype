*******************
StructuredDataTypes
*******************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AccessLocation/index.rst
   Address/index.rst
   AgentAssociation/index.rst
   AgentId/index.rst
   AgentIndicator/index.rst
   AgentRelation/index.rst
   Annotation/index.rst
   AnnotationDate/index.rst
   AreaCoverage/index.rst
   AudioSegment/index.rst
   BasedOnObjectInformation/index.rst
   BibliographicName/index.rst
   Binding/index.rst
   BusinessProcessCondition/index.rst
   BusinessProcessIndicator/index.rst
   CategoryIndicator/index.rst
   CategoryRelation/index.rst
   CategoryStatistic/index.rst
   CharacterOffset/index.rst
   ClassificationIndexEntryIndicator/index.rst
   ClassificationItemIndicator/index.rst
   ClassificationItemRelation/index.rst
   ClassificationSeriesIndicator/index.rst
   ClassificationSeriesRelation/index.rst
   CodeIndicator/index.rst
   CodeRelation/index.rst
   Command/index.rst
   CommandCode/index.rst
   CommandFile/index.rst
   ConceptIndicator/index.rst
   ConceptRelation/index.rst
   ConditionalText/index.rst
   ContactInformation/index.rst
   ContentDateOffset/index.rst
   CorrespondenceType/index.rst
   CustomItemIndicator/index.rst
   CustomItemRelation/index.rst
   CustomValueIndicator/index.rst
   DataPointIndicator/index.rst
   DataPointRelation/index.rst
   DataStoreIndicator/index.rst
   DataStoreRelation/index.rst
   Date/index.rst
   DateRange/index.rst
   DoubleNumberRangeValue/index.rst
   DynamicText/index.rst
   DynamicTextContent/index.rst
   ElectronicMessageSystem/index.rst
   ElseIfAction/index.rst
   Email/index.rst
   ExternalControlledVocabularyEntry/index.rst
   FixedText/index.rst
   Form/index.rst
   GeographicUnitIndicator/index.rst
   GeographicUnitRelation/index.rst
   GeographicUnitTypeIndicator/index.rst
   GeographicUnitTypeRelation/index.rst
   Image/index.rst
   ImageArea/index.rst
   IndexEntryRelation/index.rst
   IndividualName/index.rst
   InstanceVariableIndicator/index.rst
   InstanceVariableRelation/index.rst
   InstanceVariableValueMap/index.rst
   InternationalIdentifier/index.rst
   InternationalString/index.rst
   InternationalStructuredString/index.rst
   LabelForDisplay/index.rst
   LanguageSpecificStringType/index.rst
   LanguageSpecificStructuredStringType/index.rst
   Level/index.rst
   LineParameter/index.rst
   LiteralText/index.rst
   LocalIdFormat/index.rst
   LocationName/index.rst
   LogicalRecordIndicator/index.rst
   LogicalRecordRelation/index.rst
   Map/index.rst
   NonIsoDateType/index.rst
   NumberRange/index.rst
   NumberRangeValue/index.rst
   ObjectName/index.rst
   OrganizationName/index.rst
   PairedExternalControlledVocabularyEntry/index.rst
   PhysicalRecordSegmentIndicator/index.rst
   PhysicalRecordSegmentRelation/index.rst
   Polygon/index.rst
   PrivateImage/index.rst
   RangeValue/index.rst
   RationaleDefinition/index.rst
   ReferenceDate/index.rst
   ResourceIdentifier/index.rst
   Segment/index.rst
   SpatialCoordinate/index.rst
   SpatialLine/index.rst
   SpatialPoint/index.rst
   SpatialRelationship/index.rst
   Statistic/index.rst
   StatisticalClassificationIndicator/index.rst
   StatisticalClassificationRelation/index.rst
   StudyIndicator/index.rst
   StudyRelation/index.rst
   SummaryStatistic/index.rst
   TargetSample/index.rst
   Telephone/index.rst
   TextualSegment/index.rst
   TypedDescriptiveText/index.rst
   TypedString/index.rst
   ValueMappingIndicator/index.rst
   ValueMappingRelation/index.rst
   ValueString/index.rst
   VariableIndicator/index.rst
   VariableRelation/index.rst
   VideoSegment/index.rst
   VocabularyEntryIndicator/index.rst
   VocabularyEntryRelation/index.rst
   WebLink/index.rst
   WorkflowStepOrder/index.rst



Graph
=====

.. graphviz:: /images/graph/StructuredDataTypes/StructuredDataTypes.dot