.. _fields:


ObjectName - Properties and Relationships
*****************************************





Properties
==========

=======  =================================  ===========
Name     Type                               Cardinality
=======  =================================  ===========
content  String                             0..1
context  ExternalControlledVocabularyEntry  0..1
=======  =================================  ===========


content
#######
The expressed name of the object.


context
#######
A name may be specific to a particular context, i.e., a type of software, or a section of a registry. Identify the context related to the specified name. 

