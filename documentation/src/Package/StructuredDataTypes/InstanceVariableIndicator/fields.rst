.. _fields:


InstanceVariableIndicator - Properties and Relationships
********************************************************





Properties
==========

=====  =======  ===========
Name   Type     Cardinality
=====  =======  ===========
index  Integer  0..1
=====  =======  ===========


index
#####
Provides an index for the member within an ordered array




Relationships
=============

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
member    InstanceVariable  0..n
realizes  MemberIndicator   0..n
========  ================  ===========


member
######
Member of the collection




realizes
########
Pattern class realized by this class



