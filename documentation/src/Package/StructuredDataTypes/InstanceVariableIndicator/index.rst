.. _InstanceVariableIndicator:


InstanceVariableIndicator
*************************



Definition
==========
Allows for the identification of the InstanceVariable specifically as a member and optionally provides an index for the member within an ordered array.


Synonyms
========



Explanatory notes
=================
Note if multiple types of Variables may be included in a collection use VariableIndicator.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst