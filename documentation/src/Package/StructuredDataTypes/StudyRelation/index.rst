.. _StudyRelation:


StudyRelation
*************



Definition
==========
Relation specification between source and target studies in a relation structure


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst