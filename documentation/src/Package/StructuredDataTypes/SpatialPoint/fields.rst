.. _fields:


SpatialPoint - Properties and Relationships
*******************************************





Properties
==========

===========  =================  ===========
Name         Type               Cardinality
===========  =================  ===========
xCoordinate  SpatialCoordinate  0..1
yCoordinate  SpatialCoordinate  0..1
===========  =================  ===========


xCoordinate
###########
An X coordinate (latitudinal equivalent) value and format expressed using the Spatial Coordinate structure.


yCoordinate
###########
A Y coordinate (longitudinal equivalent) value and format expressed using the Spatial Coordinate structure.

