.. _SpatialPoint:


SpatialPoint
************



Definition
==========
A geographic point consisting of an X and Y coordinate. Each coordinate value is expressed separately providing its value and format.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst