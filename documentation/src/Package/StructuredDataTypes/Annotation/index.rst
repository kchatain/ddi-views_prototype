.. _Annotation:


Annotation
**********



Definition
==========
Provides annotation information on the object to support citation and crediting of the creator(s) of the object.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst