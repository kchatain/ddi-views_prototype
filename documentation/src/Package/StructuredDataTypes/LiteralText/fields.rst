.. _fields:


LiteralText - Properties and Relationships
******************************************





Properties
==========

====  ====================================  ===========
Name  Type                                  Cardinality
====  ====================================  ===========
text  LanguageSpecificStructuredStringType  0..1
====  ====================================  ===========


text
####
The value of the static text string. Supports the optional use of XHTML formatting tags within the string structure. If the content of a literal text contains more than one language, i.e. "What is your understanding of the German word 'Gesundheit'?", the foreign language element should be placed in a separate LiteralText component with the appropriate xmlang value and, in this case, isTranslatable set to "false". If the existence of white space is critical to the understanding of the content (such as inclusion of a leading or trailing white space), set the attribute of Text xml:space to "preserve".

