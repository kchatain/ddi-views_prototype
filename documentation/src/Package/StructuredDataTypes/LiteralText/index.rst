.. _LiteralText:


LiteralText
***********


Extends
=======
:ref:`DynamicTextContent`


Definition
==========
Literal (static) text to be used in an instrument. The value is a LanguageSpecificStruccturedString with the addition of an attribute allowing for the specification that white space is to be preserved.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst