.. _fields:


AudioSegment - Properties and Relationships
*******************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
audioClipBegin   String                             0..1
audioClipEnd     String                             0..1
typeOfAudioClip  ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


audioClipBegin
##############
The point to begin the audio clip. If no point is provided the assumption is that the start point is the beginning of the clip provided.


audioClipEnd
############
The point to end the audio clip. If no point is provided the assumption is that the end point is the end of the clip provided.


typeOfAudioClip
###############
The type of audio clip provided. Supports the use of a controlled vocabulary.

