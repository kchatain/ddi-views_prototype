.. _fields:


RationaleDefinition - Properties and Relationships
**************************************************





Properties
==========

====================  =================================  ===========
Name                  Type                               Cardinality
====================  =================================  ===========
rationaleCode         ExternalControlledVocabularyEntry  0..1
rationaleDescription  InternationalString                0..1
====================  =================================  ===========


rationaleCode
#############
RationaleCode is primarily for internal processing flags within an organization or system. Supports the use of an external controlled vocabulary.


rationaleDescription
####################
Textual description of the rationale/purpose for the version change to inform users as to the extent and implication of the version change. May be expressed in multiple languages.

