.. _NonIsoDateType:


NonIsoDateType
**************



Definition
==========
Used to preserve an historical date, formatted in a non-ISO fashion.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst