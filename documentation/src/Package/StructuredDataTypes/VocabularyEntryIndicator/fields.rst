.. _fields:


VocabularyEntryIndicator - Properties and Relationships
*******************************************************





Properties
==========

=====  =======  ===========
Name   Type     Cardinality
=====  =======  ===========
index  Integer  0..1
=====  =======  ===========


index
#####
Index value of member in an ordered array




Relationships
=============

========  ===============  ===========
Name      Type             Cardinality
========  ===============  ===========
member    VocabularyEntry  0..n
realizes  MemberIndicator  0..n
========  ===============  ===========


member
######
Restricts member target class to VocabularyEntry




realizes
########
Collection pattern class realized by this class



