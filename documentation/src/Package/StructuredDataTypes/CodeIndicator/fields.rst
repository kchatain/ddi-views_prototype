.. _fields:


CodeIndicator - Properties and Relationships
********************************************





Properties
==========

=========  =======  ===========
Name       Type     Cardinality
=========  =======  ===========
index      Integer  0..1
isInLevel  Integer  0..1
=========  =======  ===========


index
#####
Provides an index for the member within an ordered array


isInLevel
#########
Indicates the level within which the CodeItem resides




Relationships
=============

========  ===============  ===========
Name      Type             Cardinality
========  ===============  ===========
member    Code             0..n
realizes  MemberIndicator  0..n
========  ===============  ===========


member
######
The member containing the designation of the CodeItem




realizes
########
The Collection class realized by this class



