.. _Statistic:


Statistic
*********



Definition
==========
The value of the statistic expressed as an xs:decimal and/or xs:double. Indicates whether it is weighted value and the computation base.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst