.. _AnnotationDate:


AnnotationDate
**************


Extends
=======
:ref:`Date`


Definition
==========
A generic date type for use in Annotation which provides the standard date structure plus a property to define the date type (Publication date, Accepted date, Copyrighted date, Submitted date, etc.).


Synonyms
========
Equivalent of http://purl.org/dc/elements/1.1/date where the type of date may identify the Dublin Core refinement term.


Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst