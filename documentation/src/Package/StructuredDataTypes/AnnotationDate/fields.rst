.. _fields:


AnnotationDate - Properties and Relationships
*********************************************





Properties
==========

==========  =================================  ===========
Name        Type                               Cardinality
==========  =================================  ===========
typeOfDate  ExternalControlledVocabularyEntry  0..n
==========  =================================  ===========


typeOfDate
##########
Use to specify the type of date. This may reflect the refinements of dc:date such as dateAccepted, dateCopyrighted, dateSubmitted, etc.

