.. _fields:


CategoryStatistic - Properties and Relationships
************************************************





Properties
==========

=======================  =================================  ===========
Name                     Type                               Cardinality
=======================  =================================  ===========
categoryValue            ValueString                        0..1
filterValue              ValueString                        0..1
hasStatistic             Statistic                          0..n
typeOfCategoryStatistic  ExternalControlledVocabularyEntry  0..1
=======================  =================================  ===========


categoryValue
#############
Value of the category to which the statistics apply


filterValue
###########
The value of the filter variable to which the category statistic is restricted


hasStatistic
############
The value of the identified type of statistic for the category. May be repeated to provide unweighted or weighted values and different computation bases.


typeOfCategoryStatistic
#######################
Type of category statistic. Supports the use of an external controlled vocabulary. DDI strongly recommends the use of a controlled vocabulary.




Relationships
=============

===========  ===========  ===========
Name         Type         Cardinality
===========  ===========  ===========
forCodeItem  Designation  0..n
===========  ===========  ===========


forCodeItem
###########
For categorical representations the CodeItem describing the value (signifier) and category (signified) may be identified.



