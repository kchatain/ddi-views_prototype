.. _fields:


Command - Properties and Relationships
**************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
commandContent   String                             0..1
programLanguage  ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


commandContent
##############
Content of the command itself expressed in the language designated in Programming Language.


programLanguage
###############
Designates the programming language used for the command. Supports the use of a controlled vocabulary.

