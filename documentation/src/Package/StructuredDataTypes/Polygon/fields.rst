.. _fields:


Polygon - Properties and Relationships
**************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
point            SpatialPoint                       4..n
polygonLinkCode  String                             0..1
shapeFileFormat  ExternalControlledVocabularyEntry  0..1
uri              anyURI                             0..1
===============  =================================  ===========


point
#####
A geographic point defined by a latitude and longitude. A minimum of 4 points is required as the first and last point should be identical in order to close the polygon. Note that a triangle has three sides and requires 3 unique points plus a fourth point replicating the first point in order to close the polygon.


polygonLinkCode
###############
The PolygonLinkCode is the identifier of the specific polygon within the file. For example in an NHGIS file the LinkCodeForPolygon for Tract 101.01 in Hennepin County in Minnesota is 2700530010101.


shapeFileFormat
###############
The format of the shape file existing at the location indicated by the sibling ExternalURI element.


uri
###
A URI for the boundary file containing the polygon

