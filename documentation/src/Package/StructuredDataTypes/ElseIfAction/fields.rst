.. _fields:


ElseIfAction - Properties and Relationships
*******************************************





Properties
==========

=========  ===========  ===========
Name       Type         Cardinality
=========  ===========  ===========
condition  CommandCode  0..1
=========  ===========  ===========


condition
#########
The condition to verify if the parent IfThenElse condition is false.




Relationships
=============

========  ====================  ===========
Name      Type                  Cardinality
========  ====================  ===========
executes  WorkflowStepSequence  0..n
========  ====================  ===========


executes
########
The WorkflowStepSequence to execute if the condition is true



