.. _InstanceVariableValueMap:


InstanceVariableValueMap
************************



Definition
==========
A key value relationship for two or more logical records where the key is one or more equivalent instance variables and the value is a defined relationship or a relationship to a set value. Correspondence type refers to the variables themselves rather than the value of the variables concerned. In this context Correspondence type will normally be set to ExactMatch.


Synonyms
========



Explanatory notes
=================
Logical Records: Household and Person, Key: Household ID (HHID in Household Record, HHIDP in Person Record), ValueRelaitonship: Equal, Set Value: n.a.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst