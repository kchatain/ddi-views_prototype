.. _fields:


InstanceVariableValueMap - Properties and Relationships
*******************************************************





Properties
==========

=====================  =====================  ===========
Name                   Type                   Cardinality
=====================  =====================  ===========
hasCorrespondenceType  CorrespondenceType     1..1
setValue               ValueString            0..1
valueRelationship      ValueRelationshipType  1..1
=====================  =====================  ===========


hasCorrespondenceType
#####################
Describes the relationship between the source and target members using both controlled vocabularies and descriptive text. In this context the correspondence refers to the two instance variables, not their value. The relationship would normally be exactMatch.


setValue
########
A set value for the key source Instance Variables


valueRelationship
#################
Relationship between the source and target InstanceVariables or to the setValue if provided




Relationships
=============

========  ================  ===========
Name      Type              Cardinality
========  ================  ===========
realizes  ComparisonMap     0..n
source    InstanceVariable  0..n
target    InstanceVariable  0..n
========  ================  ===========


realizes
########
Collections pattern class realized by this class




source
######
The source Intance Variable for the relationship.  




target
######
Target Instance Variables if a directional relation is used



