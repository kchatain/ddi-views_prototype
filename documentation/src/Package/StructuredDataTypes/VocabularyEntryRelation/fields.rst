.. _fields:


VocabularyEntryRelation - Properties and Relationships
******************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list


semantic
########
Provide a semantic that provides a context for the relationship using and External Controlled Vocabulary


totality
########
Type of relation in terms of its totality using an enumeration list.




Relationships
=============

========  ===============  ===========
Name      Type             Cardinality
========  ===============  ===========
realizes  MemberRelation   0..n
source    VocabularyEntry  0..n
target    VocabularyEntry  0..n
========  ===============  ===========


realizes
########
realizes pattern MemberRelation




source
######
Restricts source to VocabularyEntry




target
######
Restricts target to VocabularyEntry and cardinality.



