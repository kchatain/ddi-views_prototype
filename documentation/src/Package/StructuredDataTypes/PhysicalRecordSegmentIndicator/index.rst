.. _PhysicalRecordSegmentIndicator:


PhysicalRecordSegmentIndicator
******************************



Definition
==========
Member Indicator for use with member type PhysicalRecordSegment


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst