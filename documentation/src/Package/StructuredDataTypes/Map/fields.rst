.. _fields:


Map - Properties and Relationships
**********************************





Properties
==========

=====================  =============================  ===========
Name                   Type                           Cardinality
=====================  =============================  ===========
displayLabel           LabelForDisplay                0..1
hasCorrespondenceType  CorrespondenceType             0..1
usage                  InternationalStructuredString  0..1
validDates             DateRange                      0..1
=====================  =============================  ===========


displayLabel
############
A display label for the OrderedMemberCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


hasCorrespondenceType
#####################
Type of correspondence in terms of commonalities and differences between two members.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.


validDates
##########
Date from which the Map became valid. The date must be defined if the Map belongs to a floating CorrespondenceTable. Date at which the Map became invalid. The date must be defined if the Map belongs to a floating Correspondence Table and is no longer valid.




Relationships
=============

========  =============  ===========
Name      Type           Cardinality
========  =============  ===========
realizes  ComparisonMap  0..n
source    Concept        0..n
target    Concept        0..n
========  =============  ===========


realizes
########
Class of the Collection pattern realized by this class. 




source
######
Concept or any subtype of concepts




target
######
Concept or any subtype of concept



