.. _Map:


Map
***



Definition
==========
Describes the correspondence between concepts in a correspondence table related to one or more Statistical Classifications.


Synonyms
========



Explanatory notes
=================
A Map is the pairing of similar Concepts. Each Concept in the Map belongs to a different Collection. The collection of maps for all the Concepts in corresponding Collections is a Correspondence Table. A simple example might map the following 2 martial status category sets: MS1 - single married widowed divorced MS2 - single married So, a correspondence table between these 2 category sets might look like this: MS1 MS2 single single widowed " divorced " married married


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst