.. _TextualSegment:


TextualSegment
**************



Definition
==========
Defines the segment of textual content used by the parent object. Can identify a set of lines and or characters used to define the segment.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst