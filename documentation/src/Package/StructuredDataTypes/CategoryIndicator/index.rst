.. _CategoryIndicator:


CategoryIndicator
*****************



Definition
==========
Member Indicator for use when the member type is restricted to Category.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst