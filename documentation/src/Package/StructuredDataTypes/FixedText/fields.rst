.. _fields:


FixedText - Properties and Relationships
****************************************





Properties
==========

==========  ==============  ===========
Name        Type            Cardinality
==========  ==============  ===========
whiteSpace  WhiteSpaceRule  0..1
==========  ==============  ===========


whiteSpace
##########
The default setting states that leading and trailing white space will be removed and multiple adjacent white spaces will be treated as a single white space. If the existance of any of these white spaces is critical to the understanding of the content, change the value of this attribute to "preserve".

