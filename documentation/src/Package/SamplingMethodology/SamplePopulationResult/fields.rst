.. _fields:


SamplePopulationResult - Properties and Relationships
*****************************************************





Properties
==========

============  =======  ===========
Name          Type     Cardinality
============  =======  ===========
sampleDate    Date     0..1
sizeOfSample  Integer  0..1
strataName    String   0..1
============  =======  ===========


sampleDate
##########
The date the sample was selected from the sample frame


sizeOfSample
############
The overall size of the sample


strataName
##########
The name or number of the specific strata if the sample frame has been stratified prior to sampling within the specified stage




Relationships
=============

=================  ==========  ===========
Name               Type        Cardinality
=================  ==========  ===========
populationSampled  Population  0..n
sampleUnitType     UnitType    0..n
=================  ==========  ===========


populationSampled
#################
The population represented by the sample




sampleUnitType
##############
The Unit Type of the units within the sample.



