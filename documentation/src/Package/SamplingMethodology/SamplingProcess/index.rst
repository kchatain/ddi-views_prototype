.. _SamplingProcess:


SamplingProcess
***************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Provides the details of the process used to apply the sampling design and obtain the sample. Sampling Process contains a Workflow Step which provides the constituents of a Workflow. It can be a composition (a set of Control Constructs) or atomic (an Act) and may be performed by a service. This allows the use of existing Workflow models to express various sampling process actions. Sampling Frames and Sample Population can be used as designated Inputs or Outputs using Parameters.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst