*******************
SamplingMethodology
*******************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   SampleFrame/index.rst
   SamplePopulationResult/index.rst
   SamplingAlgorithm/index.rst
   SamplingDesign/index.rst
   SamplingGoal/index.rst
   SamplingProcedure/index.rst
   SamplingProcess/index.rst



Graph
=====

.. graphviz:: /images/graph/SamplingMethodology/SamplingMethodology.dot