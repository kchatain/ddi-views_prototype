.. _SampleFrame:


SampleFrame
***********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A set of information used to identify a sample population within a larger population. The sample frame will contain identifying information for individual units within the frame as well as characteristics which will support analysis and the division of the frame population into sub-groups.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst