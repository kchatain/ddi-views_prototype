.. _fields:


SamplingGoal - Properties and Relationships
*******************************************





Properties
==========

==========================  ============  ===========
Name                        Type          Cardinality
==========================  ============  ===========
overallTargetSamplePercent  Real          0..1
overallTargetSampleSize     Integer       0..1
targetSampleSize            TargetSample  0..n
==========================  ============  ===========


overallTargetSamplePercent
##########################
The goal for the overall size of the sample expressed as a decimal (i.e. 70% entered as .7). 


overallTargetSampleSize
#######################
The goal for the overall size of the sample expressed as an integer.


targetSampleSize
################
Repeat for primary and secondary target samples. Supports identification of size, percent, unit type, and universe

