.. _SamplingGoal:


SamplingGoal
************


Extends
=======
:ref:`Goal`


Definition
==========
Provides general description and specific targets for sample sizes including sub-populations.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst