.. _fields:


ConceptSystemCorrespondence - Properties and Relationships
**********************************************************





Properties
==========

==============  =============================  ===========
Name            Type                           Cardinality
==============  =============================  ===========
correspondence  Map                            0..n
displayLabel    LabelForDisplay                0..n
purpose         InternationalStructuredString  0..1
usage           InternationalStructuredString  0..1
==============  =============================  ===========


correspondence
##############
Realization of correspondence in Comparison for mapping similar concepts.


displayLabel
############
A display label for the CollectionCorrespondence. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

========  =============  ===========
Name      Type           Cardinality
========  =============  ===========
maps      ConceptSystem  0..n
realizes  Comparison     0..n
========  =============  ===========


maps
####
Realization of structures in Symmetric Relation. When Concepts of a single ConceptSystem are mapped, the Concept has to appear twice as target.




realizes
########
Class in the Collections Pattern realized by this class.



