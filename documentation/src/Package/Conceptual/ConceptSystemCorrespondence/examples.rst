.. _examples:


ConceptSystemCorrespondence - Examples
**************************************


Correspondence between the concepts used to define the populations in the censuses of two countries with similarity mapping of Concepts "Resident Population", "Labor Force", "Housing Unit", etc.