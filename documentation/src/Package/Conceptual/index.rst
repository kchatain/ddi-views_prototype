**********
Conceptual
**********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Category/index.rst
   CategoryRelationStructure/index.rst
   CategorySet/index.rst
   Concept/index.rst
   ConceptRelationStructure/index.rst
   ConceptSystem/index.rst
   ConceptSystemCorrespondence/index.rst
   ConceptualDomain/index.rst
   ConceptualVariable/index.rst
   InstanceVariable/index.rst
   Population/index.rst
   RepresentedVariable/index.rst
   SentinelConceptualDomain/index.rst
   SubstantiveConceptualDomain/index.rst
   Unit/index.rst
   UnitType/index.rst
   Universe/index.rst
   VariableCollection/index.rst
   VariableRelationStructure/index.rst



Graph
=====

.. graphviz:: /images/graph/Conceptual/Conceptual.dot