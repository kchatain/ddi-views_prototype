.. _UnitType:


UnitType
********


Extends
=======
:ref:`Concept`


Definition
==========
A Unit Type is a type or class of objects of interest.


Synonyms
========
Object Class [ISO11179]


Explanatory notes
=================
UnitType is the most general in the hierarchy of UnitType, Universe, and Population. It is a description of the basic characteristic for a general set of Units. A Universe is a set of entities defined by a more narrow specification than that of an underlying UnitType. A Population further narrows the specification to a specific time and geography. A Unit Type is used to describe a class or group of Units based on a single characteristic with no specification of time and geography. For example, the Unit Type of “Person” groups together a set of Units based on the characteristic that they are ‘Persons’. It concerns not only Unit Types used in dissemination, but anywhere in the statistical process. E.g. using administrative data might involve the use of a fiscal unit. [GSIM 1.1]


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst