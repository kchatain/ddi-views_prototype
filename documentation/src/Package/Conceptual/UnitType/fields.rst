.. _fields:


UnitType - Properties and Relationships
***************************************





Properties
==========

===============  =============================  ===========
Name             Type                           Cardinality
===============  =============================  ===========
descriptiveText  InternationalStructuredString  0..1
===============  =============================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.




Relationships
=============

===========  =======  ===========
Name         Type     Cardinality
===========  =======  ===========
usesConcept  Concept  0..n
===========  =======  ===========


usesConcept
###########
Reference to the Concept that is being used



