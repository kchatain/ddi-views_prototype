.. _examples:


CategorySet - Examples
**********************


"Male" and "Female" categories in a CategorySet named "Gender" A CategorySet for an occupational classification system like ISCO-08 could use a ClassificationRelationStructure to describe a hierarchy (Chief Executives and Administrative and Commercial Managers as subtypes of Managers) without an associated Code List.