.. _ConceptualVariable:


ConceptualVariable
******************


Extends
=======
:ref:`Concept`


Definition
==========
The use of a Concept as a characteristic of a UnitType intended to be measured


Synonyms
========



Explanatory notes
=================
Note that DDI varies from GSIM in the use of a UnitType rather than a Universe. "The use of a Concept as a characteristic of a Universe intended to be measured" [GSIM 1.1]


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst