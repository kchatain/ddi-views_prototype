.. _fields:


ConceptualDomain - Properties and Relationships
***********************************************





Properties
==========

============  ===============  ===========
Name          Type             Cardinality
============  ===============  ===========
displayLabel  LabelForDisplay  0..n
============  ===============  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.




Relationships
=============

==========================  ==========================  ===========
Name                        Type                        Cardinality
==========================  ==========================  ===========
describedConceptualDomain   ValueAndConceptDescription  0..n
enumeratedConceptualDomain  ConceptSystem               0..n
==========================  ==========================  ===========


describedConceptualDomain
#########################
A description of the concepts in the domain. A numeric domain might use a logical expression to be machine actionable a text domain might use a regular expression to describe strings that describe the concepts.




enumeratedConceptualDomain
##########################
The set of categories containing the concepts in the domain.



