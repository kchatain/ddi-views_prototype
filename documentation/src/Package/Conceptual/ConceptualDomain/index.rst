.. _ConceptualDomain:


ConceptualDomain
****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Set of Concepts, both sentinel and substantive, that can be described by either enumeration or by an expression.


Synonyms
========



Explanatory notes
=================
Intent of a Conceptual Domain is defining a set of concepts used to measure a broader concept. For effective use they should be discrete (non-overlapping) and provide exhaustive coverage of the broader concept.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst