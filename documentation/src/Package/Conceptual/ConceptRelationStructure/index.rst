.. _ConceptRelationStructure:


ConceptRelationStructure
************************


Extends
=======
:ref:`Identifiable`


Definition
==========
Relation structure of concepts within a collection. Allows for the specification of complex relationships among concepts.


Synonyms
========



Explanatory notes
=================
The ConceptRelationStructure employs a set of ConceptRelations to describe the relationship among concepts. Each ConceptRelation is a one to many description of connections between concepts. Together they can describe relationships as complex as a hierarchy or even a complete cyclical network as in a concept map.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst