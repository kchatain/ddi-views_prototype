.. _RepresentedVariable:


RepresentedVariable
*******************


Extends
=======
:ref:`ConceptualVariable`


Definition
==========
A combination of a characteristic of a universe to be measured and how that measure will be represented.


Synonyms
========



Explanatory notes
=================
Extends from ConceptualVariable and can contain all descriptive fields without creating a ConceptualVariable. By referencing a ConceptualVariable it can indicate a common relationship with RepresentedVariables expressing the same characteristic of a universe measured in another way, such as Age of Persons in hours rather than years. RepresentedVariable constrains the coverage of the UnitType to a specific Universe. In the above case the Universe with the measurement of Age in hours may be constrained to Persons under 5 days (120 hours old). RepresentedVariable can define sentinel values for multiple storage systems which have the same conceptual domain but specialized value domains.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst