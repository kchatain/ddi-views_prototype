.. _InstanceMeasurement:


InstanceMeasurement
*******************


Extends
=======
:ref:`InstrumentComponent`


Definition
==========
An instance measurement instantiates a represented measurement, so that it can be used as an Act in the Process Steps that define a data capture process.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst