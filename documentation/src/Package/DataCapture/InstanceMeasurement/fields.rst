.. _fields:


InstanceMeasurement - Properties and Relationships
**************************************************





Relationships
=============

============  ======================  ===========
Name          Type                    Cardinality
============  ======================  ===========
instantiates  RepresentedMeasurement  0..n
============  ======================  ===========


instantiates
############
The measurement being instantiated in the data collection process



