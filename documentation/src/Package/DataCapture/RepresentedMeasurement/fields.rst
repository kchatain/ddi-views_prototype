.. _fields:


RepresentedMeasurement - Properties and Relationships
*****************************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
measurementType  ExternalControlledVocabularyEntry  0..1
===============  =================================  ===========


measurementType
###############
The type of measurement performed




Relationships
=============

======================  ===================  ===========
Name                    Type                 Cardinality
======================  ===================  ===========
hasRepresentedVariable  RepresentedVariable  0..n
======================  ===================  ===========


hasRepresentedVariable
######################
An optional link to a represented variable which can be used by each instance variable created by a use of this measure.



