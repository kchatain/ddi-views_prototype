.. _ImplementedInstrument:


ImplementedInstrument
*********************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A specific data capture tool. ImplementedInstruments are mode and/or unit specific.


Synonyms
========
aka PhysicalInstrument


Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst