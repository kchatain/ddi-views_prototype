.. _fields:


NumericResponseDomain - Properties and Relationships
****************************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
numericTypeCode  ExternalControlledVocabularyEntry  0..1
unit             ExternalControlledVocabularyEntry  0..1
usesNumberRange  NumberRange                        0..n
===============  =================================  ===========


numericTypeCode
###############
Identification of the numeric type such as integer, decimal, etc. supports the use of an external controlled vocabulary.


unit
####
An optional unit of measurement.  Examples include units from the International System of Units (SI) such as meter, seconds, mole, kilograms. Other examples include miles, tons, pints.


usesNumberRange
###############
Defines the valid number range or number values for the representation.

