.. _BooleanResponseDomain:


BooleanResponseDomain
*********************


Extends
=======
:ref:`ResponseDomain`


Definition
==========
A response domain capturing a binary response, such as selected/unselected or checked/unchecked or true/false.


Synonyms
========



Explanatory notes
=================
Exactly what is captured may be dependent upon the Implemented Instrument, however the response being captured is either there or not there (ON/OFF, True/False)


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst