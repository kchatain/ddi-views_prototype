.. _fields:


Capture - Properties and Relationships
**************************************





Properties
==========

=============  =================================  ===========
Name           Type                               Cardinality
=============  =================================  ===========
analysisUnit   ExternalControlledVocabularyEntry  0..n
captureSource  ExternalControlledVocabularyEntry  0..1
displayLabel   LabelForDisplay                    0..n
name           ObjectName                         0..n
purpose        InternationalStructuredString      0..1
usage          InternationalStructuredString      0..1
=============  =================================  ===========


analysisUnit
############
Identifies the unit being analyzed such as a Person, Housing Unit, Enterprise, etc. 


captureSource
#############
The source of a capture structure defined briefly; typically using an external controlled vocabulary


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


name
####
A name for the measurement. A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


purpose
#######
A description of the purpose or use of the Measurement. May be expressed in multiple languages and supports the use of structured content. 


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

=================  ==============  ===========
Name               Type            Cardinality
=================  ==============  ===========
hasConcept         Concept         0..n
hasExternalAid     ExternalAid     0..n
hasInstruction     Instruction     0..n
hasResponseDomain  ResponseDomain  0..n
=================  ==============  ===========


hasConcept
##########
Capture has a Concept




hasExternalAid
##############
Capture has an External Aid




hasInstruction
##############
Capture has an Instruction




hasResponseDomain
#################



