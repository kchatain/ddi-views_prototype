.. _examples:


Capture - Examples
******************


A survey question, blood pressure reading; MRI images; thermometer; web service; experimental observation. Classes could include InstanceQuestion, InstanceMeasurement or other class extending Capture.