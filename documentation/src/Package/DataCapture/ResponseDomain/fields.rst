.. _fields:


ResponseDomain - Properties and Relationships
*********************************************





Properties
==========

============  ===============  ===========
Name          Type             Cardinality
============  ===============  ===========
displayLabel  LabelForDisplay  0..1
============  ===============  ===========


displayLabel
############
A display label for the domain. May be expressed in multiple languages.




Relationships
=============

======================  ===================  ===========
Name                    Type                 Cardinality
======================  ===================  ===========
hasOutputParameter      Parameter            0..1
intendedRepresentation  RepresentedVariable  0..n
======================  ===================  ===========


hasOutputParameter
##################
A parameter which can be used to bind the output of this response domain to another parameter to direct the flow of the captured datum.




intendedRepresentation
######################
Associates the ResponseDomain with the RepresentedVariable that the question will populate into an InstanceVariable when fielded.



