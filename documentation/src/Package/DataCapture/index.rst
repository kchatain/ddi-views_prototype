***********
DataCapture
***********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   BooleanResponseDomain/index.rst
   Capture/index.rst
   CodeListResponseDomain/index.rst
   ConceptualInstrument/index.rst
   ExternalAid/index.rst
   ImplementedInstrument/index.rst
   InstanceMeasurement/index.rst
   InstanceQuestion/index.rst
   Instruction/index.rst
   InstrumentCode/index.rst
   InstrumentComponent/index.rst
   NumericResponseDomain/index.rst
   RankingResponseDomain/index.rst
   RepresentedMeasurement/index.rst
   RepresentedQuestion/index.rst
   ResponseDomain/index.rst
   ScaleResponseDomain/index.rst
   Statement/index.rst
   TextResponseDomain/index.rst



Graph
=====

.. graphviz:: /images/graph/DataCapture/DataCapture.dot