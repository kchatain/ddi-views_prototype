.. _examples:


Statement - Examples
********************


Introductory text; Explanations; Labels, Headers, Help screens, etc. "Thank you for agreeing to take this survey. We will start with a brief set of demographic questions." "The following set of questions are related to your household income. Please consider all members of the household and all sources of income when answering these questions."