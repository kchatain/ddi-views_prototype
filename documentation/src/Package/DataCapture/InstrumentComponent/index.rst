.. _InstrumentComponent:


InstrumentComponent
*******************


Extends
=======
:ref:`Act`


Definition
==========
InstrumentComponent is an abstract object which extends an Act (a type of Process Step). The purpose of InstrumentComponent is to provide a common parent for Capture (e.g., Question, Measure), Statement, and Instructions.


Synonyms
========



Explanatory notes
=================
InstrumentComponent acts as a substitution head (abstract) for semantically meaningful for objects that extend it and exist in an instrument.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst