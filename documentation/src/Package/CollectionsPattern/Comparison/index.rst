.. _Comparison:


Comparison
**********


Extends
=======
:ref:`Identifiable`


Definition
==========
The minimal pattern for a comparison including a mapping between members.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst