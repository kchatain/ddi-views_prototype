******************
CollectionsPattern
******************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   CollectionMember/index.rst
   Comparison/index.rst
   ComparisonMap/index.rst
   MemberIndicator/index.rst
   MemberRelation/index.rst
   RelationStructure/index.rst
   SimpleCollection/index.rst
   StructuredCollection/index.rst



Graph
=====

.. graphviz:: /images/graph/CollectionsPattern/CollectionsPattern.dot