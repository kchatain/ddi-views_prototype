.. _CollectionMember:


CollectionMember
****************


Extends
=======
:ref:`Identifiable`


Definition
==========
Generic class representing members of a collection.


Synonyms
========



Explanatory notes
=================
Members have to belong to some Collection, except in the case of nested Collections where the top level Collection is a Member that doesn't belong to any Collection. Member is not extended, it is realized (see Collection Pattern documentation).


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst