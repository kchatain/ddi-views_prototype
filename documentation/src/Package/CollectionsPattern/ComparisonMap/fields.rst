.. _fields:


ComparisonMap - Properties and Relationships
********************************************





Properties
==========

=====================  ==================  ===========
Name                   Type                Cardinality
=====================  ==================  ===========
hasCorrespondenceType  CorrespondenceType  1..1
=====================  ==================  ===========


hasCorrespondenceType
#####################
Describes the relationship between the source and target members using both controlled vocabularies and descriptive text.




Relationships
=============

======  ================  ===========
Name    Type              Cardinality
======  ================  ===========
source  CollectionMember  0..n
target  CollectionMember  0..n
======  ================  ===========


source
######
The member defined as the source in the relationship. Note that when a correspondence has been defined as disjoint only the source relationship should be used.




target
######
The member(s) defined as the target of the mapping. If relationship is disjoint do not use target relationship.



