.. _examples:


SimpleCollection - Examples
***************************


The sequencing of questions within a questionnaire where ordering and routing is done through conditional constructs such as IfThenElse, etc.