.. _fields:


MemberRelation - Properties and Relationships
*********************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list


semantic
########
Provide a semantic that provides a context for the relationship using and External Controlled Vocabulary


totality
########
Type of relation in terms of its totality using an enumeration list.




Relationships
=============

======  ================  ===========
Name    Type              Cardinality
======  ================  ===========
source  CollectionMember  0..n
target  CollectionMember  0..n
======  ================  ===========


source
######
The first member of the relationship. 




target
######
Second member in the relationship. Notes this could be realized as a collection supporting tuple relationship.



