.. _MemberRelation:


MemberRelation
**************



Definition
==========
Defines one kind of relationship between one member (source) and possibly several members (target). The type of the relationship, a semantic, and totality definition may also be specified.


Synonyms
========



Explanatory notes
=================
A MemberRelation functions like an adjacency list (https://en.wikipedia.org/wiki/Adjacency_list) in graph theory, describing the relationships of one Member to multiple other members. All of these relationships must have the same RelationSpecification, Totality, and semantic. These might not be the only relations from the source Member if there are other relations with different properties.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst