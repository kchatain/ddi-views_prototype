.. _examples:


MemberRelation - Examples
*************************


A member might have a ParentChild relation to several other members which is Anti-Reflexive, Anti-Symmetric, and Anti-Transitive. For a process this might have a semantic of "immediately follows".