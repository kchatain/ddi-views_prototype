.. _RelationStructure:


RelationStructure
*****************


Extends
=======
:ref:`Identifiable`


Definition
==========
The set of MemberRelations used to structure a Collection


Synonyms
========



Explanatory notes
=================
The optional RelationStructure is used to describe relations among Members of the Collection that are more complex than a simple list. There can be multiple types of relation structures for a Collection and these in turn may each have a different semantic. These differences can be described by the properties of the Relation Structure, or in the case of hybrid structures described by the individual MemberRelations. Each MemberRelations functions like an adjacency list (https://en.wikipedia.org/wiki/Adjacency_list) in graph theory, describing the relationships of one Member to multiple other members.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst