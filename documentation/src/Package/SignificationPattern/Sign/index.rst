.. _Sign:


Sign
****


Extends
=======
:ref:`CollectionMember`


Definition
==========
Something that suggests the presence or existence of a fact, condition, or quality.


Synonyms
========



Explanatory notes
=================
It is a perceivable object used to denote a concept or an object.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst