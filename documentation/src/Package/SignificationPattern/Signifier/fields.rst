.. _fields:


Signifier - Properties and Relationships
****************************************





Properties
==========

==========  ==============  ===========
Name        Type            Cardinality
==========  ==============  ===========
content     String          0..1
whiteSpace  WhiteSpaceRule  0..1
==========  ==============  ===========


content
#######
Perceivable object in the extension of the signifier. The actual content of this value as a string.


whiteSpace
##########
The usual setting "collapse" states that leading and trailing white space will be removed and multiple adjacent white spaces will be treated as a single white space. When setting to "replace" all occurrences of #x9 (tab), #xA (line feed) and #xD (carriage return) are replaced with #x20 (space) but leading and trailing spaces will be retained. If the existence of any of these white spaces is critical to the understanding of the content, change the value of this attribute to "preserve".

