.. _examples:


Signifier - Examples
********************


A perceivable object in the extension of a signifier is a token. For instance, the object 5 is a token of “the numeral five”, a signifier.