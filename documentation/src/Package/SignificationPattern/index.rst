********************
SignificationPattern
********************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Sign/index.rst
   Signified/index.rst
   Signifier/index.rst



Graph
=====

.. graphviz:: /images/graph/SignificationPattern/SignificationPattern.dot