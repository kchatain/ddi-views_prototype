.. _fields:


ConditionalControlStep - Properties and Relationships
*****************************************************





Properties
==========

=========  ===========  ===========
Name       Type         Cardinality
=========  ===========  ===========
condition  CommandCode  0..1
=========  ===========  ===========


condition
#########
Condition to be evaluated to determine whether or not to execute the Workflow Sequence in contains. It can be an expression and/or programmatic code. The specialized sub-classes determine whether the Sequence is executed when the condition is true or false




Relationships
=============

========  ====================  ===========
Name      Type                  Cardinality
========  ====================  ===========
executes  WorkflowStepSequence  0..n
========  ====================  ===========


executes
########
The Workflow Sequence is executed depending on the result of the condition evaluation. The specialized sub-classes determine whether the Sequence is executed when the condition is true or false. Specialization of contains in Control Construct.



