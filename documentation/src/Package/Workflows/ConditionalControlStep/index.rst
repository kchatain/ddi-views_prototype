.. _ConditionalControlStep:


ConditionalControlStep
**********************


Extends
=======
:ref:`WorkflowControlStep`


Definition
==========
Type of WorkflowControlStep in which the execution flow is determined by one or more conditions.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst