.. _fields:


WorkflowProcess - Properties and Relationships
**********************************************





Properties
==========

========  =============================  ===========
Name      Type                           Cardinality
========  =============================  ===========
name      ObjectName                     0..n
overview  InternationalStructuredString  0..1
purpose   InternationalStructuredString  0..1
usage     InternationalStructuredString  0..1
========  =============================  ===========


name
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which the pattern is employed. Supports  the use of multiple languages and  structured text.  




Relationships
=============

===================  ===================  ===========
Name                 Type                 Cardinality
===================  ===================  ===========
implementsAlgorithm  AlgorithmOverview    0..n
isSpecifiedBy        WorkflowControlStep  0..n
realizes             Process              0..n
===================  ===================  ===========


implementsAlgorithm
###################
The algorithms implemented by the process. May be expressed by AlgorithmOverview or any subclass




isSpecifiedBy
#############
Points to the WorkflowControlStep subtype which defines the starting point for initiating the set of process steps contained in the process.




realizes
########
ProcessPattern class realized by this class



