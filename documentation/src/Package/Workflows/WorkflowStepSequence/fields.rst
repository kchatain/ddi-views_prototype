.. _fields:


WorkflowStepSequence - Properties and Relationships
***************************************************





Properties
==========

==========================  =================================  ===========
Name                        Type                               Cardinality
==========================  =================================  ===========
name                        ObjectName                         0..n
orderedStep                 WorkflowStepOrder                  0..n
purpose                     InternationalStructuredString      0..1
typeOfWorkflowStepSequence  ExternalControlledVocabularyEntry  0..n
==========================  =================================  ===========


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


orderedStep
###########
The Workflow Steps included in the Collection with an index for position within ordered array. 


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


typeOfWorkflowStepSequence
##########################
Provides the ability to "type" a sequence for classification or processing purposes. Supports the use of an external controlled vocabulary.

