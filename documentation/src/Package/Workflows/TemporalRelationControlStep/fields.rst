.. _fields:


TemporalRelationControlStep - Properties and Relationships
**********************************************************





Properties
==========

==========================  =============================  ===========
Name                        Type                           Cardinality
==========================  =============================  ===========
typeOfTemporalRelationship  TemporalRelationSpecification  0..1
==========================  =============================  ===========


typeOfTemporalRelationship
##########################
Specify the type of temporal relationhship between identified WorkflowSteps using Allen's Interval definitions.

