.. _TemporalRelationControlStep:


TemporalRelationControlStep
***************************


Extends
=======
:ref:`WorkflowControlStep`


Definition
==========
Defines complex synchronous or asynchronous temporal ordering control. Defines temporal relationships using Allen's Intervals.


Synonyms
========



Explanatory notes
=================
When creating a specific subtype identify the workflow steps included in a structure that is appropriate to the subtype. For example, Split contains a relationship "executeConcurrently".


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst