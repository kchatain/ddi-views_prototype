.. _examples:


WorkflowControlStep - Examples
******************************


A WorkflowStepSequence is a subtype of WorkflowControlStep which defines a sequential order for included steps.