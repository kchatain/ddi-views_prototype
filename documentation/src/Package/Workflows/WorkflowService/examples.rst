.. _examples:


WorkflowService - Examples
**************************


A specification for an air flow monitor used to capture a measure for an ImplementedMeasure (a subtype of Act).