.. _fields:


WorkflowService - Properties and Relationships
**********************************************





Properties
==========

=================  =================================  ===========
Name               Type                               Cardinality
=================  =================================  ===========
estimatedDuration  Date                               0..1
serviceInterface   ExternalControlledVocabularyEntry  0..n
serviceLocation    ExternalControlledVocabularyEntry  0..1
=================  =================================  ===========


estimatedDuration
#################
The estimated time period associated with the operation of the Service. This may be expressed as a time, date-time, or duration.


serviceInterface
################
Specifies how to communicate with the service.


serviceLocation
###############
Specifies where the service can be accessed.




Relationships
=============

========  =======  ===========
Name      Type     Cardinality
========  =======  ===========
hasAgent  Agent    0..n
realizes  Service  0..n
========  =======  ===========


hasAgent
########
Actor that performs a role in the service,




realizes
########
Class in the Process Pattern realized by Workflow Service.



