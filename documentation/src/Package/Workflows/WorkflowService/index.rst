.. _WorkflowService:


WorkflowService
***************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A means of performing a Workflow Step as part of a concrete implementation of a Business Function (an ability that an organization possesses, typically expressed in general and high level terms and requiring a combination of organization, people, processes and technology to achieve).


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst