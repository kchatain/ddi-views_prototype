.. _examples:


SplitJoin - Examples
********************


A GANTT chart where a number of processes running parallel to each other are all prerequisites of a subsequent step.