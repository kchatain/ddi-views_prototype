.. _fields:


WorkflowStep - Properties and Relationships
*******************************************





Properties
==========

==================  =============================  ===========
Name                Type                           Cardinality
==================  =============================  ===========
displayLabel        LabelForDisplay                0..n
hasInformationFlow  Binding                        0..n
name                ObjectName                     0..n
overview            InternationalStructuredString  0..1
purpose             InternationalStructuredString  0..1
usage               InternationalStructuredString  0..n
==================  =============================  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


hasInformationFlow
##################
Mapping of specific information flow within the workflow


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.




Relationships
=============

==================  ===============  ===========
Name                Type             Cardinality
==================  ===============  ===========
hasInputParameter   Parameter        0..1
hasOutputParameter  Parameter        0..1
isPerformedBy       WorkflowService  0..n
realizes            ProcessStep      0..n
==================  ===============  ===========


hasInputParameter
#################
An identification for a Parameter related to the WorkflowStep and referred to by a Binding indicating the Input to the WorkflowStep 




hasOutputParameter
##################
An identification for a Parameter related to the WorkflowStep and referred to by a Binding indicating the Output from the WorkflowStep




isPerformedBy
#############
Identifies the Service Implementation which performs the Workflow Step. Specialization of isPerformedBy in Process Step for Service Implementations.




realizes
########
Class in the ProcessPattern realized by WorkflowStep.



