.. _WorkflowStep:


WorkflowStep
************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
One of the constituents of a Workflow. It can be a composition or atomic and might be performed by a Service.


Synonyms
========



Explanatory notes
=================
An atomic Workflow Step has no Control Constructs -- it's an Act. A composition consists of a tree of Control Constructs. In this tree Acts are associated with the leaf nodes. Furthermore, a composition might be a glass box or a black box from the perspective of a service. If a service performs a WorkflowStep, it might invoke just the topmost ControlConstruct -- for example a WorkflowSequence. It may know nothing about the internal workings of the sequence. In this case the WorkflowStep is a black box.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst