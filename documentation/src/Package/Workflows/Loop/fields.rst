.. _fields:


Loop - Properties and Relationships
***********************************





Properties
==========

============  ===========  ===========
Name          Type         Cardinality
============  ===========  ===========
initialValue  CommandCode  0..1
stepValue     CommandCode  0..1
============  ===========  ===========


initialValue
############
The command used to set the initial value for the process. Could be a simple value.


stepValue
#########
The command used to set the incremental or step value for the process. Could be a simple value.

