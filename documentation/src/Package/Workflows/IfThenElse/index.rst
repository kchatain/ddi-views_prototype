.. _IfThenElse:


IfThenElse
**********


Extends
=======
:ref:`ConditionalControlStep`


Definition
==========
IfThenElse describes an if-then-else decision type of control construct. If the stated condition is met, then the associated Workflow Sequence in containsSubSeqence is triggered, otherwise the Workflow Sequence that is triggered is the one associated via elseContains.


Synonyms
========



Explanatory notes
=================
Contains a condition and two associations: - one to the Workflow Sequence that is triggered when the condition is true (containsSubSequence), and - another to the Workflow Sequence that is triggered when the condition is false (elseContains).


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst