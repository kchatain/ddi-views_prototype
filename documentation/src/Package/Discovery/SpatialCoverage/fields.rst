.. _fields:


SpatialCoverage - Properties and Relationships
**********************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
description      InternationalStructuredString      0..1
spatialAreaCode  ExternalControlledVocabularyEntry  0..n
spatialObject    SpatialObjectType                  0..1
===============  =================================  ===========


description
###########
A textual description of the spatial coverage to support general searches.


spatialAreaCode
###############
Supports the use of a standardized code such as ISO 3166-1,  the Getty Thesaurus of Geographic Names, FIPS-5, etc. 


spatialObject
#############
Indicates the most discrete spatial object type identified for a single case. Note that data can be collected at a geographic point (address) and reported as such in a protected file, and then aggregated to a polygon for a public file.




Relationships
=============

====================================  ================================  ===========
Name                                  Type                              Cardinality
====================================  ================================  ===========
hasBoundingBox                        BoundingBox                       0..n
highestGeographicUnitTypeCovered      UnitType                          0..n
includesGeographicUnitType            UnitType                          0..n
includesGeographicUnit                GeographicUnit                    0..n
lowestGeographicUnitTypeCovered       UnitType                          0..n
usesGeographicUnitClassification      GeographicUnitClassification      0..n
usesGeographicUnitTypeClassification  GeographicUnitTypeClassification  0..n
====================================  ================================  ===========


hasBoundingBox
##############
The north and south latitudes and east and west longitudes that define the spatial coverage area.




highestGeographicUnitTypeCovered
################################
The unit type describing the highest or broadest geographic unit type that can be assembled within this coverage. In some cases with multiple hierarchies there may be multiple geographic unit types assembled.




includesGeographicUnitType
##########################
The geographic unit types used within this coverage.




includesGeographicUnit
######################
The geographic units used within this coverage.




lowestGeographicUnitTypeCovered
###############################
The lowest geographic unit type identified within this coverage. Repeat if multiple geographic hierarchies are used and there is no single lowest level that can be used to compile geographic areas into higher levels. For example a file that has one hierarchy using State-County-CensusTract/BNA and also data identified for Place which cannot be constructed directly from Census Tracts as they may cross Place boundaries.




usesGeographicUnitClassification
################################
A classification which describes the geographic units (specific locations) and relationships used within this coverage.




usesGeographicUnitTypeClassification
####################################
A classification which describes the geographic unit types and relationships used within this coverage.



